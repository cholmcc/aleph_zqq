#!/usr/bin/env python3
#
#  © 2023 Christian Holm Christensen
#  
#  Licensed under the GPL-3
#

from pyzqq.gammagamma import GammaGamma
from pyzqq.pipi import PiPi
from pyzqq.pipi0 import PiPi0
from pyzqq.kpi import KPi

known = {
    'pipi' : PiPi,
    'pipi0': PiPi0,
    'gammagamma': GammaGamma,
    'kpi':        KPi
}

def plotFills(infile,base='',select=None,save=False,batch=False):
    '''Plot all filled histograms according to the filler
    prescriptions
    '''
    from pprint import pprint
    from matplotlib.pyplot import ion
    from pyzqq.utils import loadResults

    if not batch:
        ion()
    
    d = loadResults(infile)
    pprint(d,depth=4)

    if select is None or select == 'all':
        select = list(known.keys())

    for k, cls in known.items():
        if k not in select:
            continue
        
        dd = d.get(k,{})
        cls.plot(dd,figsize=(16,10),num=base+k,save=save)
        
        if k not in d:
            continue

        
if __name__ == '__main__':
    from sys import exit
    from pathlib import Path
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser(description='Plot filled histograms',
                        epilog='--save will save plots into PNGs')
    ap.add_argument('input',type=FileType('r'),help='Input JSON file(s)',
                    nargs='+')
    ap.add_argument('-f',"--which",
                    choices=list(known.keys())+['all'],
                    nargs='*',default=None,
                    help='Select which filler data to plot')
    ap.add_argument('-s',"--save",action='store_true',
                    help='Save plots as PNGs')
    ap.add_argument('-b',"--batch",action='store_true',
                    help='Do not show plots')

    args = ap.parse_args()
    batch = args.batch
        
    for inp in args.input:
        base = Path(inp.name).stem + '_' if len(args.input) > 1 else ''
        plotFills(inp,
                  base=base,
                  select=args.which,
                  save=args.save,
                  batch=args.batch)
        
    if not args.batch:
        print(f'Press Ctrl-D or execute "exit()" to exit')

        import code
        code.interact(banner=False,local=locals())
            
    
#
# EOF
#
