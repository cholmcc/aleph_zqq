# Aleph Z-qq Event analyses 

This is a port of Peter Hansen's ALEPH ($\aleph$) $Z\rightarrow q\bar{q}$
analyses to Python.

## Use

Make sure you have Python and Jupyter installed.  

* For most GNU/Linux distributions, use your package manager, e.g., 

      sudo apt install jupyter jupyter-notebook python3-numpy python3-scipy python3-matplotlib

* For other operating systems use
  f.ex. [Anaconda](https://www.anaconda.com/download#downloads) or
  [MiniConda](https://docs.conda.io/projects/miniconda/en/latest/miniconda-install.html).
  Please follow the instructions for your system. 
  
If your Python installation does not have the package
[`nbi_stat`](https://cholmcc.gitlab.io/nbi-python/statistics/nbi_stat/),
please install that with 

    pip install nbi_stat 
	
(note, for some Python installations you need to replace `pip` with
`pip3`). 

Now grap this package from GitLab 

    git clone https://gitlab.com/cholmcc/aleph_zqq.git 
	
or down the code as 

| Archive type | Extract command (Un*x-like) |
|--------------|-----------------------------|
| [ZIP](https://gitlab.com/cholmcc/aleph_zqq/-/archive/master/aleph_zqq-master.zip) | `unzip aleph_zqq-master.zip` |
| [TAR.GZ](https://gitlab.com/cholmcc/aleph_zqq/-/archive/master/aleph_zqq-master.tar.gz) | `tar -xzf aleph_zqq-master.tar.gz` |
| [TAR.BZ2](https://gitlab.com/cholmcc/aleph_zqq/-/archive/master/aleph_zqq-master.tar.bz2) | `tar -xjf aleph_zqq-master.tar.bz2` |

### First look at analyses 

Now you can do (Un*x-like) 

    > cd aleph_zqq 
	> cd analysis 
	> jupyter notebook # or jupyterlab 
	
and select the [analysis](analysis) notebook to open. 

## Filling histogram 

The code to fill histograms is in the package [`pyzqq`](pyzqq/). 

Each specific way of filling summary histograms in encoded in a class
deriving from the class [`pyzqq.Filler`](pyzqq/filler.py).  Defined
fillers are 

- [`GammaGamma`](pyzqq/gammagamma.py)
- [`PiPi`](pyzqq/pipi.py)
- [`PiPi0`](pyzqq/pipi0.py)
- [`KPi`](pyzqq/kpi.py)

The fillers are not executed directly.  Rather the class
[`pyzqq.Looper`](pyzqq/looper.py) contains one or more of these filler
classes and then manages the processing of the input data.

The fillers can be executed via the script [`fillers.py`](fillers.py).
Execute 

    ./fillers --help 
	
for more about options.   The end result of this pass is a JSON file
containing the histograms filled by the fillers.   One such JSON file
is produced from each input zip archive. 

Histogram [JSON](https://json.org) files can be merged into a single
file using the tool [`merge.py`](merge.py).  Execute

    ./merge.py --help 
	
for more information.

> Python, nor the module _NumPy_ really defines a structure for
> histograms - for better or for worse.  We therefore have the class
> [`pyzqq.Histogram`](pyzqq/histogram.py).   Also, no disk storage
> format is defined in _NumPy_ for things like histograms.  We
> therefore use the ubiquitous and versatile [JavaScript Object
> Notation (JSON)](https://json.org) format.  This format is ease to
> read in, in a great deal of programming languages,  and allows us to
> structure the data in "directories" or hierarchies.  

## Analysis of the spectra 

The analysis of the spectra is done in [Jupyter
Notebook](https://jupyter.org)s.   These notebooks reside in the
sub-directory [`analysis`](analysis). 

The notebook uses tools from the [`pyzqq`](pyzqq) package as well as
other standard Python packages 

- [NumPy](https://numpy.org)
- [SciPy](https://scipy.org)
- [MatPlotLib](https://matplotlib.org)

as well as the statistical package 

- [`nbi_stat`](https://cholmcc.gitlab.io/nbi-python/statistics/nbi_stat/) 

which is based on the free (as in beer _and_ speech) book
[Statistics Overview - With
Python](https://cholmcc.gitlab.io/nbi-python/statistics/#Statistik),
available in both English and Danish.  If this package is not
available on your system, you can install it by 

    pip install nbi_stat 
    
or perhaps 

    pip3 install nbi_stat 
	

## Event data 

The event data has been uploaded to [ERDA](https://erda.dk) and can be
found [here](https://sid.erda.dk/cgi-sid/ls.py?share_id=hFntaBeRdN).

If you are registered on ERDA, then you can also request access to the
[`AlephZqq`
workgroup](https://erda.dk/wsgi-bin/viewvgrid.py?vgrid_name=AlephZqq).
See also [here](https://erda.ku.dk/vgrid/AlephZqq/index.html).

## Original data 

The original data was stored in [ROOT](https://root.cern.ch) `TTree`
objects, and the filling and fitting of distributions was done using
ROOT. 

However, ROOT is something of a beast to install and use, and is
therefore a bit if hindrance to approach the data.  On the other hand,
students are becoming more and more familiar with Python and the many
tools available within that environment.  This is because they get
direct training in Python (_Data and project_ on 1st year,
_Introduction to computing for physicist_ on the 2nd year), and Python
has proven to be an excellent teaching tool - not least through the
use of [Jupyter Notebooks](https://jupyter.org).

The original ROOT (and therefore C++) based analyses have therefore
been ported to Python. 

The first step is to convert the ROOT `TTree` files into a format
better suited for analysis in Python.  The chosen structure is a zip
archive of [NumPy](https://numpy.org) array files.  Each zip archive
corresponds to a single ROOT file.  It contains 

- `headers.np` which contains a table of all event headers. 
- `dictionary.json` is a [JSON](https://json.org) file describing the
  columns of all tables in the archive.
- One sub-directory for each event.  These sub-directories in turn
  contains the files 
  
  - `jets.np` table of (the 3) jets of the event. 
  - `v0s.np` table of predetermined V0s of the event.
  - `tracks.np` table of all detected tracks in the event.
  - `photons.np` table of all detected photons in the event. 
  
The zip archives are generated by the ROOT script
[`tools/Extract.C`](tools/Extract.C) and the Python script 
[`tools/export.py`](tools/export.py).   The first script reads in the
ROOT `TTree` file and writes the data on standard output.  The Python
script then reads from standard input and generates the events and
stores them in the output archive. 

> **NB**: Neither the original ROOT `TTree` files nor the event zip
> archives are stored in the repository but can be obtained separately
> from a different source.  This is because these files are rather
> large (Giga-bytes of data) and not really well-suited to be stored
> in a Git repository. 

## Original ROOT-based code 

The original filling methods based on ROOT (and programmed in C++) are
available, albeit re-coded in a more modern and efficient fashion, in
the sub-directory [`recoded`](recoded). 






