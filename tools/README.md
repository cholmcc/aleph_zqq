# Various tools 

- [`Convert.C`](Convert.C) converts ROOT `TH1` histograms into JSON
  data. 
- [`Extract.C`](Extract.C) Reads in events from a ROOT file with a
  `TTree` in it, and outputs the data to standard output. 
- [`Particles.C`](Particles.C)  Extracts particle information from
  ROOT's `TDatabasePDG` and outputs it to a JSON file.
- [`export.py`](export.py) Reads event data from standard input (as
  produced by `Extract.C`) and stores the data in a ZIP archive of
  NumPy arrays. 
- [`logo.png`](logo.png) The logo 
- [`logo.tex`](logo.tex) The $`\mathrm{\LaTeX}`$ source of the logo 
- [`particles.py`](particles.py) Reads in particle information from a
  JSON file (as produced by `Particles.C`) and outputs a `dict`
  definition to standard output.  The `dict` maps from PDG number to
  name, pretty format name, and so on. 
- [`test.py`](test.py) Some tests 
- [`timingInv.py`](timingInv.py) Time calculation of invariant mass. 

