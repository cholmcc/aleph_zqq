# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3
#
def make_pretty_gen(nam,cat):
    """General pretty print a name 

    Parameters
    ----------
        nam : str 
            The unencoded symbol value 
        cat : str
            Category of name 

    Returns
    -------
        prt : str
            The pretty print version of the name 
    """
    import re
    
    pp = nam
    fix = {'KS':             r'\mathrm{K}^{0}_{S}',
           'KL':             r'\mathrm{K}^{0}_{L}',
           'UNSPEC':         'x',
           'INVISIBLE':      'x',
           'CHARGED':        r'x^{\pm}', 
           'MULTICHARGED':   r'x^{\pm N}',
           'VLQ':            r'\vec{q}',
           'VLT':            r'\vec{q}^{2/3}',
           'VLB':            r'\vec{q}^{-1/3}',
           'VLX':            r'\vec{q}^{5/3}',
           'VLY':            r'\vec{q}^{-4/3}',
           'VMESON':         r'\vec{\mathrm{Meson}}',
           'CHARGED-MESON':  r'\mathrm{Meson}^{\pm}',
           'SPARTICLE':      r'\tilde{x}',
           'SQUARK':         r'\tilde{q}',
           'GLUINIUM':       r'\tilde{g}',
           'HEAVY-FLAVOR':   r'\mathrm{Heavy flavor}'
    }
    if nam in fix:
        return fix[nam]

    # Put star in exponent
    pp = re.sub(r'\*(BAR|)',r'\1^{*}',pp)
    # Put signs in exponenent 
    pp = re.sub(r'(\*?)(\++|-+|0|1|2)$', r'^{\1\2}',pp)
    # Charmed or Bottomed
    pp = re.sub(r'(\([0-9]+\)|PRIME|)/C([0-9]*)(BAR|)',r'\3_{c\2}\1',pp)
    pp = re.sub(r'(\([0-9]+\)|PRIME|)/B([0-9]*)(BAR|)',r'\3_{b\2}\1',pp)
    # Append a prime 
    pp = re.sub(r'([A-Za-z0-9\\]+|_{[cb][0-9]*})PRIME', r"\1^{\\prime}", pp)
    # Subscript after name
    pp = re.sub(r'([A-Z ]+)([0-9]+)(BAR|)',r'\1\3_{\2}',pp)
    # Special for flavour neutrinos
    pp = re.sub(r'NU(E|MU|TAU)(BAR|)',r'NU\2_{\1}',pp)
    # Special for J/PSI
    pp = re.sub(r'J/PSI',r'\\mathrm{J/\\psi}',pp)
    # Overline over anti-particles 
    pp = re.sub(r'([A-Za-z0-9*\\]+)BAR', r'\\overline{\1}', pp)
    # Put name in math-roman
    pp = re.sub(r'^(\\overline\{|)([A-Z ]+)(\}|)',r'\1\\mathrm{\2}\3',pp)
    # SUSY
    pp = re.sub(r'\\mathrm{([A-Z]+)INO}',r'\\tilde{\\mathrm{\1}}',pp)
    pp = re.sub(r'\\mathrm{S(?!IGMA|TAR|HOWER|Q|QBAR)([A-Z]+)}',
                r'\\tilde{\\mathrm{\1}}',pp)
    # Take some out of math-roman
    pp = re.sub(r'\\mathrm{(LEPTON|QUARK|CHARGED|CHARG|NEUTRAL)}',r'{\1}',pp)
    # Remove double superscripts
    pp = re.sub(r'\^{(\*|\\prime)}\^{(.*)}',r'^{\1\2}',pp)
    spec = {'N':          'n',
            'P':          'p',
            'NUCLEON':    'N',
            'HADRON':     'h',
            'LEPTON':     'l',
            'HIGGS':      'H',
            'AXION':      'A',
            'E':          'e',
            'UQ':         'u',
            'DQ':         'd',
            'SQ':         's',
            'CQ':         'c',
            'TQ':         't',
            'BQ':         'b',
            'UP':         'u',
            'DOWN':       'd',
            'STRANGE':    's',
            'CHARM':      'c',
            'TOP':        't',
            'BOTTOM':     'b',
            'GLUON':      'g',
            'GRAVITON':   'G',
            'NEUTRAL':    'x',
            'CHARGED':    'x',
            'QUARK':      'q',
            'MUON':       r'\\mu',
            'VEE':        r'\\vee',
            'HTRACK':     'H-track',
            'LEPTOQUARK': 'Lepto-quark',
            'VMESON':     'V-Meson',
            'UPSI':       'UPSILON',
            'PION':       r'\\pi',
            'GAUG':       'g',
            'CHARG':      'x',
            'PHOT':       r'\\gamma',
            'AX':         'A',
            'GLU':        'g',
            'GRAVIT':     'G',
            'GOLDST':     'Goldstone'
    }
    cap = { 'JET', 'MESON', 'HEAVY', 'LIGHT', 'FLAVOR', 'LONGLIVED',
            'NARROW', 'KINK', 'STAR', 'SHOWER', 'GREY', 'BLACK' 
    }
    lbs = ['PI',  'GAMMA',   'TAU',   'MU',   'NU',   'RHO',
           'ETA', 'PHI', 'PSI', 'CHI']
    ubs = ['LAMBDA', 'XI', 'LAMBDA', 'SIGMA', 'DELTA', 'UPSILON']

    # Omegas can be mesons and baryons 
    if cat.find('Baryon') > 0:
        ubs.append('OMEGA')
    else:
        lbs.append('OMEGA')
        
    for s in spec:
        pp = re.sub('{'+s+'}','{'+spec[s]+'}',pp)

    for p in cap:
        pp = re.sub(p,p.capitalize(),pp)
        
    for p in lbs:
        pp = re.sub('{'+p+'}',r'{\\'+p.lower()+'}',pp)

    for p in ubs:
        pp = re.sub('{'+p+'}',r'{\\'+p.capitalize()+'}',pp)
        
    return pp

def make_pretty(name,cat):
    import re
    
    anti = name.endswith('_bar')
    if anti: name = name[:-4]

    susy = name[0] == '~'
    if susy: name = name[1:]

    charge = re.sub(r'(.*[^-+_])([-01+]+)$',r'\2',name)
    if len(charge) > 0 and charge != name:
        name = name[:-len(charge)]
    else:
        charge = ''

    sub = re.sub(r'(.*)_(.+)', r'\2', name)
    if len(sub) > 0 and sub != name:
        name = name[:-len(sub)-1]
    else:
        sub = ''
        
    subeLR = sub[-1] if sub.endswith('L') or sub.endswith('R') else ''
    if subeLR != '':
        sub = sub[:-1]

    subsLR = sub[0] if sub.startswith('L') or sub.startswith('R') else ''
    if subsLR != '':
        sub = sub[1:]
        
    ex = re.sub(r"([^'\*]*)(['\*]+)$",r'\2',name)
    if len(ex) > 0 and ex != name:
        name = name[:-len(ex)]
    else:
        ex = ''

    num = re.sub(r'(.*)\(([0-9]+)\)',r'\2',name)
    if len(num) > 0 and num != name:
        name = name[:-len(num)-2]
    else:
        num = ''

    pn = {
        'nu':      r'\nu',
        'mu':      r'\mu',
        'tau':     r'\tau',
        'gamma':   r'\gamma',
        'eta':     r'\eta',
        'pi':      r'\pi',
        'rho':     r'\rho',
        'omega':   r'\omega',
        'phi':     r'\eta',
        'J/psi':   r'J/\psi',
        'psi':     r'\psi',
        'chi':     r'\chi',
        'Upsilon': r'\Upsilon',
        'Delta':   r'\Delta',
        'Sigma':   r'\Sigma',
        'Lambda':  r'\Lambda',
        'Xi':      r'\Xi',
        'Omega':   r'\Omega',
        'dd':      '(dd)',
        'uu':      '(uu)',
        'ud':      '(ud)',
        'ss':      '(ss)',
        'su':      '(su)',
        'sd':      '(sd)',
        'cc':      '(cc)',
        'cu':      '(cu)',
        'cd':      '(cd)',
        'cs':      '(cs)',
        'bb':      '(bb)',
        'bu':      '(bu)',
        'bd':      '(bd)',
        'bs':      '(bs)',
        'bc':      '(bc)',
        'tt':      '(tt)',
        'tb':      '(tb)',
        'tu':      '(tu)',
        'td':      '(td)',
        'ts':      '(ts)',
        'tc':      '(tc)',
        'reggeon': r'\mathrm{reggeon}',
        'pomeron': r'\mathrm{pomeron}',
        'LQ':      r'\mathrm{LQ}',
        'specflav':r'\mathrm{specflav}',
        'rndmflav':r'\mathrm{rndmflav}',
        'phasespa':r'\mathrm{phasespa}', 
        'c-hadron':r'\mathrm{c-hadron}',
        'b-hadron':r'\mathrm{b-hadron}',
        'cluster': r'\mathrm{cluster}',
        'string':  r'\mathrm{string}',
        'indep.':  r'\mathrm{indep}',
        'CMshower':r'\mathrm{CMshower}',
        'SPHEaxis':r'\mathrm{SPHEaxis}',
        'THRUaxis':r'\mathrm{THRUaxis}',
        'CLUSjet': r'\mathrm{CLUSjet}',
        'CELLjet': r'\mathrm{CELLjet}',
        'table':   r'\mathrm{table}',
        'proton':  r'p',
        'antiproton':  r'\overline{p}',
        'neutron':     r'n',
        'antineutron': r'\overline{n}',
        'gravitino':   r'\tilde{G}',
        'Rootino':     r'',
        'diffr':       r'\mathrm{diffr}',
        'tech':        r'\mathrm{tech}',
        'di':          r'\mathrm{diffr}',
        'diff':        r'\mathrm{diffr}',
        'ue':          r'\mathrm{ue}',
    }
    name = pn.get(name,name)
    sub  = pn.get(sub, sub)

    if len(name) <= 0:
        return ''

    if name == r'\Lambda' and charge == '0':
        charge = ''

    ex  = ex.replace("'",r'\prime')
    ret = name
    if susy:
        ret = fr'\tilde{{{ret}}}'
    if anti:
        ret = fr'\overline{{{ret}}}'
    if len(num) > 0:
        ret += f'({num})'
    if len(sub) > 0 or len(subsLR) > 0 or len(subeLR) > 0:
        ret += f'_{{{subsLR}{{}}{sub}{{}}{subeLR}}}'
    if len(charge) > 0 or len(ex) > 0:
        ret += f'^{{{ex}{{}}{charge}}}'
        


    return ret

def read_particles():
    from json import load

    with open('particles.json','r') as inp:
        return load(inp)


def trans():
    db = read_particles()

    with open('particles.tex','w') as out:
        print(r'''
        \documentclass{article}
        \usepackage{longtable}
        \begin{document}
        \begin{longtable}{rrr}
        ''',file=out)
        
        for k, p in db.items():
            if k == 'EOL': break
            
            nm = p['name']
            print(fr'{int(k):10d} & \verb@{nm:30s}@ & ${make_pretty(nm,"")}$\\',
                  file=out)
            
        print(r'''\end{longtable}
        \end{document}''',file=out)
    

def trans2():
    db = read_particles()

    with open('particlesdb.py','w') as out:
        
        print(r'''# Database of particles, generated from ROOT
class ParticleDB:
    byPDG = {''',file=out)

        lr  = max([len(p['name']) for k,p in db.items() if k != 'EOL'])
        pns = [make_pretty(p['name'],'') for k,p in db.items() if k != 'EOL']
        lp  = max([len(n) for n in pns])

        for (k,p),pn in zip(db.items(),pns):
            if k == 'EOL': break

            nm = p['name']
            pm = p['mass']
            if pn is None or pn == '':
                continue

            qn = f'r"{pn}",'
            qr = f'"{nm}",'
            print(f'        {int(k):<10d}: {{ '
                  f'"name": {qr:{lr+3}s} '
                  f'"pretty": {qn:{lp+3}s} '
                  f'"mass": {pm} }},',
                  file=out)

        print('''
        }
        
    byName = {p['name']: {'pdg': k, 'mass': p['mass'], 'pretty': p['pretty'] }
              for k,p in byPDG.items() }


    @classmethod
    def mass(cls,pdgOrName):
        d = cls.byName if isinstance(pdgOrName,str) else cls.byPDG
        return d.get(pdgOrName,{}).get('mass',None)

    @classmethod
    def pretty(cls,pdgOrName):
        d = cls.byName if isinstance(pdgOrName,str) else cls.byPDG
        return d.get(pdgOrName,{}).get('pretty',None)
        
    @classmethod
    def pdg(cls,name):
        return cls.byName.get(name,{}).get('pdg',None)
        
    @classmethod
    def name(cls,pdg):
        return cls.byPDG.get(pdg,{}).get('name',None)
        
# EOF
''',file=out)
    
        
