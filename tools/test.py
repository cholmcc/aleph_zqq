#!/usr/bin/env python3

def make4vectors(n):
    from numpy import arange

    return arange(4*n).reshape(n,4)

def loops(four):
    from numpy import array
    ret = []
    for ir,ar in enumerate(four):
        for ic in range(ir+1,len(four)):
            ret.append(ar + four[ic])

    return array(ret)


def np(four):
    from numpy import newaxis, transpose, triu_indices

    return transpose((four[...,newaxis]+four.T[newaxis,...]),(0,2,1))[triu_indices(len(four),1)]

def format_time(dt,precision=3,unit=None):
    
    units  = {"nsec": 1e-9, "usec": 1e-6, "msec": 1e-3, "sec": 1.0}

    if unit is not None:
        scale = units[unit]
    else:
        scales = [(scale, unit) for unit, scale in units.items()]
        scales.sort(reverse=True)
        for scale, unit in scales:
            if dt >= scale:
                break
         
    return "%.*g %s" % (precision, dt / scale, unit)
    

def mytimeit(func,number,repeat,prefix='',verbose=False):
    from timeit import Timer
    
    timer   = Timer(func)
    timings = timer.repeat(repeat, number)
    average = [ dt / number for dt in timings]
    best    = min(average)
    worst   = max(average)

    print(f'{prefix}{number} loop{"s" if number > 1 else ""}, '
          f'best of {repeat}: {format_time(best)} per loop')

    return best

def process(ns,bls,bns):
    from matplotlib.pyplot import ion, gca 
    from scipy.optimize import curve_fit
    from numpy import diagonal, sqrt, linspace, log
    
    ion()
    ax = gca()
    ax.plot(ns,bls,'*',label='Loops')
    ax.plot(ns,bns,'s',label='NumPy')

    poly2 = lambda x,a,b,c : a + b * x + c * x ** 2
    nlogn = lambda x,a,b : a * x * log(x)+b

    guess = [0,1,1]

    pl, cl = curve_fit(poly2, ns, bls, guess)
    el     = sqrt(diagonal(cl))

    guess = [1,1]
    pn, cn = curve_fit(nlogn, ns, bns, guess)
    en     = sqrt(diagonal(cn))

    x = linspace(min(ns),max(ns), 30)
    ax.plot(x,poly2(x,*pl),label='Loops fit')
    ax.plot(x,nlogn(x,*pn),label='NumPy fit')

    ax.legend()

    print(pl)
    print(pn)
    return

    sc = min(pn[0],pl[0])
    pl /= sc
    pn /= sc
    
    nw = 8
    lw = 2 * nw + 5
    print(f'Parameter | {"Loops":{lw}s} | {"NumPy":{lw}s} | Factor ')
    print(f'----------|-{"-"*lw}-|-{"-"*lw}-|--------')
    for nn, ppl,eel,ppn,een in zip(['a','b','c'],pl,el,pn,en):
        print(f'{nn:9s} | '
              f'{ppl:{nw}.2g} +/- {eel:{nw}.2g} | '
              f'{ppn:{nw}.2g} +/- {een:{nw}.2g} | '
              f'{ppl/ppn:{nw}.2g}'
              )

    
def test():
    m = 1000
    r = 10

    res = {}
    ns  = [2, 3, 4, 5, 6, 7, 8, 10, 20, 50]
    bls = []
    bns = []
    for n in ns:
        four = make4vectors(n)
        
        bl = mytimeit(lambda : loops(four),m,r,f'Loops {n:2d}: ')
        bn = mytimeit(lambda : np(four),   m,r,f'NumPy {n:2d}: ')

        bls.append(bl)
        bns.append(bn)

    # More than 6, then NumPy wins 
    for n,bl,bn in zip(ns,bls,bns):
        winner = ("Loops" if bl < bn else
                  "NumPy" if bn < bl else "noone")
        factor = (bn/bl if bl < bn else bl/bn)
        print(f'{n:2d} elements, '
              f'{format_time(bl):10s} vs {format_time(bn):10s} '
              f'{winner} wins by a factor {factor:4.1f}')


    process(ns,bls,bns)

if __name__ == '__main__':

    four = make4vectors(10)

    l    = loops(four)
    n    = np(four)

    from numpy import all
    
    assert all(l == n), 'The arrays do not match'


    test()
    
    

    

                                                                      

    
