#!/usr/bin/env python
# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3
#
from sys import stdin, path
path.extend(['..','pyzqq'])
print(path)
from progress import Progress

# ====================================================================
def readHeader(file):
    '''Read in header information'''
    mcFlavour   = int(file.readline().strip())
    cmEnergy    = float(file.readline().strip())
    thrustAxisX = float(file.readline().strip())
    thrustAxisY = float(file.readline().strip())
    thrustAxisZ = float(file.readline().strip())
    quarkAxisX  = float(file.readline().strip())
    quarkAxisY  = float(file.readline().strip())
    quarkAxisZ  = float(file.readline().strip())
    logProbPos  = float(file.readline().strip())
    logProbNeg  = float(file.readline().strip())
    beauty1NN   = float(file.readline().strip())
    beauty2NN   = float(file.readline().strip())
    charge1NN   = float(file.readline().strip())
    charge2NN   = float(file.readline().strip())

    return [float(mcFlavour),
            cmEnergy,   
            thrustAxisX,
            thrustAxisY,
            thrustAxisZ,
            quarkAxisX, 
            quarkAxisY, 
            quarkAxisZ, 
            logProbPos, 
            logProbNeg, 
            beauty1NN, 
            beauty2NN,  
            charge1NN,  
            charge2NN  ]
#---------------------------------------------------------------------
def getHeaderInfo():
    '''Get information dictionary on header table'''
    return {
        'mcFlavour'  : {'index':       0,
                        'type':        'int',
                        'description': 'MC quark PDF, 0 for data' },
        'cmEnergy'   : {'index':       1,
                        'type':        'float',
                        'description': 'Centre-of-mass energy' },   
        'thrustAxisX': {'index':       2,
                        'type':        'float',
                        'description': 'Thrust axis X coordiante' },
        'thrustAxisY': {'index':       3,
                        'type':        'float',
                        'description': 'Thrust axis Y coordinate' },
        'thrustAxisZ': {'index':       4,
                        'type':        'float',
                        'description': 'Thrust axis Z coordinate' },
        'quarkAxisX':  {'index':       5,
                        'type':        'float',
                        'description': 'Quark axis X coordinate' }, 
        'quarkAxisY':  {'index':       6,
                        'type':        'float',
                        'description': 'Quark axis Y coordinate' }, 
        'quarkAxisZ':  {'index':       7,
                        'type':        'float',
                        'description': 'Quark axis Z coordinate' }, 
        'logProbPos':  {'index':       8,
                        'type':        'float',
                        'description': 'log10(Prob(quark)) for z>0' }, 
        'logProbNeg':  {'index':       9,
                        'type':        'float',
                        'description': 'log10(Prob(quark)) for z<0' }, 
        'beauty1NN':   {'index':       10,
                        'type':        'float',
                        'description': '?' },  
        'beauty2NN':   {'index':       11,
                        'type':        'float',
                        'description': '?' },  
        'charge1NN':   {'index':       12,
                        'type':        'float',
                        'description': '?' },        
        'charge2NN':   {'index':       13,
                        'type':        'float',
                        'description': '?' } }

# ====================================================================
def readJets(file):
    '''Read in jets information'''
    from numpy import array
    jets = [
        [float(v) for v in file.readline().strip().split()]
        for i in range(3)
    ]
    ret = array(jets)
    del jets
    return ret

#---------------------------------------------------------------------
def getJetsInfo():
    '''Get information dictionary on jet table'''
    return {
        'px': {'index':       0,
               'type':        'float',
               'description': 'X-momentum'},
        'py': {'index':       1,
               'type':        'float',
               'description': 'Y-momentum'},
        'pz': {'index':       2,
               'type':        'float',
               'description': 'Z-momentum'},
        'e': {'index':        3,
               'type':        'float',
               'description': 'Energy'} 
    }


# ====================================================================
def readArray(file,line):
    '''Generic read of an array'''
    from numpy import array 
    n = int(line.strip().split()[1])
    data = [
        [float(v) for v in file.readline().strip().split()]
        for i in range(n)
    ]
    ret = array(data);
    del data
    return ret

# ====================================================================
def readV0S(file,line):
    '''Read in v0 information'''
    return readArray(file,line)

#---------------------------------------------------------------------
def getV0SInfo():
    '''Get information dictionary on v0s table'''
    return {
        'px':     {'index':       0,
                   'type':        'float',
                   'description': 'Out-going X-momentum'},
        'py':     {'index':       1,
                   'type':        'float',
                   'description': 'Out-going Y-momentum'},
        'pz':     {'index':       2,
                   'type':        'float',
                   'description': 'Out-going Z-momentum'},
        'length': {'index':       3,
                   'type':        'float',
                   'description': 'Decay length'},
        'eLength':{'index':       4,
                   'type':        'float',
                   'description': 'Uncertainty on decay length'},
        'charge': {'index':       5,
                   'type':        'float',
                   'description': 'Electric charge'},
        'type':   {'index':       6,
                   'type':        'int',
                   'description': 'PDG code'} }

# ====================================================================
def readTracks(file,line):
    '''Read in track information'''
    from numpy import bitwise_and, all 
    return readArray(file,line)

#---------------------------------------------------------------------
def getTracksInfo():
    '''Get information dictionary on tracks table'''
    return {
        'px':     {'index':       0,
                   'type':        'float',
                   'description': 'X-momentum'},
        'py':     {'index':       1,
                   'type':        'float',
                   'description': 'Y-momentum'},
        'pz':     {'index':       2,
                   'type':        'float',
                   'description': 'Z-momentum'},
        'e':      {'index':       3,
                   'type':        'float',
                   'description': 'Energy'},
        'tof':    {'index':       4,
                   'type':        'float',
                   'description': 'Time of flight'},
        'status': {'index':       5,
                   'type':        'int',
                   'description': 'Status bits'},
        'type':   {'index':       6,
                   'type':        'int',
                   'description': 'PDG code'},
        'mcType': {'index':       7,
                   'type':        'int',
                   'description': 'PDG code from MC'},
        'mother': {'index':       8,
                   'type':        'int',
                   'description': 'PDG code of mother particle'} }

# ====================================================================
def readPhotons(file,line):
    '''Read in photon information'''
    return readArray(file,line)

#---------------------------------------------------------------------
def getPhotonsInfo():
    '''Get information dictionary on photons table'''
    return {
        'px': {'index':       0,
               'type':        'float',
               'description': 'X-momentum'},
        'py': {'index':       1,
               'type':        'float',
               'description': 'Y-momentum'},
        'pz': {'index':       2,
               'type':        'float',
               'description': 'Z-momentum'} }

# ====================================================================
def writeArray(archive,filename, data):
    '''Write a NumPy array to the zip file'''
    from numpy import save
    
    with archive.open(filename, 'w') as out:
        save(out, data)

    del out

def eventFile(eventNo, what):
    '''Create the file name for a given event and table'''
    return f'{eventNo:09d}/{what}.np'

# ====================================================================
def convert(input,outName,verbose=False,level=None):
    '''Reads information from input stream and generates an archive of
    the read data.  The archive has the structure

        archive -+- dictionary.json
                 |
                 +- headers.np
                 |
                 +- <<event no padded to 9 digits>>
                    |
                    +- jets.np
                    |
                    +- tracks.np
                    |
                    +- v0s.np
                    |
                    +- photons.np

    here, `dictionary.json` contains information about the columns of
    the data tables, and `headers.np` is a NumPy array of header
    information, one row per event.

    For every event, there is a sub-directory named by the event
    number, zero-padded to 9 digits.  In each of these sub-directories
    are up to 4 files

    - jets.np:    NumPy array of jet information (always 3 jets)
    - v0s.np:     NumPy array of V0 information (always 2 V0s) 
    - tracks.np:  NumPy array of track information.  Each row is a
                  separate track, and the number of rows are variable.
    - photons.np: NumPy array of photon information.  Each row is a
                  separate photon, and the number of rows are variable.

    The input is expected to be ASCII data, with the format

         EVENTS <count>
         <events>

    Here, <count> is the expected number of events to see.  Each <event>
    if a multi-section message with the format

         HEADER
         <header lines>
    
         TRACKS <n track lines>
         <track lines>

         PHOTONS <n photon lines>
         <photon lines>

         V0S <n v0 lines>
         <v0 lines>

         JETS <n jet lines>
         <jet lines>

         END

    The order of the sections does not matter, _except_ HEADER must
    come first and END must come last.

    The HEADER section has a particular format where each line
    identifies a parameter.  The order of these lines are significant.

    For the other sections has a integer parameter after the section
    heading which tells how many sub-sequent lines contain the section
    data.   E.g., if we read `TRACKS 10` then 10 track-lines will follow.

    Each data line in a section represents one row of the
    corresponding table.  The number of columns is not fixed, between
    the sections, but each type of section should have the same number
    of columns in each row.

    The event is written to the output archive when the END section is seen.
    '''
    from zipfile import ZipFile, \
        ZIP_BZIP2, ZIP_LZMA, ZIP_DEFLATED, ZIP_STORED
    from numpy import array, vstack
    from json import dumps
    from sys import stderr
    from pathlib import Path
    from gc import collect as garbage_collect

    # Based on the output file name, select the compression type 
    outPath = Path(outName)
    comp    = {'.bzip2': ZIP_BZIP2,
               '.lzma':  ZIP_LZMA,
               '.zip':   ZIP_DEFLATED}.get(outPath.suffix,ZIP_STORED)
               
    

    # Open output archive 
    with ZipFile(outName,"w",
                 compression=comp,
                 compresslevel=level) as output:

        # Get information about all the tables, and write to a JSON
        # file in the archive
        info = { 'headers':          getHeaderInfo(),
                 'jets':             getJetsInfo(),
                 'v0s':              getV0SInfo(),
                 'tracks':           getTracksInfo(),
                 'photons':          getPhotonsInfo() }
        output.writestr('dictionary.json',dumps(info))

        # Headers are initially made as a list, since appending to a
        # numpy array involves copying (e.g., vstack), which will slow
        # things down considerably.
        headers = []
        eventNo = 0

        # Preloop to get info
        more = True
        line = input.readline();
        nev  = -1
        
        while True:
            # Get the number of events to expect 
            if line.startswith("EVENTS"):
                nev  = int(line.strip().split()[1])
                line = input.readline();
                break

            # If we see the start of an event, break and let the next
            # loop handle it.
            if line.startswith("HEADER") or  \
               line.startswith("JETS") or \
               line.startswith("TRACKS") or \
               line.startswith("PHOTONS") or \
               line.startswith("V0S"):
                break

            # Ignore the this line
            print(f'Skipping line: "{line}"',file=stderr)
    
            line = input.readline();
            if len(line) <= 0:
                more = False
                break;
            
                
        if not more:
            raise RuntimeError(f'Failed to read number of events from '
                               f'{outName}')

        # Loop over the data.  Lines are pre-read at end of loop and
        # in pre-loop.
        with Progress(nev) as progress:
            while more:
                if line.startswith("HEADER"):
                    # if verbose>=10: print("New event",file=stderr)
                    headers.append(readHeader(input))
                    jets    = None
                    v0s     = None
                    tracks  = None
                    photons = None
                    progress.step()
                    
                elif line.startswith("JETS"):
                    jets = readJets(input)
                    if verbose>=10:
                        print(f'Read {len(jets)} jets',file=stderr)
                    
                elif line.startswith("V0S"):
                    v0s = readV0S(input, line)
                    if verbose>=10:
                        print(f'Read {len(v0s)} v0s',file=stderr)
                    
                elif line.startswith("TRACKS"):
                    tracks = readTracks(input, line);
                    if verbose>=10:
                        print(f'Read {len(tracks)} tracks',file=stderr)
                    
                    
                elif line.startswith("PHOTONS"):
                    photons = readPhotons(input, line)
                    if verbose>=10:
                        print(f'Read {len(photons)} photons',file=stderr)
                        
                elif line.startswith("END"):
                    for a,n in zip([jets,v0s,tracks,photons],
                                   ['jets','v0s','tracks','photons']):
                        if a is None:
                            if verbose >=5:
                                print(f'No data for {n}',file=stderr)
                            continue
                        if len(a) < 1:
                            if verbose>=5:
                                print(f'No entries in data for {n}',
                                      file=stderr)
                            del a
                            continue
                        
                        if verbose>=10: print(f'Write {n}, {len(a)} rows')
                        writeArray(output, eventFile(eventNo, n), a)
                        del a
            
                    eventNo += 1
                    if eventNo % 10000 == 0:
                        garbage_collect()
                    
                else:
                    print(f'Skipping line: "{line}"',file=stderr)
                    
                line = input.readline();
                if len(line) <= 0:
                    break;
    
    
        writeArray(output, 'headers.np', array(headers));



# ====================================================================
if __name__ == '__main__':
    from argparse import ArgumentParser
    from sys import stdin, stdout, stderr 
    
    ap = ArgumentParser('Convert zqq ROOT files to zip archives')
    ap.add_argument('input',type=str,help='Input filename')
    ap.add_argument('output',type=str,help='Output filename')
    ap.add_argument('--verbose','-v',type=int,default=0,
                    help='Be verbose')
    ap.add_argument('--level','-l',type=int,default=None,
                    help='Be verbose')

    args    = ap.parse_args()
    inFile  = stdin if args.input == '-' else open(args.input,'r')

    try:
        convert(inFile, args.output, args.verbose, args.level)
    except Exception as e:
        print(e,file=stderr)

        # Remove output file if there's an error 
        from os import unlink
        unlink(args.output)

    if not inFile is stdin:
        inFile.close()
