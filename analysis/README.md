# Example analyses 

The data analysed by these example analyses, in [Jupyter
Notebook](https://jupyter.org)s is 

- `zqqdaall.json` and 
- `zqqmcall.json` 

These are generated from the individual `zqq{mc}{91,92,93,94,95}.json`
files which in turn are produced by [`fillers.py`](../fillers.py), and
merged into one by [`merge.py`](../merge.py).  The application
`fillers.py` reads in a single event archive
`zqq{mc}{91,92,93,94,95}.zip` and generates the necessary histograms.

The analyses are 

- [`FitGGPi0`](FitGGPi0.ipynb) Fit $X\rightarrow\gamma\gamma$
  invariant mass spectrum around the $\pi^0$ mass 

- [`FitGGEta`](FitGGEta.ipynb) Fit $X\rightarrow\gamma\gamma$
  invariant mass spectrum around the $\eta0$ mass 

- [`FitPiPi`](FitPiPi.ipynb) Fit $X\rightarrow\pi^{\pm}\pi^{\mp}$
  invariant mass spectrum via template fits. 

- [`FitPiPi0`](FitPiPi0.ipynb) Dummy analysis.  The notebook merely
  loads the data and displays it.   It is up to the student to flesh
  out this analysis. 

- [`FitKPi`](FitKPi.ipynb) Dummy analysis.  The notebook merely
  loads the data and displays it.   It is up to the student to flesh
  out this analysis. 

The notebook uses tools from the [`pyzqq`](pyzqq) package as well as
other standard Python packages 

- [NumPy](https://numpy.org)
- [SciPy](https://scipy.org)
- [MatPlotLib](https://matplotlib.org)

as well as the statistical package 

- [`nbi_stat`](https://cholmcc.gitlab.io/nbi-python/statistics/nbi_stat/) 

which is based on the free (as in beer _and_ speech) book
[Statistics Overview - With
Python](https://cholmcc.gitlab.io/nbi-python/statistics/#Statistik),
available in both English and Danish.  If this package is not
available, one can install it by 

    pip install nbi_stat 

or sometimes 

    pip3 install nbi_stat 
    
