#
#  © 2023 Christian Holm Christensen
#  
#  Licensed under the GPL-3
#
ROOT		:= root 
ROOTFLAGS	:= -l
ROOTBATCH	:= -b -q
NEV		:= -1
ANA_NEV		:= -1
VERBOSE		:= 0

REALDATA	:= data/zqqda91.root		\
		   data/zqqda92.root		\
		   data/zqqda93.root		\
		   data/zqqda94.root		\
		   data/zqqda95.root
MCDATA		:= data/zqqmc91.root		\
		   data/zqqmc92.root		\
		   data/zqqmc93.root		\
		   data/zqqmc94.root		\
		   data/zqqmc95.root

FILLERS		:= pyzqq/filler.py		\
		   pyzqq/pipi.py		\
		   pyzqq/pipi0.py		\
		   pyzqq/gammagamma.py		

ROOTDATA	:= $(REALDATA) $(MCDATA)

#		   data/zqqmcb91.root		\
#		   data/zqqmcb92.root		\
#		   data/zqqmcb93.root		\

ZIPDATA		:= $(ROOTDATA:data/%.root=%.zip)
JSONFILES	:= $(ZIPFILES:%.zip=%.json)

vpath	%.root data

%.zip %.bzip2 %.lzma %.dat:%.root
	$(ROOT) $(ROOTFLAGS) $(ROOTBATCH) \
	   tools/Extract.C+\(\"$<\",$(NEV),$(VERBOSE)\) | \
		./tools/export.py - $(notdir $@) -v $(VERBOSE) $(LEVEL)

%.json: %.zip fillers.py 
	./fillers.py $< -o $@ -n $(ANA_NEV) -a all

%.plt:	%.json
	ipython3 -i ./plotFills.py $< 

all:	$(ZIPDATA)

json:	$(ZIPDATA:%.zip=%.json)

zqqdaall.json:$(REALDATA:data/%.root=%.json)
	./merge.py $@ $^

zqqmcall.json:$(MCDATA:data/%.root=%.json)
	./merge.py $@ $^


clean:
	rm -f  *~ pyzqq/*~ tools/*~ recoded/*~
	rm -f  tools/*_C_* tools/*_C.*
	rm -f  tools/*.log tools/*.aux tools/*.synctex* tools/*.pdf
	rm -f  recoded/*_C_* recoded/*_C.* recoded/*_h_* recoded/*_h.*
	rm -f  *.json
	rm -rf __pycache__
	rm -rf pyzqq/__pycache__
	rm -rf analysis/.ipynb_checkpoints
	rm -f  *.png

realclean: clean
	rm -f *.zip
	rm -f recoded/*.root 


checksizes:
	@for i in $(ZIPDATA) ; do \
	   if ! test -f $$i ; then \
	     echo "$$i missing" ; \
	   else \
             rf=data/`basename $$i .zip`.root ; \
	     zs=`du -k $$i  | cut -f1` ; \
	     rs=`du -k $$rf | cut -f1` ; \
	     rr=`echo "$$zs/$$rs" | bc -l` ; \
             ir=$$(($$zs/$$rs)) ; \
	     msg="" ; \
	     if test $$ir -lt 4; then msg=" remake!" ; fi ; \
	     printf "%-12s: %12d vs %12d -> %6.3f%s\n" \
	        $$i $$zs $$rs $$rr "$$msg"; \
	  fi ; done 

fillers.py:	$(FILLERS)

.PRECIOUS:	%.json
