#!/usr/bin/env python3
#
#  © 2023 Christian Holm Christensen
#  
#  Licensed under the GPL-3
#

def merge(files,output):
    from pyzqq.utils import loadResults, mergeResults, saveResults

    results = [loadResults(file) for file in files]
    merged  = mergeResults(*results)

    saveResults(output, merged)


if __name__ == '__main__':
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser(description='Merge results files')
    ap.add_argument('output',type=FileType('w'),help='Ouptut JSON file')
    ap.add_argument('input',type=FileType('r'),help='Input JSON files',
                    nargs='+')

    args = ap.parse_args();

    merge(args.input,args.output)

    
    
