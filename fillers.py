#!/usr/bin/env python3
#
#  © 2023 Christian Holm Christensen
#  
#  Licensed under the GPL-3
#
'''Application to run fillers of histograms.'''

from pyzqq import Looper
from pyzqq.pipi import PiPi
from pyzqq.pipi0 import PiPi0
from pyzqq.gammagamma import GammaGamma
from pyzqq.kpi import KPi
            
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType
    from pathlib import Path

    known = {
        'pipi': PiPi,
        'pipi0': PiPi0,
        'gammagamma': GammaGamma,
        'kpi': KPi
    }
    aliases = {
        'pp': 'pipi',
        'pp0': 'pipi0',
        'gg': 'gammagamma',
        'kp': 'kpi'
    }
    
    ap = ArgumentParser(description='Loop over data, example')
    ap.add_argument('input',type=str,help='Input event archive file')
    ap.add_argument('--output','-o',type=FileType('w'),help='Output JSON file',
                    default=None)
    ap.add_argument('--analyses','-a',type=str,help='Analyses to run',
                    default=['pipi'],nargs='+')
    ap.add_argument('-n','--nevents',type=int,default=-1,
                    help='Maximum number of events to loop over')

    args = ap.parse_args()
    inp  = args.input
    outp = args.output
    anas = [a.lower() for a in args.analyses]

    if 'all' in anas: anas = list(known.keys())
    
    if outp is None:
        outp = Path(inp).with_suffix('.json')

    try:
        from pyzqq import Looper
        
        looper = Looper(inp)
        for an in anas:
            ran = aliases.get(an,an)
            cls = known.get(ran,None)
            if cls is None:
                raise RuntimeError(f'Unknown analysis: {an}')

            print(f'Adding analysis {ran}')
            looper.addAnalysis(ran,cls())
            
        looper.loop(args.nevents)
        looper.finish(outp)

        print(f'Results written to {outp}')
    except Exception as e:
        from traceback import print_exc
        from sys import stderr, exit
        
        print(e,file=stderr)
        print_exc()

        outpp = Path(outp if isinstance(outp,str) or isinstance(outp,Path) else
                     outp.name)
        outpp.unlink(True)

        exit(1)
            
        
#
# EOF
#
