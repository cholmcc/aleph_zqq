/*
  © 2023 Christian Holm Christensen
  
  Licensed under the GPL-3
*/
#ifndef UTILS_C
#define UTILS_C

// -------------------------------------------------------------------
TH1* GetH1(TDirectory* d, const char* name, Color_t color)
{
  TObject* o = d->Get(name);
  if (not o) {
    Warning("GetH1", "Failed to find \"%s\" in \"%s\"",
	    name, d->GetName());
    d->ls();
    return 0;
  }
  if (not o->IsA()->InheritsFrom(TH1::Class())) {
    Warning("GetH1", "Object \"%s\" in \"%s\" is not a TH1 but a %s",
	    name, d->GetName(), o->ClassName());
    return 0;
  }
  TH1* h = static_cast<TH1*>(o);
  h->SetLineColor(color);
  h->SetMarkerColor(color);

  return h;
}

// -------------------------------------------------------------------
TFitResultPtr Fit(TH1* h, TF1* f)
{
  f->SetLineColor(h->GetLineColor());
  
  TFitResultPtr ret = h->Fit(f,"SENRQ");

  Printf("Fit of %s to %s, status %d",
	 f->GetName(), h->GetName(), ret->Status());
  Printf("chi^2/nu = %f / %d = %f (%5.1f%%)",
	 ret->Chi2(), ret->Ndf(), ret->Chi2()/ret->Ndf(),
	 ret->Prob()*100);
  for (Int_t i = 0; i < ret->NPar(); i++) {
    Printf("%2d %10s %9.5f +/- %9.5f", i,
	   ret->ParName(i).c_str(),
	   ret->Parameter(i),
	   ret->ParError(i));
  }
  return ret;
}

// -------------------------------------------------------------------
void DrawBackground(TF1*,TFitResultPtr res);

// -------------------------------------------------------------------
void DrawFit(TF1* f, TFitResultPtr res, Double_t ulx, Double_t uly)
{
  f->Draw("same");
  DrawBackground(f, res);
  
  TLatex* ltx = new TLatex(ulx,uly,
			   Form("#chi^{2}/#nu = %.1f / %d = %.1f (%.1f%%)",
				res->Chi2(), res->Ndf(),
				res->Chi2() / res->Ndf(),
				res->Prob()*100));
  ltx->SetTextAlign(13);
  ltx->SetTextColor(f->GetLineColor());
  ltx->SetTextSize(0.02);
  ltx->SetNDC();
  ltx->Draw();

  Double_t dy = 0.03;
  Double_t y  = uly - dy;
  Double_t x  = ulx;
  for (Int_t i = 0; i < res->NPar(); i++) {
    ltx->DrawLatex(x,y,      Form("%-10s", res->ParName(i).c_str()));
    ltx->DrawLatex(x+0.03,y, Form("%9.5f", res->Parameter(i)));
    if (not res->IsParameterFixed(i)) {
      ltx->DrawLatex(x+0.11,y, "#pm");
      ltx->DrawLatex(x+0.12,y, Form("%9.5f", res->ParError(i)));
    }
    y -= dy;
  }
}
#endif
//
// EOF
//
