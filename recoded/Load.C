/*
  © 2023 Christian Holm Christensen
  
  Licensed under the GPL-3
*/
void
Load()
{
  gROOT->LoadMacro("Filler.h+");
  gROOT->LoadMacro("FillGG.C+");
  gROOT->LoadMacro("FillPiPi.C+");
  gROOT->LoadMacro("FillPiPi0.C+");
  gROOT->LoadMacro("FillKPi.C+");
}

  
