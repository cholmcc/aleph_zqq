# Original C++ (ROOT based) code re-coded

The code in this directory reflects the original code by Peter Hansen,
except common utility member functions are used.

The original code suffered greatly from Fortrantitis[^1] and had a lot of
copy'n'paste'd  (spaghetti-code[^2]).  Here, the code has been cleaned-up
quite a bit and done more efficiently and expressively. 

One can argue that the original approach "gets the job done" albeit in
a convoluted fashion.  I would argue that due to the sprawling nature
of the original code, it is _very_ easy to make mistakes (as indeed
was the case) and _very_ hard to spot these problem.  For the sake of
clarity and to ensure that one does not produce bogus results, it is
_very_ important to structure ones code in a clear an concise
fashion. 

## Recoded 

In this recoding, we have made a number of changes 

- Introduce a base class [`Filler`](Filler.h) which serves multiple
  purposes 
  
  - Contains the event data, and steers loading of event data 
    - `Loop` Loop over defined input data 
    - `Add` Add data files to loop over 
  - Defines interface for specific fillers
    - `Init` Initialize filler 
    - `Event` Process a single event 
    - `Finish` end of job call 
  - Provides a number of service functions: 
    - `Select` selects events 
    - `Plot` show results of filling 
    - Various member functions to make histograms 
      - `H` Generic histogram
      - `M` Invariant mass histogram 
      - `X` Momentum fraction histogram
      - `MX` Invariant mass and momentum fraction histogram 
    - Various member functions to do common calculations 
      - `Side` side of 3-momentum relative to thrust axis 
      - `IsPID` check PID 
      - `IsGood` check track status 
      - `Length` length of 3-momentum 
      - `Energy` energy of 3-momentum plus mass 
      - `InvVector` V0 4-vector from 3- and 4-momenta 
      - `InvMass` V0 invariant mass from 3- and 4-momenta 
      - `InvVectorM` V0 4-vector from 3-momenta and masses
      - `InvMassM` V0 invariant mass from 3-momenta and masses 
      
    - `FindPi0Eta` find pi0 and eta V0's from photon-photon,
      photon-conversion, and conversion-conversion pairs 
      - `FitPi0` Fit pi0 invariant mass by optimising incoming momenta 
        - `ChiSquare` chi-square value for given momenta 
  
- Specific filler classes ([`FillGG`](FillGG.C),
  [`FillPiPi`](FillPiPi.C), [`FillPiPi0`](FillPiPi0.C), and
  [`FillKPi`](FillKPi.C)) that does calculations of invariant masses
  in various channels and fills histograms. 
  
  By using the service functions from `Filler` (as outlined above),
  these classes can be vastly simplified and streamlined, thus making
  the code much more transparent and easy to validate. 
  
  Also, since a lot of code has been moved into the base class, a lot
  of code duplication has been removed. 
  
  For example, originally the invariant mass of a two-particle
  combination would be determined in a block like 


        for (int i = 0; i < ntrack; i++) {
          if (type[i] != 1 and type[i] != 2) continue; 
           
          Float_t p1 = Length(momx[i],momy[i],momz[i]);
          Float_t e1 = TMath::Sqrt(p1 * p1 + pimass * pimass);
            
          for (int i = j + 1; < ntrack; j++) {
            if (type[j] != 1 and type[j] != 2) continue 
               
            if (type[i] == type[j]) {
              Float_t p2 = Length(momx[j],momy[j],momz[j]);
              Float_t e2 = TMath::Sqrt(p2 * p2 + pimass * pimass);
              Float_t px = momx[i] + momx[j];
              Float_t py = momy[i] + momy[j];
              Float_t pz = momz[i] + momz[j];
              Float_t e  = e1 + e2;
              Float_t p  = Length(px,py,pz);
              Float_t m  = TMath::Sqrt(e * e - p * p);
              Float_t x  = 2 * p / cmenergy;
	      	
	      	if (x > cut) 
                hist_same->Fill(m);
            }
            else {
              Float_t p2 = Length(momx[j],momy[j],momz[j]);
              Float_t e2 = TMath::Sqrt(p2 * p2 + pimass * pimass);
              Float_t px = momx[i] + momx[j];
              Float_t py = momy[i] + momy[j];
              Float_t pz = momz[i] + momz[j];
              Float_t e  = e1 + e2;
              Float_t p  = Length(px,py,pz);
              Float_t m  = TMath::Sqrt(e * e - p * p);
              Float_t x  = 2 * p / cmenergy;
	      	
	      	if (x > cut) 
                hist_oppo->Fill(m);
            }
          }
        }
	  
  and this structure could be replicated over and over again, even in
  a nested fashion.  Now, we use services from `Filler` to simplify
  this 
  
        for (int i = 0; i < ntrack; i++) {
          if (not IsPID(type[i],PID::piMinus) and 
	          not IsPID(type[i],PID::piPlus)) continue; 
           
          for (int i = j + 1; < ntrack; j++) {
            if (not IsPID(type[j],PID::piMinus) and 
  	            not IsPID(type[j],PID::piPlus)) continue 
               
	        Float_t px, py, pz, e;
	        Float_t m = InvVectorM(momx[i],momy[i],momz[i],pimass,
	                               momx[j],momy[j],momz[j],pimass,
	      						 px,     py,     pz,     e);
            Float_t x = 2 * Length(px,py,pz) / cmenergy;
            
	        if (x < cut) continue;
	        
	        Bool_t same = type[i] == type[j];
	        TH1*   h    = same ? hist_same : hist_oppo;
	        h->Fill(m);
          }
        }
	  
  Not only is the code shorter (and more efficient), but also much
  more expressive.  The call `InvVectorM` clearly demonstrates to the
  reader that we are calculating the V0 invariant vector and mass. 
  
  What's more, the member function `InvVectorM` is coded exactly
  _once_, and we can thoroughly check that it does the right
  calculation. 
  
  Since we do the same calculation between same and opposite sign, and
  the _only_ difference is which histogram to fill, it makes sense to
  use that distinction _only_ when actually filling the histograms.
  By doing so, we greatly reduce code-duplication and therefore the
  likeliness of errors when copy'n'pasting code and subsequent
  updates. 

[^1]: Fortrantitis is a serious affliction that much code written by
Physicists, young and old, suffer from.  The name is derived from the
favourite programming-language of yore of Physicists:
[Fortran](https://en.wikipedia.org/wiki/Fortran).  Not all code
written in Fortran suffers from Fortrantitis, but a heck of a lot
does.  The symptoms are: lack of structure; low code re-usability;
obscure variable names.

[^2]: [Spaghetti-code](https://en.wikipedia.org/wiki/Spaghetti_code) is
code that is all over the place and the flow of code is very hard to
follow.  Spaghetti-code is characterised by:  a lot of code
repetition, often a result of indiscriminate copy'n'paste; deep
nesting of control statements such as `if`, `for`, and `while`. 
