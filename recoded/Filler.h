/*
  © 2023 Christian Holm Christensen
  
  Licensed under the GPL-3
*/

#ifndef Filler_h
#define Filler_h
#include <TChain.h>
#include <TMath.h>
#include <TList.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TCanvas.h>
#include <THStack.h>
#include <TSystem.h>
#include <list>

// Header file for the classes stored in the TTree if any.

class Filler
{
public :
  constexpr static size_t  maxNpart  = 100;
  constexpr static size_t  maxV      =  10;
  constexpr static size_t  maxPhoton =  30;
  constexpr static Float_t piMass    = 0.139570; // pion mass
  constexpr static Float_t pi0Mass   = 0.135;    // neautral pion mass
  constexpr static Float_t k0Mass    = 0.497648; // K0 mass
  constexpr static Float_t kMass     = 0.493677; // K+- mass
  constexpr static Float_t etaMass   = 0.547;

  Float_t  pi0Min = 0.08;
  Float_t  pi0Max = 0.2;
  Float_t  etaMin = 0.52;
  Float_t  etaMax = 0.58;
  Float_t  kMin   = 0.48;
  Float_t  kMax   = 0.52;
  Float_t  maxCosTheta = 0.96;

  enum class PID {
    piPlus       = 1,
    piMinus      = 2,
    p            = 3,
    pBar         = 4,
    kPlus        = 5,
    kMinus       = 6,
    kZero        = 7,
    lambda       = 8,
    lambdaBar    = 9,
    conversion   = 11,
    gamma        = 12,
    ePlus        = 13,
    eMinus       = 14,
    muPlus       = 15,
    muMinus      = 16,
    d0           = 17,
    d0Bar        = 18,
    partialD0    = 19,
    partialD0Bar = 20,
    sigmaPlus    = 21,
    sigmaMinus   = 22,
    xiMinus      = 23,
    xiPlus       = 24
  };
  
  TChain* chain = nullptr;   //!pointer to the analyzed TTree or TChain
  TList*  list = nullptr;
  
  // Declaration of leaf types
  UChar_t         mcflavor;
  Float_t         cmenergy;
  Float_t         thru[3];
  Float_t         pquark[3];
  Float_t         pudsh1;
  Float_t         pudsh2;
  Float_t         b1nn;
  Float_t         b2nn;
  Float_t         ch1nn;
  Float_t         ch2nn;
  Float_t         pjet1[4];
  Float_t         pjet2[4];
  Float_t         pjet3[4];
  UShort_t        nbsv;
  Float_t         pxbsv[maxV];    //[nbsv]
  Float_t         pybsv[maxV];    //[nbsv]
  Float_t         pzbsv[maxV];    //[nbsv]
  Float_t         lbsv[maxV];     //[nbsv]
  Float_t         elbsv[maxV];    //[nbsv]
  Float_t         sumwsv[maxV];   //[nbsv]
  Float_t         chbsv[maxV];    //[nbsv]
  UChar_t         codebsv[maxV];  //[nbsv]
  UShort_t        npart;
  Float_t         pmomx[maxNpart];   //[npart]
  Float_t         pmomy[maxNpart];   //[npart]
  Float_t         pmomz[maxNpart];   //[npart]
  Float_t         pmass[maxNpart];   //[npart]
  Float_t         pflight[maxNpart]; //[npart]
  UShort_t        nphot;
  Float_t         pphotx[maxPhoton];  //[nphot]
  Float_t         pphoty[maxPhoton];  //[nphot]
  Float_t         pphotz[maxPhoton];  //[nphot]
  UChar_t         pbits[maxNpart];   //[npart]
  UChar_t         ptype[maxNpart];   //[npart]
  UChar_t         ptruth[maxNpart];  //[npart]
  UShort_t        pmoth[maxNpart];   //[npart]  
  Bool_t          mc = false;
  Long64_t        nEvents = 0;

  /** Reset all event data */
  void Reset()
  {
    mcflavor = 0;
    cmenergy = 0;
    pudsh1   = 0;
    pudsh2   = 0;
    b1nn     = 0;
    b2nn     = 0;
    ch1nn    = 0;
    ch2nn    = 0;
    for (size_t i = 0; i < 3; i++) {
      thru[i]   = 0;
      pquark[i] = 0;
    }
    for (size_t i = 0; i < 4; i++) {
      pjet1[i] = 0;
      pjet2[i] = 0;
      pjet3[i] = 0;
    }
    nbsv = 0;
    for (size_t i = 0; i < maxV; i++) {
      pxbsv  [i] = 0;
      pybsv  [i] = 0;
      pzbsv  [i] = 0;
      lbsv   [i] = 0;
      elbsv  [i] = 0;
      sumwsv [i] = 0;
      chbsv  [i] = 0;
      codebsv[i] = 0;
    }
    npart = 0;
    for (size_t i = 0; i < maxNpart; i++) {
      pmomx  [i] = 0;
      pmomy  [i] = 0;
      pmomz  [i] = 0;
      pmass  [i] = 0;
      pflight[i] = 0;
      pbits  [i] = 0;
      ptype  [i] = 0;
      ptruth [i] = 0;
      pmoth  [i] = 0;
    }
    nphot = 0;
    for (size_t i = 0; i < maxPhoton; i++) {
      pphotx[i] = 0;
      pphoty[i] = 0;
      pphotz[i] = 0;
    }
  }

  /** Constructor.

      This defines the chain and the branch addresses i.e., sets up
      data members to read data from the events into.
   */
  Filler()
    : chain(0),
      list(0),
      mc(false)
  {
    TBranch* dummy;
    chain   = new TChain("tree");
    chain->SetBranchAddress("mcflavor",      &mcflavor,      &dummy);
    chain->SetBranchAddress("cmenergy",      &cmenergy,      &dummy);
    chain->SetBranchAddress("thru",          thru,           &dummy);
    chain->SetBranchAddress("pquark",        pquark,         &dummy);
    chain->SetBranchAddress("pudsh1",        &pudsh1,        &dummy);
    chain->SetBranchAddress("pudsh2",        &pudsh2,        &dummy);
    chain->SetBranchAddress("b1nn",          &b1nn,          &dummy);
    chain->SetBranchAddress("b2nn",          &b2nn,          &dummy);
    chain->SetBranchAddress("ch1nn",         &ch1nn,         &dummy);
    chain->SetBranchAddress("ch2nn",         &ch2nn,         &dummy);
    chain->SetBranchAddress("pjet1",         pjet1,          &dummy);
    chain->SetBranchAddress("pjet2",         pjet2,          &dummy);
    chain->SetBranchAddress("pjet3",         pjet3,          &dummy);
    chain->SetBranchAddress("nbsv",          &nbsv,          &dummy);
    chain->SetBranchAddress("pxbsv",         pxbsv,          &dummy);
    chain->SetBranchAddress("pybsv",         pybsv,          &dummy);
    chain->SetBranchAddress("pzbsv",         pzbsv,          &dummy);
    chain->SetBranchAddress("lbsv",          lbsv,           &dummy);
    chain->SetBranchAddress("elbsv",         elbsv,          &dummy);
    //chain->SetBranchAddress("sumwsv",        sumwsv,         &dummy);
    chain->SetBranchAddress("chbsv",         chbsv,          &dummy);
    chain->SetBranchAddress("codebsv",       codebsv,        &dummy);
    chain->SetBranchAddress("npart",         &npart,         &dummy);
    chain->SetBranchAddress("pmomx",         pmomx,          &dummy);
    chain->SetBranchAddress("pmomy",         pmomy,          &dummy);
    chain->SetBranchAddress("pmomz",         pmomz,          &dummy);
    chain->SetBranchAddress("pmass",         pmass,          &dummy);
    chain->SetBranchAddress("pflight",       pflight,        &dummy);
    chain->SetBranchAddress("nphot",         &nphot,         &dummy);
    chain->SetBranchAddress("pphotx",        pphotx,         &dummy);
    chain->SetBranchAddress("pphoty",        pphoty,         &dummy);
    chain->SetBranchAddress("pphotz",        pphotz,         &dummy);
    chain->SetBranchAddress("pbits",         pbits,          &dummy);
    chain->SetBranchAddress("ptype",         ptype,          &dummy);
    chain->SetBranchAddress("ptruth",        ptruth,         &dummy);
    chain->SetBranchAddress("pmoth",         pmoth,          &dummy);   

    list = new TList;
  }
  /** Add all available data

      @param mc If true, read in MC data
   */
  void AddAll(Bool_t mc)
  {
    if (mc) {
      Info("Looper","Analysing Z->qq(bar) MC");
      Add("../data/zqqmc91.root");
      Add("../data/zqqmc92.root");
      Add("../data/zqqmc93.root");
      Add("../data/zqqmc94.root");
      Add("../data/zqqmc95.root");
    }
    else {
      Info("Looper","Analysing Z->qq(bar) DATA");
      Add("../data/zqqda91.root");
      Add("../data/zqqda92.root");
      Add("../data/zqqda93.root");
      Add("../data/zqqda94.root");
      Add("../data/zqqda95.root");
    }

  }
  /** Add a single file to be read */
  virtual void Add(const char* name)
  {
    Info("Add", "Adding file \"%s\" to chain",name);
    chain->Add(name);
  }
  /** @{
      @name Processing member functions
  */
  /** Interface member function.  Derived classes should override this
      member function to define histograms and the like. */
  virtual void   Init() = 0;
  /** Interface member function.  Derived classes should override this
      member function to do single event processing */ 
  virtual Bool_t Event() = 0;
  /** Select and event.  The criteria are

      - sqrt(s) between 90.8 and 91.6
      - Thrust axis z coordinate less than 0.8
      - Total jet energy 32 GeV
      - At least minGood good tracks

      @param minGood Least number of good tracks

      @return true if the event was selected
  */
  virtual Bool_t Select(Int_t minGood=2) {
    // Only Z peak data is kept
    if (cmenergy < 90.8 || cmenergy > 91.6) return false;
    if (TMath::Abs(thru[2]) > 0.8)          return false;

    // count good tracks:
    //
    //  - at least 7 hits in the TPC		 
    // 	- hit in at least one layer of silicon
    int ngood=0;
    int ntracks = 0;
    for( int i=0; i< npart; i++) {   //    
      if(IsGood(pbits[i])) {
	ngood++;//  good primary vertex probability
	ntracks++;
      }
    } 
    if (ngood < minGood) return false;
    // Check sum jet energy - suppress Z -> tau tau-bar
    if (pjet1[3] + pjet2[3] + pjet3[3] < 32.) return false;

    return true;
  }
  /** Do the actucal loop over the data.  This calls `Init` on start,
      `Finish` on end, and `Event` for each event.  Events are
      selected by a call to th `Select`.

      @param output Output filename
      @param maxEntry Maximum number of events to process. If
                      negative, process all available events.
  */
  virtual void Loop(const char* output, Long64_t maxEntry=-1) {
    if (!chain) return;

    TString outName(output);
    
    // Create histograms and stuff 
    Init();
    list->ls();
    
    Info("Loop","Chain has %lld entries, asked for %lld",
	 chain->GetEntries(), maxEntry);
    maxEntry = maxEntry > 0 ? maxEntry : 0xFFFFFFFF;
    maxEntry = TMath::Min(maxEntry,chain->GetEntries());

    for (Long64_t jentry = 0; jentry < maxEntry; jentry++) {
      Reset();

      Int_t nb = chain->GetEntry(jentry);
      if (nb <= 0) break;

      mc = mcflavor != 0;
      if (not Select()) continue;
      nEvents++;

      if (jentry % 1000 == 0)
	Info("Event","%6lld of %6lld, %6lld accepted so far",
	     jentry, maxEntry, nEvents);

      if (not Event()) continue;
    }

    Finish(outName);
  }
  /** Plot the result of the analysis. */
  virtual void Plot()
  {
    TObject* i = list->FindObject("invM");
    if (not i) return;
    THStack* s = dynamic_cast<THStack*>(i);
    if (not s) return;
    
    TCanvas* c = new TCanvas("c","c",1600,1000);
    c->SetTopMargin(0.05);
    c->SetRightMargin(0.20);
    s->Draw("nostack");
    s->GetHistogram()->SetXTitle("#it{m}_{inv}");
    s->GetHistogram()->SetYTitle("d#it{N}/d#it{m}_{inv}");
    c->BuildLegend(1-c->GetRightMargin(),
		   c->GetBottomMargin(),
		   .99,
		   1-c->GetTopMargin());

    c->Write();
  }
  /** Called at end of processing.  By default stores the result in
      the named file

      @param output Filename of file to store results in. 
  */ 
  virtual void Finish(const TString& output)
  {
    for (auto* o : *list) {
      if (not o->IsA()->InheritsFrom(TH1::Class())) continue;
      TH1* h = static_cast<TH1*>(o);
      h->Scale(1./nEvents, "width");
    }
    
    Info("Finish","Will write to \"%s\"",output.Data());
    TFile* outFile = TFile::Open(output.Data(),"RECREATE");
    outFile->cd();
    list->Write();

    Plot();
    
    outFile->Write();
    Info("Finish","Wrote output to \"%s\"",output.Data());
      
  }
  /** Run the whole thing.  The output file name is derived from the
      input.

      @param input    - Input file.  If `all` then add all files
      @param prefix   - Prefix on output file 
      @param maxEntry - Maximun number of events to process
  */
  void DoRun(const TString& input,
	     const TString& prefix,
	     Long64_t maxEntry=-1)
  {
    TString output = "all.root";
    if (input.IsNull() or input.BeginsWith("all")) 
      AddAll(input.Contains("mc"));
    else 
      Add(input);
    
    output = prefix+"_"+gSystem->BaseName(input.Data());
    output.ReplaceAll(".root","").Append(".root");

    Loop(output,maxEntry);
  }
  /** @} */
  /**
     @{
     @name Member functions to make histograms
  */
  /** Set attributes on a histogram

      @param h      Histogram to manipulate
      @param color  Line, marker, and fill (if fill is non-zero) colour
      @param marker Marker style
      @param fill   Fill style
  */
  TH1* A(TH1* h, Color_t color, Style_t marker, Style_t fill=0)
  {
    h->SetLineColor(color);
    h->SetFillColor(fill == 0 ? 0 : color);
    h->SetFillStyle(fill);
    h->SetMarkerColor(color);
    h->SetMarkerStyle(marker);
    return h;
  }
  /** Create a histogram with equidistant bins.  The histogram is
      added to the output list.

      @param name     Name of histogram
      @param title    Title of histogram
      @param nBins    Number of bins
      @param xMin     Least binned value
      @param xMax     Largest binned value
      @param color    Line, marker, and fill colour
      @param marker   Marker style

      @return Newly created histogram. 
  */
  TH1* H(const char* name,
	 const char* title,
	 Int_t       nBins,
	 Float_t     xMin,
	 Float_t     xMax,
	 Color_t     color=kRed+1,
	 Style_t     marker=20)
  {
    TH1* h = new TH1F(name, title, nBins, xMin, xMax);
    list->Add(h);
    return A(h, color, marker);
  }
  /** Create a histogram with variable bins. The histogram is added to
      the output list.

      @param name     Name of histogram
      @param title    Title of histogram
      @param nBins    Number of bins
      @param xBins    Array of `nBins+1` bin boundaries 
      @param color    Line, marker, and fill colour
      @param marker   Marker style 
  */
  TH1* H(const char* name,
	 const char* title,
	 Int_t       nBins,
	 Float_t*    xBins,
	 Color_t     color=kRed+1,
	 Style_t     marker=20)
  {
    TH1* h = new TH1F(name, title, nBins, xBins);
    list->Add(h);
    return A(h, color, marker);
  }
  /** Create a histogram of invariant masses. The histogram is
      added to the output list.
      
      @param name     Name of histogram
      @param title    Title of histogram
      @param nBins    Number of bins
      @param xMin     Least binned value
      @param xMax     Largest binned value
      @param color    Line, marker, and fill colour
      @param marker   Marker style

      @return Newly created histogram. 
  */
  TH1* M(const char* name,
	 const char* title,
	 Int_t       nBins,
	 Float_t     xMin,
	 Float_t     xMax,
	 Color_t     color=kRed+1,
	 Style_t     marker=20)
  {
    TH1* h = H(name,title,nBins,xMin,xMax,color,marker);
    h->SetXTitle("#it{m}_{inv}");
    h->SetYTitle("d#it{N}/d#it{m}_{inv}");
    return h;
  }
  /** Create a histogram of momentum fractions. The histogram is
      added to the output list.
      
      @param name     Name of histogram
      @param title    Title of histogram
      @param nBins    Number of bins
      @param xBins    Array of `nBins+1` bin boundaries 
      @param color    Line, marker, and fill colour
      @param marker   Marker style

      @return Newly created histogram. 
  */
  TH1* X(const char* name,
	 const char* title,
	 Int_t       nBins,
	 Float_t*    xBins,
	 Color_t     color=kRed+1,
	 Style_t     marker=20)
  {
    TH1* h = H(name,title,nBins,xBins,color,marker);
    h->SetXTitle("#it{x}");
    h->SetYTitle("d#it{N}/d#it{x}");
    return h;
  }
  /** Create a 2D histogram of invariant massses and momentum
      fractions. The histogram is added to the output list.
      
      @param name     Name of histogram
      @param title    Title of histogram
      @param nM       Number of invariant mass bins 
      @param mMin     Least invariant mass 
      @param mMax     Largest invariant mass
      @param nX       Number of momentum fraction bins 
      @param xBins    Array of `nBins+1` x bin boundaries 
      @param color    Line, marker, and fill colour
      @param marker   Marker style

      @return Newly created histogram. 
  */
  TH2* MX(const char* name,
	  const char* title,
	  Int_t       nM,
	  Float_t     mMin,
	  Float_t     mMax,
	  Int_t       nX,
	  Float_t*    xBins,
	  Color_t     color=kRed+1,
	  Style_t     marker=20)
  {
    Double_t xx[nX+1];
    for (Int_t i = 0; i < nX+1; i++) xx[i] = xBins[i];
    TH2* h = new TH2F(name,title,nM,mMin,mMax,nX,xx);
    list->Add(h);
    h->SetXTitle("#it{m}_{inv}");
    h->SetYTitle("#it{x}");
    h->SetZTitle("d^{2}#it{N}/(d#it{m}_{inv}d#it{x})");
    A(h,color,marker);
    return h;
  }
  /** @} */
  /** @{
      @name Analysis utilities
  */
  /** Check particle type */
  static Bool_t IsPID(UChar_t type, PID test)
  {
    return PID(type) == test;
  }
  /** Check particle status */
  static Bool_t IsGood(UChar_t bits)
  {
    return bits < 4;
  }
  /** Calculate side of vector releative to thrust axis */
  Int_t Side(Float_t px, Float_t py, Float_t pz) const
  {
    return Int_t(TMath::Sign(1, px * thru[0]+ py * thru[1]+pz * thru[2]));
  }
  /** Calculate length of a three-vector */
  static Float_t Length(Float_t px, Float_t py, Float_t pz)
  {
    return TMath::Sqrt(px*px+py*py+pz*pz);
  }
  /** Calculate the energy of a particle */
  static Float_t Energy(Float_t px, Float_t py, Float_t pz, Float_t m)
  {
    return TMath::Sqrt(TMath::Power(px,2)+
		       TMath::Power(py,2)+
		       TMath::Power(pz,2)+
		       TMath::Power(m, 2));
  }
  /** Calculate invariant 3-vector */
  static Float_t InvVector(Float_t px1, Float_t py1, Float_t pz1,
			   Float_t px2, Float_t py2, Float_t pz2,
			   Float_t &px, Float_t &py, Float_t &pz)
  {
    Float_t p1 = Length(px1,py1,pz1);
    Float_t p2 = Length(px2,py2,pz2);
    px         = px1 + px2;
    py         = py1 + py2;
    pz         = pz1 + pz2;
    Float_t pp = px * px + py * py + pz * pz;
    return TMath::Sqrt((p1 + p2) * (p1 + p2) - pp); 
  }
  /** Calculate invariant mass from 3-vectors */
  static Float_t InvMass(Float_t px1, Float_t py1, Float_t pz1,
			 Float_t px2, Float_t py2, Float_t pz2)
  {
    Float_t px, py, pz;
    return InvVector(px1,py1,pz1,
		     px2,py2,pz2,
		     px, py, pz);
  }
  /** Calculate invariant 4-vector */
  static Float_t InvVector(Float_t px1, Float_t py1, Float_t pz1, Float_t e1,
			   Float_t px2, Float_t py2, Float_t pz2, Float_t e2,
			   Float_t &px, Float_t &py, Float_t &pz, Float_t &e)
  {
    px         = px1 + px2;
    py         = py1 + py2;
    pz         = pz1 + pz2;
    e          = e1  + e2;
    Float_t pp = px * px + py * py + pz * pz;
    return TMath::Sqrt(e * e - pp);
  }
  /** Calculate invariant 4-vector from 3-vectors and masses */
  static Float_t InvVectorM(Float_t px1, Float_t py1, Float_t pz1, Float_t m1,
			    Float_t px2, Float_t py2, Float_t pz2, Float_t m2,
			    Float_t &px, Float_t &py, Float_t &pz, Float_t &e)
  {
    Float_t e1 = Energy(px1,py1,pz1,m1);
    Float_t e2 = Energy(px2,py2,pz2,m2);
    return InvVector(px1,py1,pz1,e1,
		     px2,py2,pz2,e2,
		     px, py, pz, e);
  }
  /** Calculate invariant mass from 4-vectors */
  static Float_t InvMass(Float_t px1, Float_t py1, Float_t pz1, Float_t e1,
			 Float_t px2, Float_t py2, Float_t pz2, Float_t e2)
  {
    Float_t px,py,pz,e;
    return InvVector(px1,py1,pz1,e1,
		     px2,py2,pz2,e2,
		     px, py, pz, e);
  }
  /** Calculate invariant mass from 3-vectors and masses */
  static Float_t InvMassM(Float_t px1, Float_t py1, Float_t pz1, Float_t m1,
			  Float_t px2, Float_t py2, Float_t pz2, Float_t m2)
  {
    Float_t px, py, pz, e;
    return InvVectorM(px1,py1,pz1,m1,
		      px2,py2,pz2,m2,
		      px, py, pz, e);
  }
  /** @} */
  /** @{
      @name Specific service methods
  */
  /** Calculate chi-square of fit to pi0 invariant mass

      @param p1    Estimate of momentum of first photon
      @param p2    Estimate of momentum of second photon
      @param p1m   Found momentum of first photon
      @param p2m   Found momentum of second photon
      @param s1sq  Variance on p1m
      @param s2sq  Variance on p2m
      @param angle Angle between photons

      @return chi-square for (p1,p2)
  */
  static Float_t ChiSquare(Float_t p1,
			   Float_t p2,
			   Float_t p1m,
			   Float_t p2m,
			   Float_t s1sq,
			   Float_t s2sq,
			   Float_t angle) {
    // chi-squared for photon energies p1 and p2, when constraining
    // them to have the pi0 mass (0.135 GeV) assuming
    // 
    //   Measured energies p1m and p2m with squared errors s1sq and
    //   s2sq
    //   
    //   The error on the opening angle is negligible
    //
    Float_t d1sq  = (p1-p1m)*(p1-p1m);
    Float_t d2sq  = (p2-p2m)*(p2-p2m);
    Float_t a     = 0.135*0.135/2.0/(1.0-TMath::Cos(angle));
    // This is the mass constraint term and its error		      
    Float_t d12sq = (p1*p2-a)*(p1*p2-a);           
    Float_t s12sq = p1*p1*d2sq+p2*p2*d1sq;         
    // We minimise this: the p1 and p2 pulls from their measured values
    // plus the mass pull from the pi0 mass
    return d1sq/s1sq + d2sq/s2sq + d12sq/s12sq;
  }
  /** Fit the momentum of incoming gammas in a pi0 vertex

      Do a primitive brute-force mass-constrained chi-squared
      mimimalization The fitted parameters are the two photon energies
      These are contrained by the measured energies and the known pi0
      mass performed on photon pairs with initial mass in an range of
      about +- 2*sigma from the pi0 mass.  we assume an energy
      resolution

         Delta(E)/E = 0.18/sqrt(E) +0.009
  */
  static Float_t FitPi0(Float_t& p1, Float_t& p2, Float_t angle)
  {
    float chi2min = 1.0e24;
    float p1min   = p1;
    float p2min   = p2;
    float s1sq    = 0.18*0.18*p1+0.009*0.009*p1*p1;
    float s2sq    = 0.18*0.18*p2+0.009*0.009*p1*p1;
    float p1start = p1-TMath::Sqrt(s1sq)*2.0;
    float p2start = p2-TMath::Sqrt(s2sq)*2.0;
    float p1end   = p1+TMath::Sqrt(s1sq)*2.0;
    float p2end   = p2+TMath::Sqrt(s2sq)*2.0;
    float step1   = (p1end-p1start)/50.;
    float step2   = (p2end-p2start)/50.;

    for(int istep1 = 0; istep1 < 50; istep1++) {
      float p1fit = p1start + step1 * istep1;
      for(int istep2 = 0; istep2 < 50; istep2++) {
	float p2fit = p2start + step2 * istep2;
	float chi2  = ChiSquare(p1fit,p2fit,p1,p2,s1sq,s2sq,angle);
	if(chi2<chi2min) {
	  chi2min=chi2;
	  p1min=p1fit;
	  p2min=p2fit;
	}	
      }
    }
    p1 = p1min;
    p2 = p2min;
    return chi2min;
  }
  /** Structure to hold found pi0 and etas */
  struct Pi0Eta {    
    Float_t px;
    Float_t py;
    Float_t pz;
    Float_t m;
    Float_t chi2;
    Char_t  side;
    UChar_t type; // 0x0: gg, 0x1: ge, 0x2: ee
    Bool_t  isEta;
    Bool_t  best;

    Pi0Eta(Float_t px, Float_t py, Float_t pz, Float_t m,
	   Float_t chi2, Char_t side, UChar_t type, Bool_t isEta,
	   Bool_t best)
      : px   (px),
	py   (py),
	pz   (pz),
	m    (m),
	chi2 (chi2),
	side (side),
	type (type),
	isEta(isEta),
	best (best)
    {}
    void Print() const
    {
      Printf("%3s (%6.3f,%6.3f,%6.3f) m=%6.3f chi2=%6.3f side=%2d type=%2d best=%d",
	     (isEta ? "eta" : "pi0"), px, py, pz, m, chi2, side, type, best);
    }
  };
  /** List of eta or pi0s */
  using Pi0Etas=std::list<Pi0Eta>;
  /** Find all pi0 and etas from gamma-gamma and possibly from
      gamma-conversion and conversion-conversion pairs

      If onlyBest == true, we will only store one possible
      combination, and that combination is flagged as either pi0 or
      eta.  Note that if the first photon can combine with one or more
      other photons and these fall within the cuts for both pi0 and
      eta, then the pi0 combination is chosen.
      
      If onlyBest == false, then we store all combinations, including
      the best combination (least chi2 for pi0, invariant mass closest
      to eta mass for eta), with the caveats mentioned above.  Note,
      in this case, the best combination will be present twice in
      output list: once with best == false and once with best == true.
      Thus, if one can do
      
         if   (pi0eta.best) h_best->Fill(pi0eta.m);
         else               h_all ->Fill(pi0eta.m);
      
      That is, we _must_ make the selection exclusive to not
      double-count.
      
      If the least chi^2 is set to a negative number, then we do not
      do a refit of the incoming momenta, and all possible
      combinations are stored as pi0.
      
      @param l        List to fill with pi0 and eta
      @param minChi2  Least chi2 to store.  If negative do not re-fit.
      @param onlyBest Only store best estimate pi0 or eta
      @param onlyGG   Only do gamma-gamma (not photon conversion)
  */
  void FindPi0Eta(Pi0Etas& l,
		  Float_t  minChi2=3,
		  Bool_t   onlyBest=false,
		  Bool_t   onlyGG=false)
  {
    /** Empty list */
    l.clear();
    
    // look for photons
    for(int i = 0; i < nphot - 1; i++) {
      float p1 = Length(pphotx[i],pphoty[i],pphotz[i]);

      // Photons are badly measured below 2 GeV, so following cut is a
      // possibility.
      // 
      // if(p1 < 2) continue; 

      // Side relative to thrust axis 
      Int_t s1 = Side(pphotx[i],pphoty[i],pphotz[i]);
	
      // Now start to look for photons.
      //
      Float_t pi0chi2 = 1.0e24;
      Float_t pi0px   = 0;
      Float_t pi0py   = 0;
      Float_t pi0pz   = 0;
      Float_t pi0m    = 0;
      Float_t etapx   = 0;
      Float_t etapy   = 0;
      Float_t etapz   = 0;
      Float_t etam    = 0;		

      for(int j = i + 1;j < nphot; j++) {
	// Same side 
	if (s1 * Side(pphotx[j],pphoty[j],pphotz[j]) < 0) continue;
	Float_t p2 = Length(pphotx[j],pphoty[j],pphotz[j]);
	Float_t px, py, pz;
	Float_t invM = InvVector(pphotx[i],pphoty[i],pphotz[i],
				 pphotx[j],pphoty[j],pphotz[j],
				 px, py, pz);

	Bool_t pi0Cand = invM > pi0Min and invM < pi0Max;
	Bool_t etaCand = invM > etaMin and invM < etaMax;

	// Store as pi0 if asked to store all candidates 
	if (not onlyBest)
	  // px,py,pz,m,chi2,side,type,eta,best
	  l.emplace_back(px,py,pz,invM,-1,s1,
			 0x0,not pi0Cand and etaCand,false);

	// If no minimum chi2 is given, then go on to next pair.  That
	// is, we storing _all_ candidates.
	if (minChi2 < 0) continue;

	// If we get here, then we're refitting the momemta 
	Float_t angle   = TMath::ACos(1.0 - invM * invM/(2 * p1 * p2)); 
	Float_t p1min   = p1;
	Float_t p2min   = p2;

	// Try to do a refit if candidate for pi0 
	if (pi0Cand) {
	  Float_t chi2 = FitPi0(p1min, p2min, angle);

	  Float_t f1 = p1min / p1;
	  Float_t f2 = p2min / p2;
	  px         = pphotx[i] * f1 + pphotx[j] * f2;
	  py         = pphoty[i] * f1 + pphoty[j] * f2;
	  pz         = pphotz[i] * f1 + pphotz[j] * f2;
	  Float_t pp = Length(px, py, pz);
	  invM       = TMath::Sqrt(TMath::Power(p1min + p2min,2)-
				   TMath::Power(pp, 2));
	  if (chi2 < minChi2) {
	    if (not onlyBest)
	      // px,py,pz,m,chi2,side,type,eta,best
	      l.emplace_back(px,py,pz,invM,chi2,s1,0x0,false,false);
	    if (chi2 < pi0chi2) {
	      pi0chi2    = chi2;
	      pi0px      = px;
	      pi0py      = py;
	      pi0pz      = pz;
	      pi0m       = invM;
	    }
	  } // chi2 < minChi2
	} // within pi0 mass
	
	// Check if withing eta mass and closer to previsously found 
	else if (etaCand and 
		 TMath::Abs(invM-etaMass) < TMath::Abs(etam-etaMass)) {
	  etapx   = px;
	  etapy   = py;
	  etapz   = pz;
	  etam    = invM;
	} // within eta mass range 
      } //end loop over second photon

      // case of good pi0 hypothesis for the two photons
      Bool_t isPi0 = pi0m > pi0Min and pi0m < pi0Max;
      Bool_t isEta = not isPi0 and TMath::Abs(etam-etaMass) < 0.3;
      
      if (isPi0 or isEta) 
	// px,py,pz,m,chi2,side,type,eta,best
	l.emplace_back(isPi0 ? pi0px   : etapx,
		       isPi0 ? pi0py   : etapy,
		       isPi0 ? pi0pz   : etapz,
		       isPi0 ? pi0m    : etam,
		       isPi0 ? pi0chi2 : -1,
		       s1,
		       0x0, 
		       isEta,
		       true);

      // if we're not combining photons with photons from conversion,
      // continue to next photon.
      if (onlyGG) continue;

      // Look for a converted photon
      for (int j = 0; j < npart; j++) {
	if (not IsPID(ptype[j],PID::conversion)) continue;
	if (s1 * Side(pmomx[j],pmomy[j],pmomz[j]) < 0) continue;
	Float_t px, py, pz;
	Float_t invM = InvVector(pphotx[i],pphoty[i],pphotz[i],
				 pmomx[j], pmomy[j], pmomz[j],
				 px, py, pz);
	// px,py,pz,m,chi2,side,type,eta,best
	l.emplace_back(px,
		       py,
		       pz,
		       invM,
		       -1,
		       s1,
		       0x1,
		       false,
		       false);
      }
    } // Loop over first photon

    if (onlyGG) return;

    // look also cases where both the photons has converted in the
    // detector to an e+e- pair
    for (int i = 0; i < npart; i++) {
      if (not IsPID(ptype[i],PID::conversion)) continue;
      Int_t s1 = Side(pmomx[i],pmomy[i],pmomz[i]);
      
      //look for a converted photon
      for (int j = i+1; j < npart; j++) {
	if(not IsPID(ptype[j],PID::conversion)) continue;
	if(s1 * Side(pmomx[j],pmomy[j],pmomz[j]) < 0) continue;

	Float_t px, py, pz;
	Float_t invM = InvVector(pmomx[i], pmomy[i], pmomz[i],
				 pmomx[j], pmomy[j], pmomz[j],
				 px, py, pz);

	// px,py,pz,m,chi2,side,type,eta,best
	l.emplace_back(px,
		       py,
		       pz,
		       invM,
		       -1,
		       s1,
		       0x2,
		       false,
		       false);
      }
    }
  }
  /** @} */
};

#endif

