# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3 
'''Base class for analysers'''


class Filler:
    class TrackStatus :
        '''Status flags on tracks'''
        positive    =   0x1
        '''Positive electric charge (redundant?)'''
        missingHits =   0x2
        '''Some missing ITS hits'''
        failedDCA   =   0x4
        '''Cut on Distance of Closest Approach to IP failed'''
        lepton      =   0x8
        '''Failed lepton cut'''
        missingTPC  =  0x10
        '''Missing hits in the TPC'''
        lowIP       =  0x20
        '''Low IP resolution'''
        decay       =  0x40
        '''From decay in-flight'''
        secondary   =  0x80
        '''Produced in material'''
        conversion  = 0x100
        '''electron/positron from gamma-conversion'''
        partial     = 0x200
        '''Partial D0, ...'''

        standardBad = (failedDCA  |
                       lepton     |
                       missingTPC |
                       lowIP      |
                       decay      |
                       secondary)
        
    pimMass    = 0.13957  # charged pion mass in GeV
    pi0Mass    = 0.135    # neutral pion mass in GeV
    etaMass    = 0.547    # eta mass in GeV
    k0Mass     = 0.497648 # K0 mass
    kMass      = 0.493677 # K+- mass
    lambdaMass = 1.11568  # Lambda mass
    
    def __init__(self):
        '''Constructor'''
        pass 

    def setIndexes(self,
                   trackIndex,  
                   jetIndex,    
                   headerIndex, 
                   v0Index,     
                   photonIndex):
        '''Called when we get a new file to read from'''
        self._trackIndex  = trackIndex
        self._jetIndex    = jetIndex
        self._headerIndex = headerIndex
        self._v0Index     = v0Index
        self._photonIndex = photonIndex 


    def beginLoop(self,mc):
        '''Called at the begining of loop to define histograms, etc.

        Parameters
        ----------
        mc : bool
            True if looping over simulated (so-called Monte-Carlo) data.
        '''
        # Use that mcFlavour is non-zero for MC data 
        self._isMC = mc
    
    def event(self,event):
        '''Called for each event.  Must be overriden.

        Parameters
        ----------
        event : Event
            Event structure.  Derived classes can use this object to
            access tracks, photons, V0s, jets, and so on.
        '''
        raise RuntimeError('Event member function not defined')


    def results(self):
        '''Get results as a dictionary, should be overriden'''
        return {}

    @property
    def headerIndex(self):
        '''Return namespace of header indexes'''
        return self._headerIndex
    
    @property
    def trackIndex(self):
        '''Return namespace of track indexes'''
        return self._trackIndex
    
    @property
    def photonIndex(self):
        '''Return namespace of photon indexes'''
        return self._photonIndex
    
    @property
    def jetIndex(self):
        '''Return namespace of jet indexes'''
        return self._jetIndex
    
    @property
    def v0Index(self):
        '''Return namespace of v0 indexes'''
        return self._v0Index

    # ----------------------------------------------------------------
    # Header utilities
    def headerSqrtS(self,header):
        '''Returns the collision energy

            sqrt(s)

        in GeV'''
        return header[self.headerIndex.cmEnergy]
    
    # ----------------------------------------------------------------
    # Track utilities 
    def trackPdgIn(self,tracks,pdgs):
        '''Check if track is one of the particle types listed

        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate
        pdgs : numpy.ndarray shape=(M)
            PDGs to check for 
        Returns
        -------
        mask : numpy.ndarray shape=(N), dtype=bool
            Mask of tracks that meet criterion 
        '''
        from numpy import isin 

        return isin(tracks[:,self.trackIndex.type],pdgs)

    def trackStatus(self,tracks):
        '''Get track status flags

        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate

        Returns
        -------
        status : numpy.ndarray shape(N)
            The track statuses 
        '''
        return tracks[:,self.trackIndex.status]

    def trackSquareMomentum(self,tracks):
        '''Calculate square momentum of tracks

            (px^2 + py^2 + pz^2)

        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate

        Returns
        -------
        p2 : numpy.ndarray shape=(N)
            The square momentum of tracks 
        '''
        return (tracks[:,[self.trackIndex.px,
                          self.trackIndex.py,
                          self.trackIndex.pz]]**2).sum(axis=1)
    
    def trackSquareTransverseMomentum(self,tracks):
        '''Calculate square transverse momentum of tracks

            (px^2 + py^2)
        
        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate

        Returns
        -------
        pt2 : numpy.ndarray shape=(N)
            The square tranverse momentum of tracks 
        
        '''
        return (tracks[:,[self.trackIndex.px,
                          self.trackIndex.py]]**2).sum(axis=1)

    def trackMomentumFraction(self,tracks,header):
        '''Returns the track momentum fraction (x)

            2 * sqrt(px^2+py^2+pz^2) / sqrts

        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate

        Returns
        -------
        x : numpy.ndarray shape=(N)
            The momentum fraction of tracks         
        '''
        from numpy import sqrt 
        return 2 * sqrt(self.trackSquareMomentum(tracks)) / \
            self.headerSqrtS(header)
        
    
    def trackSquareCosTheta(self,tracks):
        '''Calculate square cos(theta) of tracks

            pz^2 / p^2
        
        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate


        Returns
        -------
        cosTheta2 : numpy.ndarray shape=(N)
            The square costine theta of tracks 
        
        '''
        from numpy import abs, sqrt
    
        return (tracks[:,self.trackIndex.pz]**2 /
                self.trackSquareMomentum(tracks))
    
    def trackSide(self,tracks,header):
        '''Side of tracks relative to thrust axis

            sign(px * tx + py * ty + pz * tz)
        
        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate
        header : numpy.ndarray shape=(12)
            Event header. Thrust axis taken from here 

        Returns
        -------
        side : numpy.ndarray shape=(N)
            The side of the tracks (-1,0,1)
        
        '''
        from numpy import sign
        if tracks is None or len(tracks) < 1: return []
        
        return sign((tracks[:,[self.trackIndex.px,
                               self.trackIndex.py,
                               self.trackIndex.py]] *
                     header[[self.headerIndex.thrustAxisX,
                             self.headerIndex.thrustAxisY,
                             self.headerIndex.thrustAxisZ]]).sum(axis=1))
    
    def trackFourVector(self,tracks):
        '''Get the momentum four-vector of a set of tracks

            {px,py,pz,E}
        
        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate

        Returns
        -------
        fourVector : numpy.ndarray shape(N,4)
            The four vector tracks 
        '''
        return tracks[:,[self.trackIndex.px,
                         self.trackIndex.py,
                         self.trackIndex.pz,
                         self.trackIndex.e]]
    
    def trackPDG(self,tracks):
        '''Get the PDG of tracks

        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate

        Returns
        -------
        pdgs : numpy.ndarray shape(N,9)
            The PDG (particle type) codes of tracks 
        
        '''
        return tracks[:,self.trackIndex.type]
    
    def trackMcPDG(self,tracks):
        '''Get the MC PDG code of tracks

        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate

        Returns
        -------
        mcPdgs : numpy.ndarray shape(N,9)
            The MC ("truth") PDG (particle type) codes of tracks 
        
        '''
        return tracks[:,self.trackIndex.mcType]
    
    def trackMother(self,tracks):
        '''Get the MC PDG of mother particles of tracks

        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate

        Returns
        -------
        motherPdgs : numpy.ndarray shape(N,9)
            The MC ("truth") PDG (particle type) codes of mother
            particles of tracks
        '''
        return tracks[:,self.trackIndex.mother]
    
    def trackV0(self,tracks1,tracks2=None,targetMass=None,cmEnergy=None):
        '''Calculate vertexes from all combinations of two given sets
        of tracks.  The sets can be the same (or the second not
        specified), in which case the combinations are done within
        first set.  In that case, only distinct and non
        self-combinations are returned.
        
        Parameters
        ----------
        tracks1 : numpy.ndarrray shape=(M,9)
            The tracks to calculate vertexes from
        tracks2 : numpy.ndarrray shape=(M,9)
            The tracks to calculate vertexes from.  Can be None,
            in which case the combinations are done within tracks1 
        targetMass : float
            The mass of the assumed particle.  Can be None, in which case
            the Q value is not computed (return q=None)
        cmEnergy : float
            The collision energy. Can be none, in which case the
            momentum fraction x is not computed (return x=None)
        
        Returns
        -------
        invM : numpy.ndarray - shape=(N), dtype=float
            The invariant massm of each vertex
        four : numpy.ndarray - shape=(N,4)
            Total out-going four-vector 
        pdgs : numpy.ndarray - shape=(N,2), dtype=int
            PDGs of particles of the incoming particle to each vertex
        mcPdgs : numpy.ndarray - shape=(N,2), dtype=int
            MC ("true") PDGs of particles of the incoming particle to
            each vertex (MC input only)
        mothers: numpy.ndarray - shape=(N,2), dtype=int
            PDGs of the mother particles of each vertex incoming
            particles
        x : numpy.ndarray - shape=(N), dtype=float
            The momentum fraction of the vertexes (uses cmEnergy)
        q :  numpy.ndarray - shape=(N), dtype=float
            The Q value of each vertex (uses targetMass)
        '''
        from numpy import sqrt

        # If the second track set was not given, then set it to be the
        # first track set.
        if tracks2 is None: tracks2 = tracks1

        # Check if the two track sets are the same 
        same = tracks1 is tracks2

        # Noting to combine 
        if (same and len(tracks1) < 2) or \
           (len(tracks1)<1 or len(tracks2)<1):
            return [None]*7

        # Get four vectors of selected tracks,
        v1      = self.trackFourVector(tracks1)
        v2      = self.trackFourVector(tracks2) if not same else v1
        # Get PDGs of selected tracks
        pdg1    = self.trackPDG       (tracks1)
        pdg2    = self.trackPDG       (tracks2) if not same else pdg1 
        # Get MC PDG (MC only) of selected tracks 
        mcPdg1  = self.trackMcPDG     (tracks1)
        mcPdg2  = self.trackMcPDG     (tracks2) if not same else mcPdg1
        # Get Mother PDG (MC only) of selected tracks
        mother1 = self.trackMother    (tracks1)
        mother2 = self.trackMother    (tracks2) if not same else mother1
        # Calculate invariant four-vectors between selected tracks
        four     = Filler.invariantVector(v1,v2);
        # Calculate square invariant mass
        invM2    = Filler.squareMass     (four)
        # Calculate square momentum of invariant four-vectors
        p2       = Filler.squareMomentum (four)
        # Get PDG pairs of tracks for each invariant four-vector
        pdgs     = Filler.invariantAttr  (pdg1,    pdg2)
        # Get Mother PDG of tracks for each invariant four-vector
        mothers  = Filler.invariantAttr  (mother1, mother2)
        # Get MC PDG of tracks for each invariant four-vector
        mcPdgs   = Filler.invariantAttr  (mcPdg1,  mcPdg2)
        # Calculate the momemtum fraction
        x        = None if cmEnergy is None else 2 * sqrt(p2) / cmEnergy 
        # Calculate Q of those invariant masses that have OK X
        q        = None if targetMass is None else sqrt(invM2 - 4 * targetMass)
        # Calculate invariant mass 
        invM     = sqrt(invM2)

        return invM, four, pdgs, mcPdgs, mothers, x, q

    def make3Tracks(self,three,invM,pdg,mcPdg=0,parentPdg=0,
                    status=TrackStatus.secondary):
        '''Create secondary tracks from 3-momenta and invariant masses

        Parameters
        ----------
        three : numpy.ndarray - shape=(N,3)
            Three-momenta
        invM : numpy.ndarray - shape=(N)
            Invariant masses
        pdg : numpy.ndarray - shape(N), or int
            Particle type of created tracks
        mcPdg : numpy.ndarray - shape(N), or int
            MC "true" type
        parentPdg : numpy.ndarray - shape(N), or int
           Parent type
        status : int - of TrackStatus bits
           Status code of the tracks made

        Returns
        -------
        tracks : numpy.ndarray - shape(N,9)
            Table of tracks 
        '''
        from numpy import hstack, sqrt, newaxis

        e = sqrt((three**2).sum(axis=1)+ invM**2)[:,newaxis]
        return self.make4Tracks(hstack((three,e)),pdg,mcPdg,parentPdg)

    def make4Tracks(self,four,pdg,mcPdg=0,parentPdg=0,
                    status=TrackStatus.secondary):
        '''Create secondary tracks from 4-momenta

        Parameters
        ----------
        four : numpy.ndarray - shape=(N,4)
            Four-momenta (px,py,pz,e)
        pdg : numpy.ndarray - shape(N), or int
            Particle type of created tracks
        mcPdg : numpy.ndarray - shape(N), or int
            MC "true" type
        parentPdg : numpy.ndarray - shape(N), or int
           Parent type
        status : int - of TrackStatus bits
           Status code of the tracks made

        Returns
        -------
        tracks : numpy.ndarray - shape(N,9)
            Table of tracks 
        '''
        from numpy import zeros
        
        tracks = zeros((len(four),len(self.trackIndex)))
        tracks[:,[self.trackIndex.px,
                  self.trackIndex.py,
                  self.trackIndex.pz,
                  self.trackIndex.e]]    = four
        tracks[:,self.trackIndex.type]   = pdg
        tracks[:,self.trackIndex.mcType] = mcPdg
        tracks[:,self.trackIndex.mother] = parentPdg
        tracks[:,self.trackIndex.status] = status

        return tracks
    

    def estimateType(self,m):
        '''Estimate the particle type from the mass.  The masses are
        looked up in the array `bins`.  The found bin indexes are then
        used to look-up the particle type in the array `types`.  Note,
        `digitize` returns len(bins) if a mass is not within the
        defined bins.  This is why we add another element to `types`
        which will return 0.

        '''
        from numpy import digitize, array
        bins = [0.53,
                0.57,
                0.75,
                0.83]
        types  = array([221,  # 0.53 <= m < 0.57 eta
                        0,    # 0.57 <= m < 0.75
                        223,  # 0.75 <= m < 0.83 omega 
                        0]) # Last is _out-of-range_ value

        return types[digitize(m,bins)-1]
    
    def selectTracksStatus(self,
                           tracks,
                           ignore=TrackStatus.standardBad,
                           keep=0):
        '''Select tracks that do _not_ have any of the bits in ignore
        set.  Returns a mask

        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate
        ignore : int
            Do _not_ select tracks that has _any_ of these bits set
        keep : int
            Keep only tracks that do match these bits if set

        Returns
        -------
        selected : numpy.ndarray shape=(N), dtype=bool
            Mask tracks selected by status 
        
        '''
        from numpy import bitwise_and, logical_not, logical_and
        status  = self.trackStatus(tracks).astype(int)
        trash   = bitwise_and(status,ignore) == 0
        keep    = bitwise_and(status,keep) == keep
        return logical_and(trash,keep)
        
    def selectTracks(self,
                     tracks,
                     pdgs,
                     minPtSquare,
                     maxSquareCosTheta,
                     ignore = TrackStatus.standardBad,
                     keep   = 0):
        '''Utility member function to select tracks that meet the
        requirements
        
        - Particle is one of the listed types
        - The square transverse momentum is larger than cut
        - The square cos(theta) is smaller than cut
        - The status code does not contain any of the flags in ignore
        - The status code has the keep bits set 
        

        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate
        pdgs : numpy.ndarray shape=(M), dtype=int
            PDG (particle type) codes to select
        minPtSquare : float
            Least square transverse momemtum
        maxSquareCosTheta : float
            Largest square cosine theta 
        ignore : int
            Do _not_ select tracks that has _any_ of these bits set

        Returns
        -------
        selected : numpy.ndarray shape=(N), dtype=bool
            Mask tracks selected 
        
        '''
        from numpy import logical_and, abs,sqrt, isin

        return logical_and(
            logical_and(
                self.trackPdgIn(tracks,pdgs),
                self.trackSquareTransverseMomentum(tracks) >= minPtSquare),
            logical_and(
                self.trackSquareCosTheta(tracks) <= maxSquareCosTheta,
                self.selectTracksStatus(tracks,ignore,keep)))

    def selectPions(self,
                    tracks,
                    minPtSquare       = 0.04,
                    maxSquareCosTheta = 0.9216,
                    ignore            = TrackStatus.standardBad,
                    keep              = 0):
        '''Select pions using the given requirements.  Returns mask

        Parameters
        ----------
        tracks : numpy.ndarray shape=(N,9)
            The tracks to investigate
        minPtSquare : float
            Least square transverse momemtum
        maxSquareCosTheta : float
            Largest square cosine theta 
        ignore : int
            Do _not_ select tracks that has _any_ of these bits set

        Returns
        -------
        selected : numpy.ndarray shape=(N), dtype=bool
            Mask of selected charge pion tracks

        '''
        return self.selectTracks(tracks,
                                 pdgs              = [211,-211],
                                 minPtSquare       = minPtSquare,
                                 maxSquareCosTheta = maxSquareCosTheta,
                                 ignore            = ignore,
                                 keep              = keep)
    
    # ----------------------------------------------------------------
    def photonSide(self,photons,header):
        '''Side of photons relative to thrust axis

            sign(px * tx + py * ty + pz * tz)
        
        Parameters
        ----------
        photons : numpy.ndarray shape=(N,9)
            The photons to investigate
        header : numpy.ndarray shape=(12)
            Event header. Thrust axis taken from here 

        Returns
        -------
        side : numpy.ndarray shape=(N)
            The side of the photons (-1,0,1)
        
        '''
        from numpy import sign
        if photons is None or len(photons) < 1: return []
        
        return sign((photons *
                     header[[self.headerIndex.thrustAxisX,
                             self.headerIndex.thrustAxisY,
                             self.headerIndex.thrustAxisZ]]).sum(axis=1))

    def photonSquareMomentum(self,photons):
        '''Get momentum of photons'''
        return (photons**2).sum(axis=1)
        
    def photonFourVector(self,photons):
        '''Get the momentum four-vector of a set of photons

            {px,py,pz,p}

        Note, this copies the data
        
        Parameters
        ----------
        photons : numpy.ndarray shape=(N,9)
            The tracks to investigate

        Returns
        -------
        fourVector : numpy.ndarray shape(N,4)
            The four vector tracks 
        '''
        from numpy import hstack, newaxis,sqrt
        return hstack((photons,sqrt((photons**2).sum(axis=1)[:,newaxis])))

    def photonV0(self,
                 photons1,
                 photons2=None):
        '''Calculate V0 between photons

        Parameters
        ----------
        photons1 : numpy.ndarray shape(N,3)
            Set of photons to calculate V0s from
        photons2 : numpy.ndarray shape(M,3)
            Set of photons to calculate V0s from.  Can be None, in which
            case it is taken to be the same as photons1
        
        Returns
        -------
        invM  : numpy.ndarray shape=(M)
            Invariant mass
        three : numpy.ndarray - shape=(M,3)
            Outgoing total three-vector vertex
        p : numpy.ndarray - shape=(M,2)
            Total momentum of pairs of photons 
        ids : numpy.ndarray(M,2)
            Unique identifers of photons outgoing of each V0
        '''
        from numpy import sqrt, arange
        if photons1 is None or len(photons1) < 1:
            return [None]*4
        
        # Check if the second input set is given or not 
        if photons2 is None: photons2 = photons1
        # Check if the two input sets are the same 
        same = photons2 is photons1
        # Get the lengths of the input 
        n1    = len(photons1)
        n2    = len(photons2) if not same else n1
        # Check that we have something to correlate
        if (same and n1 < 2) or (n1 < 1 and n2 < 1):
            return [None]*4
        
        # Make ids of photons - for making unique matches
        i1    = arange(n1)
        i2    = arange(n1,n1+n2) if not same else i1
        # Get photons IDs within this side
        ids   = self.invariantAttr(i1,i2)
        # Get momentum vectors of photons 
        p1    = sqrt(self.photonSquareMomentum(photons1))
        p2    = sqrt(self.photonSquareMomentum(photons2)) if not same else p1
        # Calculate total out-goint vector 
        three = self.invariantVector(photons1,photons2)
        # Get momenta of incomting photons 
        p     = self.invariantAttr  (p1,p2)
        # Calculate the invariant mass 
        invM2 = p.sum(axis=1)**2 - (three**2).sum(axis=1)

        return sqrt(invM2), three, p, ids
    

    def fitPhotonV0(self,
                    invM,
                    p,
                    photons1,
                    photons2,
                    targetMass,
                    nBins=20):
        '''Refit the V0s from photon-photon daughters

        Parameters
        ----------
        invM : numpy.ndarray - shape=(N)
            Invariant masses between photon sets
        photons1 : numpy.ndarray shape(N,3)
            Set of photons the V0s was calculated from
        photons2 : numpy.ndarray shape(N,3)
            Set of photons the V0s was calculated from 
        targetMass : double
            Mass of target particle when refitting.  If None, then
            do not refit.  May be zero,  in which case the refit is not
            constrained
        nBins : int
            Number of search-grid points in each momentum
            direction. If zero, then do not refit.
        onlyBest : bool        
            If true, then determine best candidate for each photon and
            return only the V0 with the least chi2
        
        Returns
        -------
        chi2 : numpy.ndarray shape(M)
            Chi^2 of refit (all zero if no refit was done)
        ids : numpy.ndarray(M,2)
            Unique identifers of photons outgoing of each V0
        '''
        from numpy import sqrt, linspace, meshgrid, array
        
        cosAng = 1 - invM**2 / (2 * p.prod(axis=1))
        cons   = targetMass**2 / 2 / (1 - cosAng)
        # Variance (square uncertainty) on momentum
        varP   = 0.18**2 * p + 0.009**2 * p**2
        sigP   = sqrt(varP)
        # Range of fit
        startP = p - 2 * sigP
        endP   = p + 2 * sigP

        minThree = []
        minChi2  = []
        minInvM2 = []
        # Calculates search ranges for each parameter bins become
        # a list of 2 elements, and each element is a (N,M=50),
        # matrix where the columns is the parameter sample points 
        bins     = [linspace(s,e,nBins).T
                    for s, e in zip(startP.T,endP.T)]
        for i,(a1,a2,(mp1,mp2),(vp1,vp2),ph1,ph2,c) in \
                enumerate(zip(*bins,p,varP,photons1,photons2,cons)):
            # Search values, square difference to mid
            p1, p2 = meshgrid(a1, a2)
            d1, d2 = meshgrid((a1-mp1)**2,(a2-mp2)**2)
            # Constraint term and uncertainty 
            deltaC = (p1 * p2 - c)**2
            varC   = p1**2 * d1 + p2**2 * d2
            # Chi^2, and minimum index
            chi2m  = d1 / vp1 + d2 / vp2 + deltaC / varC
            minij  = chi2m.argmin()
            chi2   = chi2m.flatten()[minij]
            # Check if we already have candidates for one of the photons
            # Get minimized p's
            minP1  = p1.flatten()[minij]
            minP2  = p2.flatten()[minij]
            # scales
            scale1 = minP1 / mp1
            scale2 = minP2 / mp2
            # print(i1,j2,ip1,ip2)
            vv1      = scale1 * ph1
            vv2      = scale2 * ph2
            tgtThree = vv1 + vv2
            # New invariant mass
            minPP2     = (tgtThree**2).sum()
            tgtInvM2   = (minP1+minP2)**2 - minPP2
            tgtChi2    = chi2
            minThree.append(tgtThree)
            minInvM2.append(tgtInvM2)
            minChi2 .append(tgtChi2)

        return sqrt(array(minInvM2)), \
            array(minThree), \
            array(minChi2)
        
        
    def bestPhotonV0(self,invM,three,chi2,ids):
        '''Find the best V0 candidate for each photon and return only
        that.

        Parameters
        ----------
        invM : numpy.ndarray - shape=(N)
            Invariant masses
        three : numpy.ndarray - shape(N,3)
            Three vector of V0
        chi2 : numpy.ndarray - shape (N)
            chi-square of refit of photon V0s
        ids : numpy.ndarray - shape (N,2)
            Photon indexes contributing to each V0

        Returns
        -------
        index : list
            List of indexes corresponding to best chi2s for each photon
        '''
        # Sort chi2-values
        from numpy import array
        order = Filler.argsorted(chi2)
        seen  = []

        # Define capture so that our filtering has state 
        def filter(i,ids=ids,seen=seen):
            if ids[i][0] in seen or ids[i][1] in seen:
                return False

            seen.extend(ids[i])
            return True

        return array([i for i in order if filter(i)])
    
    # ----------------------------------------------------------------
    # Jet utilities
    def jetEnergy(self,jets):

        '''Get energies of the three jets in the event 

        Parameters
        ----------
        jet : numpy.ndarray shape=(3,4)
            Jet four vectors 

        Returns
        -------
        energy : numpy.ndarray shape=3
            Energies of the jets 
        
        '''
        return jets[:,self.jetIndex.e]

    # ----------------------------------------------------------------
    # Four vector utilities
    @classmethod
    def squareMass(cls,fourVector):
        ''' Calculates the square-mass of a set of four-vectors

             E^2 - (px^2 + py^2 + pz^2)
        
        Parameters
        ----------
        fourvector : numpy.ndarray, shape=(N,4)
            Four vectors 

        Returns
        -------
        mass2 : numpy.ndarray, shape=(N)
            Square mass 
        
        '''
        return fourVector[:,3]**2 - (fourVector[:,:3]**2).sum(axis=1)

    @classmethod
    def squareMomentum(cls,fourVector):
        '''Returns the square momentum of a set of four-vectors

            px^2 + py^2 + pz^2
        
        Parameters
        ----------
        fourvector : numpy.ndarray, shape=(N,4)
            Four vectors 

        Returns
        -------
        p2 : numpy.ndarray, shape=(N)
            Square momentum 
        '''
        return (fourVector[:,:3]**2).sum(axis=1)

    @classmethod
    def invariantVector(cls,v1,v2=None,switch=7):
        '''Calculate invariant four-vector of combinations of the
        input four-vectors with it self. Self-combinations and
        redundant combinations are _not_ returned.

        Some explanations of the NumPy calculations 
        
            fourVector.T             transposes matrix 
            fourVector[...,newaxis]  adds a dummy dimension to matrix
            transpose(...,(0,2,1))   transposes last and second to last dim
            triu_indices(...,1)      selects upper triangle, except diagonal
        
        So,
         
            v[...,newaxis]+v.T[newaxis,..] creates all combinations of
                                           the vectors (rows) of the
                                           vector v with it self.  If
                                           v has N particles (or rows,
                                           or dimension (N,4)), then
                                           the result has dimensions
                                           (N,4,N)
            
            transpose(...,(0,2,1))         reorders the rows so that the
                                           result has dimension
                                           (6,6,4).
                                           
            triu_indices(...,1)            Then selects the upper triangle,
                                           without the diagonal. These
                                           indices ar then used to
                                           extract the result.
        
        A more straight forward way of coding this would be
        
             ret = []
             for ir,v in enumerate(fourVector):
                 for ic in range(ir+1,len(fourVector)):
                     ret.append(v + fourvector[ic])
             
             return array(ret)
        
        The NumPy computations has a complexity of O(N log N), while
        the straight forward loop has complexity O(N^2).  The scale
        factors on both of these orders are roughly the same, while
        the over head (O(1)) is larger for the NumPy computations.
        Thus, for N <= 6, the straight forward loop computation _is_
        faster (factor ~3 for N=2, but only ~1.3 for N=6).
        
        Hence, we use a strategy that if we have 6 or less four
        vectors, then we use the loop-based calculation, otherwise
        we use the NumPy array calculations.
        
        Parameters
        ----------
        fourVector : numpy.ndarray shape=(N,4)
             Four-vectors
        switch : int
             For which N to switch algorithm

        Returns
        -------
        invFourVector: numpy.ndarray shape=((N-1)+(N-2)+...+1,4)        
             The combined four vectors of all input four vectors.
             Self-combinations and redundant combinations are _not_
             returned.

        '''
        from numpy import newaxis, transpose, triu_indices, array, \
            full, asarray, shape

        if v2 is None: v2 = v1
        same = v2 is v1
        
        if max(len(v1),len(v2)) < switch:
            ret = []
            for ir,r1 in enumerate(v1):
                sc = ir + 1 if same else 0
                for ic in range(sc,len(v2)):
                    ret.append(r1 + v2[ic])
        
            return array(ret)
            
        ret = transpose((v1[...,newaxis]+v2.T[newaxis,...]),(0,2,1))

        if same: return ret[triu_indices(len(v1),1)]
        return ret.reshape(len(v1)*len(v2),v1.shape[1])

    @classmethod
    def invariantAttr(cls,a1,a2=None):
        '''Calculate "invariant" PDG of combined tracks. The returned
        vector is the sum or track PDGs.  Thus a combination of a
        particle and its anti-particle will result in a 0, while the
        combination of particle with it self is twice its PDG number

        Parameters
        -----------
        a1 : numpy.ndarray, shape=(N)
            The attribute to combine
        a2 : numpy.ndarray, shape=(M)
            The attribute to combine

        Returns
        -------
        pairs : numpy.ndarray, shape=(N,2)
            The combined attribute of each combination 
        '''
        from numpy import newaxis, transpose, triu_indices, array, \
            meshgrid, append

        if a2 is None: a2 = a1
        same = a1 is a2
        ret  = append(*([a[...,newaxis] for a in meshgrid(a1,a2)][::-1]),axis=2)

        if same: return ret[triu_indices(len(a1),1)]
        return ret.reshape(len(a1)*len(a2),ret.shape[2])
    
    # ----------------------------------------------------------------
    # Track utilities
    def selectEvent(self,event,nGoodTracks=5,minJetE=32):
        '''Select event based on number of good tracks and least total
        jet energy

        Parameters
        ----------
        event : pyzqq.Event
            The event to inspect
        nGoodTracks : int
            The least number of good tracks
        minJetE : float
            The least total Jet energy in GeV 

        Returns
        -------
        select : bool
            True if event meets requirements 
        '''

        jetE = sum(self.jetEnergy(event.jets))
        if jetE < minJetE:
            return False

        nGood = sum(self.selectTracksStatus(event.tracks))
        if nGood < nGoodTracks:
            return False;

        return True
        
    # ----------------------------------------------------------------
    # Track utilities 
    @classmethod
    def mergedict(cls,target,update):
        '''Function to merge dictionaries recursively'''
        for k,v in update.items():
            if k not in target:
                target[k] = v
                continue
                
            if isinstance(target[k],dict):
                if isinstance(v,dict):
                    cls.mergedict(target[k],v)
                    continue
                else:
                    raise RuntimeError(f'Cannot update dict {k} '
                                       f'with {type(v)}')
            # If key k is in target, and it is not a dictionary, then
            # keep the value of that key

    @classmethod
    def argsorted(cls,seq,key=lambda x : x):
        '''Indirect sort of sequence by key.  The sorted indexes of
        the input sequence is returned.  That is, by indexing the
        sequence with the return value, one will get a sorted
        sequences corresponding to the input sequence.
        
        Parameters
        ----------
        seq : sequence
            Input sequence to sort
        key : callable        
            A callable that takes a single element of the input
            sequence and returns a value to sort by.

        Returns
        -------
        indexes : list
            A list of indexes that when applied in order to the input
            sequence sorts the input sequence.
        '''
        return [i for i,e in sorted(enumerate(seq),
                                    key = lambda ie: key(ie[1]))]
    
#
# EOF
#


        
