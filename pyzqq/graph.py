# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3 
class Graph:
    def __init__(self,x,y,ex=None,ey=None,
                 title='',xLabel='', yLabel=''):
        from numpy import asarray, shape

        assert shape(x) == shape(y), \
            f'Shape of x ({shape(x)}) '\
            f'not the same as shape of y ({shape(y)})'

        assert ex is None or shape(ex) == shape(x), \
            f'ex not None or shape ({shape(ex)}) ' \
            f'not the same as shape of x ({shape(x)})'

        assert ey is None or shape(ey) == shape(x), \
            f'ey not None or shape ({shape(ey)}) ' \
            f'not the same as shape of x ({shape(x)})'
        
        self._x      = asarray(x)
        self._y      = asarray(y)
        self._ex     = asarray(ex)
        self._ey     = asarray(ey)
        self._xLabel = xLabel
        self._yLabel = yLabel
        self._title  = title

    @property 
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @property
    def ex(self):
        if self._ex.ndim == 0:
            return None
        return self._ex

    @property
    def ey(self):
        if self._ey.ndim == 0:
            return None
        return self._ey

    @property
    def xLabel(self):
        return self._xLabel

    @xLabel.setter
    def xLabel(self,l):
        self._xLabel = l
        
    @property
    def yLabel(self):
        return self._yLabel

    @yLabel.setter
    def yLabel(self,l):
        self._yLabel = l

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self,l):
        self._title = l
        
    def mask(self,low,high):
        from numpy import logical_and
        return logical_and(self.x>low,self.x<high)

    def isCompat(self,other,throw=True):
        from numpy import all

        if not all(self.x == other.x):
            if throw:
                raise AssertionError(
                    f'Inconsistent X values')
            return False

        return True

    def plot(self,ax,**kwargs):
        label = kwargs.pop('label',self.title)

        ax.errorbar(self.x,self.y,self.ey,self.ex,
                    label=label,**kwargs)
        ax.set_xlabel(self.xLabel)
        ax.set_ylabel(self.yLabel)
        
    @classmethod
    def sum(cls,lhs,rhs,lhsf=1,rhsf=1):
        from numpy import sqrt

        lhs.isCompat(rhs)
        
        x        = lhs.x
        y        = lhsf*lhs.y + rhsf*rhs.y
        ex       = lhs.ex
        ey       = sqrt((lhsf*lhs.ey)**2 + (rhsf*rhs.ey)**2)
        return Graph(x,y,ex,ey,
                     f'{lhs.title} plus {rhs.title}',
                     lhs.xLabel,
                     f'{lhs.yLabel}+{rhs.yLabel}')

    @classmethod
    def difference(cls,lhs,rhs,lhsf=1,rhsf=1):
        from numpy import sqrt

        lhs.isCompat(rhs)
        
        x        = lhs.x
        y        = lhsf*lhs.y - rhsf*rhs.y
        ex       = lhs.ex
        ey       = sqrt((lhsf*lhs.ey)**2 + (rhsf*rhs.ey)**2)
        return Graph(x,y,ex,ey,
                     f'{lhs.title} minus {rhs.title}',
                     lhs.xLabel,
                     f'{lhs.yLabel}-{rhs.yLabel}')

    @classmethod
    def product(cls,lhs,rhs,lhsf=1,rhsf=1):
        from numpy import sqrt

        lhs.isCompat(rhs)
        
        x        = lhs.x
        y        = lhsf*lhs.y * rhsf*rhs.y
        ex       = lhs.ex
        ey       = sqrt((rhsf*rhs.y)**2*(lhsf*lhs.ey)**2 +
                        (lhsf*lhs.y)**2*(rhsf*rhs.ey)**2)
        return Graph(x,y,ex,ey,
                     f'{lhs.title} times {rhs.title}',
                     lhs.xLabel,
                     fr'{lhs.yLabel}\times{rhs.yLabel}')
    
    @classmethod
    def ratio(cls,lhs,rhs):
        from numpy import sqrt

        lhs.isCompat(rhs)
        
        x        = lhs.x
        y        = (lhsf*lhs.y) / (rhsf*rhs.y)
        ex       = lhs.ex
        ey       = y * sqrt(lhs.ey**2/lhs.y**2 + rhs.ey**2/rhs.y**2)
        return Graph(x,y,ex,ey,
                     f'{lhs.title} divided by {rhs.title}',
                     lhs.xLabel,
                     fr'{lhs.yLabel}/{rhs.yLabel}')
    
    def __str__(self):
        '''Get a string representation of this histogram'''
        return self.title

    def __repr__(self):
        '''Get a string representation of this histogram'''
        return self.title


#
# EOF
#
