# `pyzqq` package to for $\aleph$ $Z\rightarrow q\bar{q}$ data 

## Content 

### Basic and utilities

- [`__init__.py`](__init__.py): Load the package, and so on. 

- [`Graph`](graph.py): A set of correlated $(x,y)$ pairs, and their
  associated uncertainties.  These objects can be manipulated (adding,
  subtracting, multiplying, dividing).
  
- [`Histogram`](histogram.py): A histogram of observations.  We can
  only add observations to objects of this kind.  When we need to do
  manipulations, we _must_ extract a `Graph` object from this.
  
- [`Event`](event.py): A structure holding event information.  These
  are read from the Zip files containing events.

- [`ParticlesDB`](particlesdb.py): Database of particle properties. 

- [`Progress`](progress.py): A simple progress bar 

- [`utils.py`](utils.py): Various utilities for saving, loading
  etc. data. 


### Event processing 

- [`Reader`](reader.py): A class to read in the ZIP files with
  event data. 

- [`Looper`](looper.py): Loop over events in Zip file and call
  contained [`Filler`](`filler.py`) objects

- [`Filler`](filler.py): A base class for other filler classes, such as
  [`GammaGamma`](gammagamma.py), [`PiPi`](pipi.py), and so
  on. Contains various utility member functions for calculating
  invariant masses, V0s, and other quantities derived from tracks and
  photons. 
  
- [`GammaGamma`](gammagamma.py): Fill histograms related to
  $\gamma\gamma$ V0s. 

- [`KPi`](kpi.py): Fill histograms related to $K$s 

- [`PiPi0`](pipi0.py): Analyse the events for triple decays like
  $X\rightarrow\pi^0\pi^{\pm}\pi^{\pm}$ and
  $X\rightarrow\eta\pi^{\pm}\pi^{\pm}$

- [`PiPi`](pipi.py): Analyse the events for
  $X\rightarrow\pi^{\pm}\pi^{\pm}$ events


## Some comments 

- Python does not have a standardised representation of histograms.
  The closest we got is the [NumPy](https://numpy.org)
  [`histogram`](https://numpy.org/doc/stable/reference/generated/numpy.histogram.html)
  function.  However, the storage and manipulation of the return of
  that function is up to the user.  That is why we provide the
  [`Histogram`](histogram.py) class here. 
  
- [`Histogram`](histogram.py)s are _statistical_ objects representing
  an empirical probability distribution.  As such, it does not make
  sense to subtract, scale, multiply, or divide such objects.
  Hence, the  [`Histogram`](histogram.py) class only allows us to add
  observations to the object.  
   
  We _can_ extract the distribution (either normalised to an integral
  of 1 - a true probability distribution or as a number density) into
  a [`Graph`](graph.py) object, which _does_ allow manipulations.
  However, once we do that, we not necessarily dealing with
  probability distribution.  The package makes that distinction
  evident. 
   
- Python has many different options for storing data, most of which
   are not really appropriate for particle collision data.  
   
  Here we have chosen to store event data as NumPy _tables_ (or
  arrays), as these allow for easy manipulation.   That does mean,
  however, that we need a bit of additional scaffolding around data.
  To that end, we use the common Zip archive format with additional
  meta information stored as JSON data. 
  
  The classes [`Event`](event.py) and [`Reader`](reader.py) takes care
  of all the intricate details in a way that allows us to be selective
  (and therefore efficient) in what data we read in. 
  
  Incidentally, this format is very much akin to the ALICE $O^2$ data
  format which is stored in Apache Arrow tables. 
  
- In the [analyses](../analyses) of the histograms produced, we use
  standard NumPy and SciPy tools, plus the package
  [`nbi_stat`](https://cholmcc.gitlab.io/nbi-python/statistics/nbi_stat/)
  which is based on the freely (as in beer _and_ speech) book
  [Statistics Overview - With
  Python](https://cholmcc.gitlab.io/nbi-python/statistics/#Statistik),
  available in both English and Danish.  If this package is not
  available, one can install it by

      pip install nbi_stat 

  
  Note that we take most decision _up-front_, rather than relying on
  black-box solutions, so that it is clear to us what is going on and
  so that we may better understand the results of our analysis. 
  
  
