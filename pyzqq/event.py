# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3 
'''Event classes'''

# --------------------------------------------------------------------
class Event:
    def __init__(self,reader):
        '''Construct an event, done by reader'''
        self._reader = reader 
        self._header  = None
        self._tracks  = None
        self._photons = None
        self._jets    = None
        self._v0s     = None


    def clear(self):
        '''Clear the event, i.e., set data to none'''
        self._header  = None
        self._tracks  = None
        self._photons = None
        self._jets    = None
        self._v0s     = None

    @property 
    def header(self):
        '''Get the header'''
        return self._header

    @header.setter
    def header(self,value):
        '''Set the header'''
        self._header = value

    @property
    def isMC(self):
        '''Check if this is MC data by checking if 'mcFlavour' the
        header is non-zero'''
        return self.header[0] != 0

    @property 
    def tracks(self):
        '''Get tracks of the event. If not read in already, it will be'''
        if self._tracks is None:
            self._tracks = self._reader._getData('tracks')

        return self._tracks

    @property 
    def photons(self):
        '''Get photons of the event. If not read in already, it will be'''
        if self._photons is None:
            self._photons = self._reader._getData('photons')

        return self._photons

    @property 
    def jets(self):
        '''Get jets of the event. If not read in already, it will be'''
        if self._jets is None:
            self._jets = self._reader._getData('jets')

        return self._jets
    
    @property 
    def v0s(self):
        '''Get v0s of the event. If not read in already, it will be'''
        if self._v0s is None:
            self._v0s = self._reader._getData('v0s')

        return self._v0s
