# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3
#
from . filler import Filler
from . histogram import Histogram

class PiPi(Filler):
    def __init__(self,xMin=0.05):
        '''Analyse events for charged pi-pi vertexes'''
        super(PiPi,self).__init__()
        self._xMin = xMin

    def beginLoop(self,mc):
        '''Start of looping.

        Parameters
        ----------
        mc : bool
            True if looping over simulated (so-called Monte-Carlo) data.
        '''
        
        from numpy import linspace
        
        super(PiPi,self).beginLoop(mc)

        # Use that mcFlavour is non-zero for MC data 
        self._isMc = mc

        # Labels used for same and opposite sign pion histograms
        sameLabel = r'$\pi^{\pm}\pi^{\pm}$'
        oppoLabel = r'$\pi^{\pm}\pi^{\mp}$'
        # Cuts on momentum fraction 
        xCuts    = [0.025,
                    0.050,
                    0.1,
                    0.15,
                    0.2,
                    0.3,
                    0.5]
        # Lambda (function) to make and invariant mass histogram 
        makeM = lambda title : Histogram(linspace(0, 1.8, 180+1),
                                         title,
                                         r'$m_{\mathrm{inv}}\,(\mathrm{GeV})$',
                                         r'$P(m_{\mathrm{inv}})$')
        # Lambda (function) to make and Q histogram 
        makeQ = lambda title : Histogram(linspace(0, 1.8, 180+1),
                                         title,
                                         r'$Q$',
                                         r'$P(Q)$')

                    
        # Our histograms 
        self._histograms = {
            'nev': 0,
            # For all momentum fractions above min cut 
            'all': {
                'inv_mass': {
                    'same':     makeM(sameLabel),
                    'opposite': makeM(oppoLabel)
                },
                'q': {
                    'same':     makeQ(sameLabel),
                    'opposite': makeQ(oppoLabel)
                }
            },
            # For momentum fractions below a certain cut
            'x': {
                cut: {
                    'same':     makeM(fr'{sameLabel} $x<{cut}$'),
                    'opposite': makeM(fr'{oppoLabel} $x<{cut}$')
                }
                for cut in xCuts
            }
        }
        if self._isMc:
            # List of particles we want to see the pions came from
            # First is name,
            # Second is the PDGs to match to pion mother particle
            # Third is the invariant mass range to accept 
            other = [['K_S^0',  [310],    [0.48,0.52]],
                     [r'\eta',  [221,331],[0,0.55]],
                     [r'\omega',[223],    [0,0.85]],
                     [r'\rho',  [113],    [0,100]],
                     ['other',  [310,221,331,223,113],[0,100]]]
            mc = {
                # For all momentum fractions above minimum 
                'all': {
                    # Make MC sub-item 
                    'mc': {
                        # Make an entry with two histograms of
                        # invariant mass and store the PDGs and
                        # invariant mass ranges to check for.
                        n : {
                            'pdg':      p,
                            'range':    r, 
                            'same':     makeM(f'From ${n}$, and $\pm\pm$'),
                            'opposite': makeM(f'From ${n}$, and $\pm\mp$')
                        }
                        for n,p,r in other
                    },
                    # Make a sub-item for mis-identified pions
                    # according to the MC "truth".
                    'mis': {
                        'same':     makeM(fr'Mis-identified {sameLabel}'),
                        'opposite': makeM(fr'Mis-idenified {oppoLabel}')
                    }
                },
                # For momentum fractions less than cut 
                'x': {
                    cut: {
                        # Make a sub-item for mis-identified pions
                        # according to the MC "truth".
                        'mis': {
                            'same':     makeM(fr'Mis-identified {sameLabel} '
                                              fr'$x<{cut}$'),
                            'opposite': makeM(fr'Mis-idenified {oppoLabel} '
                                              fr'$x<{cut}$')
                        }
                    }
                    for cut in xCuts
                }
            }
            Filler.mergedict(self._histograms,mc)
        

    def event(self,event):
        '''Process an event'''
        from numpy import argwhere, logical_and, logical_not, \
            isin, any, all, logical_or

        # Check event.  at least 5 good tracks and jet energy of at
        # least 32 GeV
        if not self.selectEvent(event):
            return 

        # Increase event counter
        self._histograms['nev'] += 1
        
        # Get collision energy from header 
        header   = event.header
        cmEnergy = header[self.headerIndex.cmEnergy]

        # Get tracks 
        tracks = event.tracks

        # Select pios that have
        # - pT>0.2,
        # - cos(theta) < 0.9215
        # - status < 3 (enough hits in Si-tracker)
        pions = self.selectPions(tracks)
        
        # Calculate side of each selected track by projection onto the
        # thrust axis
        sides = self.trackSide(tracks[pions], header)
            
        for s in [-1,1]:
            # Get the pions on this side 
            cand = tracks[pions.flatten()][sides==s,:]
            if len(cand) < 2: continue # Nothing to combine

            # Calculate the momemtum fraction
            invM, four, pdgs, mcPdgs, mothers, x, q = \
                self.trackV0(cand,
                             targetMass=Filler.pimMass,
                             cmEnergy=cmEnergy)

            if x is None:
                # No vertexes found - unlikely
                return 
            
            # Select those larger minimum cut, and select those values
            okX     = x > self._xMin
            invM    = invM   [okX]
            q       = q      [okX]
            x       = x      [okX]
            mothers = mothers[okX]
            pdgs    = pdgs   [okX]
            # Select those that are same-sign 
            # Select those that are opposite sign
            # Select X for same-sign
            # Select X for opposite-sign
            same    = (pdgs[:,0] == pdgs[:,1])
            oppo    = (pdgs[:,0] != pdgs[:,1])
            xSame   = x[same]
            xOppo   = x[oppo]
            # Mis identified
            okId    = None

            # Fill histograms corresponding to all good invariant masses
            self._histograms['all']['inv_mass']['same']    .fill(invM[same])
            self._histograms['all']['inv_mass']['opposite'].fill(invM[oppo])
            self._histograms['all']['q']       ['same']    .fill(q[same])
            self._histograms['all']['q']       ['opposite'].fill(q[oppo])
            if self._isMc:
                mcPdgs  = mcPdgs[okX]
                okId    = all(isin(mcPdgs,[-211,211]),axis=1)
                misId   = logical_not(okId)
                misSame = misId[same]
                misOppo = misId[oppo]

                self._histograms['all']['mis']['same']    .fill(invM[
                    logical_and(misId,same)])
                self._histograms['all']['mis']['opposite'].fill(invM[
                    logical_and(misId,oppo)])

                # Loop over histigrams defined for other particles
                # 
                # `name` is the name of the other particle
                # `data` contains the mother PDGs to match to and
                #        the range of invariant masses to match 
                for name,data in self._histograms['all']['mc'].items():
                    pdgs = data['pdg']
                    rnge = data['range']
                    # Create a test lambda (function).  Default is returns
                    # true if both parent particles have the same PDG and
                    # that PDG is one of the specified PDGs
                    test = lambda m,p : logical_and(m[:,0]==m[:,1],
                                                    isin(abs(m)[:,0],p))
                    if name == 'other':
                        # If the name is other, then invert the match
                        # against the given PDGs.  That is, the test
                        # returns true the parent PDGs do not match, or
                        # one of the parent PDGs is not one of the
                        # specified PDGs.
                        test = lambda m,p : logical_or(m[:,0]!=m[:,1],
                                                       all(isin(abs(m),p,
                                                                invert=True),
                                                           axis=1))

                    # Perform the selection as test for the given PDGs and
                    # that the invarriant mass is in the given range.
                    sel  = logical_and(okId,
                                       logical_and(test(mothers,pdgs),
                                                   logical_and(invM>rnge[0],
                                                               invM<rnge[1])))
                    data['same']    .fill(invM[logical_and(same,sel)])
                    data['opposite'].fill(invM[logical_and(oppo,sel)])
                

            # Fill histograms for ever increasing x region - should be bins
            for cut,histos in self._histograms['x'].items():
                histos['same']    .fill(invM[same][xSame<cut])
                histos['opposite'].fill(invM[oppo][xOppo<cut])
                if self._isMc:
                    histos['mis']['same']    .fill(invM[same][
                        logical_and(misSame,xSame<cut)])
                    histos['mis']['opposite'].fill(invM[oppo][
                        logical_and(misOppo,xOppo<cut)])

            
    def results(self):
        '''Return dictionary of results'''
        return self._histograms


    @classmethod
    def plotOpts(cls,name,dir):
        marker = {'opposite': {'marker': 'o' },
                  'same':     {'marker': 's', 'mfc': 'none'}}.get(name,{})
        color  = {'inv_mass': {'color': 'C0' },
                  'q':        {'color': 'C0' },
                  'mis':      {'color': 'C6' },
                  'other':    {'color': 'C5' },
                  r'\rho':    {'color': 'C4' },
                  r'\omega':  {'color': 'C3' },
                  r'\eta':    {'color': 'C2' },
                  r'K^0':     {'color': 'C1' }}.get(dir,{})
        ret = {}
        ret.update(marker)
        ret.update(color)
        return ret
                  
    @classmethod
    def plot(cls,dct,**kwargs):
        if 'pipi' in dct:
            dct = dct['pipi']

        nev = dct.get('nev',None)

        cls.plotBasic(dct['all'],nev,**kwargs)
        cls.plotCuts(dct['x'],nev,**kwargs)

    @classmethod
    def plotBasic(cls,basic,nev,**kwargs):
        from matplotlib.pyplot import subplots

        num  = kwargs.pop('num','pipi')
        sav  = kwargs.pop('save',False)
        
        fig, ax = subplots(ncols=2,
                           sharey=True,
                           gridspec_kw={'wspace':0},
                           num=num+'_basic',
                           **kwargs)

        toAx = { 'inv_mass': ax[0],
                 'q':        ax[1],
                 'mis':      ax[0],
                 'mc':       ax[0] }
        
        for s,v in basic.items():
            a = toAx.get(s,None)
            if a is None:
                printf(f'Unknown axes for {s}')
                continue
            
            for k,h in v.items():
                if isinstance(h,dict):
                    for l,hh in h.items():
                        if isinstance(hh,Histogram):
                            hh.plot(a,density=True,nev=nev,
                                    **cls.plotOpts(l,k))
                else:
                    h.plot(a,density=True,nev=nev,**cls.plotOpts(k,s))

            a.legend()

        fig.tight_layout()
        if sav:
            print(f'Storing plot in {fig.get_label()}.png')
            fig.savefig(fig.get_label()+'.png')
        

    @classmethod
    def plotCuts(cls,cuts,nev,**kwargs):
        from matplotlib.pyplot import subplots
        from math import ceil
    
        ncut = len(cuts)
        num  = kwargs.pop('num','pipi')
        sav  = kwargs.pop('save',False)
        
        fig, ax = subplots(ncols=2,
                           nrows=int(ceil(ncut/2)),
                           sharex=True,
                           sharey=True,
                           gridspec_kw={'hspace':0,'wspace':0},
                           num=num+'_byx',
                           **kwargs)
    
        for a,(s,v) in zip(ax.flatten(),cuts.items()):
            for k,h in v.items():
                if k == 'mis':
                    for l,hh in h.items():
                        hh.plot(a,density=True,nev=nev,**cls.plotOpts(l,k))
                else:
                    h.plot(a,density=True,nev=nev,
                           **cls.plotOpts(k,s))

            a.legend()
                
        fig.tight_layout()
        if sav:
            print(f'Storing plot in {fig.get_label()}.png')
            fig.savefig(fig.get_label()+'.png')
        

#
# EOF
#
