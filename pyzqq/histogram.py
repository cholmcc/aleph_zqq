# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3 
from . graph import Graph

class Histogram:
    def __init__(self,bins,title='',xLabel='',yLabel=None):
        '''Create a histogram.

        This is mainly a wrapper around `numpy.histogram` that allows for

        - Fill by events rather than accumulating all observations
        - I/O to JSON
        - Adding of histograms (i.e., add more statistics)
        - Plotting 
        

        Parameters
        ----------
        bins : sequence
        
            Sequence of bin-edges.  For N equidistant bins between A
            and B, this can be made with

                bins = numpy.linspace(A,B,N+1)

            N logarithmic bins between A and B can be made with

                bins = numpy.geomspace(A,B,N+1)

            Variable bins can be specified directly, e.g.,

                bins = [0,1,3,6,12,24,128]

        title : str
            Title on histogram, mainly used when drawing and as a
            reminder of hte content.
        xLabel : str
            Label of independent variable which is histogrammed
        yLabel : str
            Label of frequency of variable.  If not specified, then
            set to `P(x)`
        '''
        from numpy import array, asarray 
        self._bins   = asarray(bins)
        self._sumW   = array([0 for i in range(len(bins)-1)])
        self._nfills = 0
        self._title  = title
        self._xLabel = xLabel
        self._yLabel = yLabel
        if self._yLabel is None:
            if self._xLabel != '':
                from re import sub
                self._yLabel = sub(r'\$(\S+)\$',r'$P(\1)$',self._xLabel)
            else:
                self._yLabel = ''
        
    def fill(self,data,weights=None):
        '''Fill in obersvation in data, optionally with a weight
        (individual or overall).  Increments the number of fills by 1.

        Internally, this uses `numpy.histogram` to do the heavy lifting.
        
        Parameters
        ----------
        data : value of value-sequence
            Data to fill into the histogram.  Can be a sequnece of values.
        weights: value of value sequence (optional)
            Weight of data.  Can be a single value or a sequence, one
            per data value, to use when filling

        Return
        ------
        self : Histogram
            Returns self 
        '''
        if data is None: return
        
        try:
            iter(data)
        except:
            data = [data]

        if len(data) <= 0:
            return self
        
        from numpy import histogram

        sumW, _ = histogram(data, self._bins,weights=weights)
        self._sumW   += sumW        
        self._nfills += 1

        return self 

    def isCompat(self,other,throw=True):
        from numpy import all

        if not all(self._bins == other._bins):
            if throw:
                raise AssertionError(
                    f'Inconsistent binning when adding histograms ' 
                    f'{self.title} and {other.title}')
            return False

        return True
        
    def add(self,other):
        '''Add another histogram to this histogram.  Note that the
        histogram added must have the same binning as this histogram.
        '''
        self.isCompat(other)
        
        self._sumW   += other._sumW
        self._nfills += other._nfills

        return self

    @property
    def title(self):
        '''Get the title of the histogram'''
        return self._title

    @property
    def xLabel(self):
        '''Get the label set on the x axis'''
        return self._xLabel

    @property
    def yLabel(self):
        '''Get the label set on the y axis'''
        return self._yLabel
    
    @property
    def binEdges(self):
        '''Get the edges of the bins'''
        return self._bins

    @property
    def sumWeights(self):
        '''Get the individual sum of weights'''
        return self._sumW;

    @property
    def uncertaintySumWeights(self):
        '''Get the uncertainty on the weights'''
        from numpy import sqrt
        return sqrt(self.sumWeights)
    
    @property
    def binWidths(self):
        '''Get the widths of the bins'''
        return (self._bins[1:] - self._bins[:-1])

    @property
    def binHalfWidths(self):
        '''Get the half-widths of the bins'''
        return (self._bins[1:] - self._bins[:-1]) / 2
    
    @property
    def binCentres(self):
        '''Get the centres of the bins'''
        return (self._bins[1:] + self._bins[:-1]) / 2

    @property
    def sumTotal(self):
        '''Get the total sum of weights'''
        return self.sumWeights.sum()
    
    @property
    def normalized(self):
        '''Return a normalised version of the histogram, and the
        uncertainties on each bin.''' 
        scale = self.sumTotal
        return self.scaled(scale)

    def scaled(self,scale=None):
        '''Return a normalised version of the histogram, and the
        uncertainties on each bin.'''
        if scale is None:
            scale = self._nfill
            
        wscale = scale * self.binWidths
        return (self.sumWeights / wscale,
                self.uncertaintySumWeights / wscale)
    
    def toDict(self):
        '''Create dictionary from histogram'''
        return {'bins':   self._bins.tolist(),
                'sumW':   self._sumW.tolist(),
                'title':  self._title,
                'xLabel': self.xLabel,
                'yLabel': self.yLabel,
                'nfills': self._nfills}

    @classmethod
    def fromDict(cls,dct):
        '''Create a histogram from a dictionary'''
        from numpy import array
        ret         = Histogram(array(dct['bins']),
                                title  = dct.get('title',''),
                                xLabel = dct.get('xLabel',''),
                                yLabel = dct.get('yLabel',''))
        ret._sumW   = array(dct['sumW'])
        ret._nfills = array(dct['nfills']).item()

        return ret;

    @classmethod
    def expandResults(cls,res):
        '''Replace all histograms in dict with dictionaries.  Used
        when streaming a histogram to JSON.'''
        if isinstance(res,Histogram):
            return res.toDict()
        if isinstance(res,dict):
            return {key: cls.expandResults(val) for key,val in res.items() }
        if isinstance(res,list):
            return [cls.expandResults(val) for val in res]
        return res

    @classmethod
    def collapseResults(cls,res):
        '''Creates a histogram object in a dictionary if certain
        elements of the dictionary entry exists.  Used when streaming
        in JSON data'''
        if isinstance(res,dict):
            if 'bins' in res and 'sumW' in res:
                return Histogram.fromDict(res)

            return { key: cls.collapseResults(val) for key, val in res.items() }
        if isinstance(res,list):
            return [cls.collapseResults(val) for val in res]

        return res
            

    def asGraph(self,**kwargs):
        '''Return histogram as a graph (x,y) and (ex,ey)

        Parameters
        ----------
        kwargs : dict
            Keyword arguments:

            normalized : bool        
                If true, return histogram as a normalised graph.  That
                is, the integral of the returned graph is unity.  Each
                bin is scaled to its width and divided by the total
                sum of weights.
        
            density : bool
                If true, return as a histogram as density.   Each
                bin is scaled to its width and divided by the total
                number of fills or scale given by keyword scale.

            scale : float        
                In case of density, divide the returned y values by
                this number instead of the number of fills.

        Returns
        -------
        graph : Graph
            Graph of (x,y) and (ex,ey)
        '''
        from re import sub
        
        norm  = kwargs.pop('normalized',
                           not kwargs.pop('density',False))
        if norm and ('nev' in kwargs or 'scale' in kwargs):
            print(f'Keywords "nev" and "scale" ignored when normalising')
            
        scale = kwargs.pop('scale',kwargs.pop('nev',self._nfills))
        x     = self.binCentres
        ex    = self.binHalfWidths
        y, ey = self.normalized if norm else self.scaled(scale)

        xLabel = kwargs.pop('xLabel',kwargs.pop('xlabel',self.xLabel))
        yLabel = kwargs.pop('yLabel',kwargs.pop('ylabel',self.yLabel))
        title  = kwargs.pop('title', kwargs.pop('label', self.title))
        if not norm:
            yLabel = sub(r'\$?P\(\$?(.*)\)',r'$\\mathrm{d}N/\\mathrm{d}\1',
                         yLabel)

        return Graph(x,y,ex,ey,title,xLabel,yLabel)


    
    def plot(self,ax, **kwargs):
        '''Plot this histogram in the given axes

        Parameters
        ----------
        ax : matplotlib.axes.Axes
            An axes object to plot this histogram into.
        kwargs : dict
            Keyword arguments.  Recognised keywords are

            label : str
                If set, overrides the title as the histogram label
            normalized : bool
                If true (overrides density), plot the normalised histogram
            density : bool
                If true, plot a density histogram - i.e., scaled to
                the number of fills, or the scale factor given by the
                keyword `nev`.  This is the default plotting mode.

                When this option is set, then the y label is set to
                `dN/dx` if the it was otherwise set to `P(x)`.  
            nev : float
                Scale factor when plotting the histogram with option
                `density`. 

            Other keyword arguments are passed to `ax.errorbar`.
        
        '''
        label = kwargs.pop('label', self.title)
        graph = self.asGraph(**kwargs);
        kwargs.pop('normalized','')
        kwargs.pop('density','')
        kwargs.pop('nev','')
        kwargs.pop('scale','')

        ax.errorbar(graph.x,
                    graph.y,
                    graph.ey,
                    graph.ex,
                    label=label,**kwargs)
        ax.set_xlabel(graph.xLabel)
        ax.set_ylabel(graph.yLabel)
        
    def __str__(self):
        '''Get a string representation of this histogram'''
        return self.title

    def __repr__(self):
        '''Get a string representation of this histogram'''
        return self.title

#
# EOF
#

