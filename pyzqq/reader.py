# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3
#
'''Reader class'''
from . import Event

# --------------------------------------------------------------------
class Indexes:
    def __init__(self,table):
        '''Construct a namespace from dictionary'''
        d = {key: value['index'] for key, value in table.items()}
        self.__dict__.update(d)
        self._len = len(d)

    def __len__(self):
        return self._len
    
# --------------------------------------------------------------------
class Reader:
    class EventIterator:
        def __init__(self, reader):
            '''Iterate over events'''
            self._reader  = reader;

        def __iter__(self):
            '''Make this iterable'''
            return self
        
        def __next__(self):
            '''Get next event.  Return None when there are no more events'''
            event = self._reader._nextEvent()
            if event is None:
                raise StopIteration
            return event

        @property
        def isMc(self):
            '''Check if this is MC input'''
            return self._reader.isMc
        
        @property 
        def dictionary(self):
            '''Get description of the tables as a dictionary'''
            return self._reader.dictionary 

        @property 
        def numberOfEvents(self):
            '''Get number of events available'''
            return self._reader.numberOfEvents

        @property 
        def numberOfSelectedEvents(self):
            '''Get number of events available'''
            return self._reader.numberOfSelectedEvents
        
        @property
        def eventNumber(self):
            '''Get current event number'''
            return self._reader.eventNumber

        @property
        def headerIndex(self):
            '''Return namespace of header indexes'''
            return self._reader.headerIndex
    
        @property
        def trackIndex(self):
            '''Return namespace of track indexes'''
            return self._reader.trackIndex
    
        @property
        def photonIndex(self):
            '''Return namespace of photon indexes'''
            return self._reader.photonIndex
    
        @property
        def jetIndex(self):
            '''Return namespace of jet indexes'''
            return self._reader.jetIndex
        
        @property
        def v0Index(self):
            '''Return namespace of v0 indexes'''
            return self._reader.v0Index
        
            
    def __init__(self,filename,headerFilter=None):
        '''Reader of zqq event files''' 
        from zipfile import ZipFile
        from json import loads
        from numpy import load, argwhere, arange
        
        print(f'Opening {filename}, please be patient ...',end='',flush=True)
        self._zip     = ZipFile(filename, 'r')
        print('done')
        
        self._dict    = None
        self._current = -1
        self._event   = Event(self)

        with self._zip.open('dictionary.json','r') as df:
            self._dict = loads(df.read())

        self._headerIndex  = Indexes(self._dict['headers'])
        self._trackIndex   = Indexes(self._dict['tracks'])
        self._jetIndex     = Indexes(self._dict['jets'])
        self._photonIndex  = Indexes(self._dict['photons'])
        self._v0Index      = Indexes(self._dict['v0s'])

        print('Reading headers, please be patient ...',end='',flush=True)
        with self._zip.open('headers.np','r') as hf:
            self._headers = load(hf)

        self._index = arange(len(self._headers));
        if headerFilter is not None:
            self._index = argwhere(headerFilter(self._headers,
                                                self._headerIndex)).flatten()
        print(f'done, selected {len(self._index)} out of {len(self._headers)}')

    def __enter__(self):
        '''Called upon entering a context'''
        return iter(self)

    def __exit__(self,*args):
        '''Called upon exiting a context'''
        if self._zip is not None:
            self._zip.close()

        self._zip = None

    def __iter__(self):
        '''Create an iterator over events'''
        return Reader.EventIterator(self)
    
    @property 
    def isMc(self):
        '''Get event'''
        return self._headers[0,self._headerIndex.mcFlavour] != 0
    
    @property 
    def event(self):
        '''Get event'''
        return self._event

    @property 
    def eventNumber(self):
        '''Get event'''
        return self._index[self._current]
    
    @property 
    def numberOfEvents(self):
        '''Number of events read in'''
        return len(self._headers)

    @property 
    def numberOfSelectedEvents(self):
        '''Number of events read in'''
        return len(self._index)
    
    @property 
    def dictionary(self):
        '''Dictionary of columns in arrays'''
        return self._dict
    
    @property
    def headerIndex(self):
        '''Return namespace of track indexes'''
        return self._headerIndex
    
    @property
    def trackIndex(self):
        '''Return namespace of track indexes'''
        return self._trackIndex

    @property
    def photonIndex(self):
        '''Return namespace of photon indexes'''
        return self._photonIndex

    @property
    def jetIndex(self):
        '''Return namespace of jet indexes'''
        return self._jetIndex
    
    @property
    def v0Index(self):
        '''Return namespace of v0 indexes'''
        return self._v0Index
        
    def _nextEvent(self):
        '''Get the next event'''
        self._current += 1
        if self._current >= self.numberOfSelectedEvents:
            print(f'{self._current} > {self.numberOfSelectedEvents} (selected)')
            return None

        no = self.eventNumber
        
        self._event.clear();
        if no >= self.numberOfEvents:
            print(f'{no} > {self.numberOfEvents} (real)')
            return None
        
        self._event.header = self._headers[no,:]
        
        return self._event

    def _getData(self,which):
        '''Get a data array from the file for current event'''
        from numpy import load, array
        
        name = f'{self.eventNumber:09d}/{which}.np'
        
        try:
            with self._zip.open(name,'r') as inp:
                return load(inp);
        except:
            # Failing to load some data is not an error 
            pass
        return array([])
                    
    
    
#
# EOF
#
