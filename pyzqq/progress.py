# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3
#
'''A simple progress bar'''
from sys import stdout

class Progress:
    def __init__(self,total,every=None,size=80,out=stdout):
        from time import time
        from math import log10, ceil
        self._total   = total;
        self._start   = time()
        self._every   = every
        self._current = 0;
        self._width   = int(ceil(log10(total)))
        self._out     = out
        self._size    = (size # Terminal width
                         - 2 * self._width - 1 # event and total
                         - 3                   # '[' and '] '
                         - 4 - 2               # percent and '% '
                         - 5 - 2 - 3 - 2 - 1   # sec,m,h,separators,' '
                         - self._width - 3     # speed '/s '
                         - 4)                  # 'ETA '
        self._speed   = 0
        if self._every is None:
            self._every = max(min(self._total // (self._size * 10),
                                  int(10**(self._width-2))),1)
            print(f'Progress bar updates every {self._every} step')

    def __enter__(self):
        return self

    def __exit__(self,*args):
        from time import time
        if (self._current < self._total):
            print('\n'
                  f'Expected to get {self._total} steps, '
                  f'but only did {self._current}',
                  file=self._out,end='')
        
        print('\n'
              f'Processing of {self._current} steps took '
              f'{self.format_duration(time()-self._start)} '
              f'at a rate of {self._speed:.1f}/s',
              file=self._out,flush=True)

    def format_duration(self,seconds):
        min, sec = divmod(seconds,60)
        hour, min = divmod(min, 60)

        return f'{int(hour):3d}:{int(min):02d}:{sec:05.2f}'
        
    def step(self):
        self._current += 1
        if self._current % self._every != 0:
            return

        self.print()


    def print(self):
        from time import time 
        n     = int(self._size * self._current / self._total)
        per   = (time() - self._start) / self._current
        rem   = per * (self._total - self._current)

        self._speed = 1/per

        bar  = '='*n
        fill = '.'*(self._size - n)
        cur  = f'{self._current:{self._width}d}'
        tot  = f'{self._total  :{self._width}d}'
        eta  = self.format_duration(rem)
        spd  = f'{int(self._speed):{self._width}d}/s'
        pct  = f'{self._current*100/self._total:4.1f}%'
        print(f"[{bar}{fill}] {cur}/{tot} {pct} {spd} ETA {eta}",
              end='\r', file=self._out, flush=True)

        

        
