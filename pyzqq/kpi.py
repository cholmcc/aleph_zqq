# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3 
from . filler import Filler
from . histogram import Histogram

class KPi(Filler):
    def __init__(self,
                 maxChi2=3,
                 pi0MRange=[0.08,0.2],
                 nChi2Bins=50):
        '''Fill histograms with

            K*+ -> K0 pi+
            K*0 -> K+ pi-
            K*0 -> K- pi+
            K*+ -> K+ pi0 (from gamma gamma)
            phi -> K+ K-
            phi -> K0 K0
        '''
        from numpy import array 
        super(KPi,self).__init__()

        self._maxChi2   = maxChi2
        self._mPi0Range = pi0MRange
        self._nChi2Bins = nChi2Bins
        self._xBins     = array([0,0.025,0.05,0.1,0.15,0.2,0.3,0.5])

    def beginLoop(self,mc):
        from numpy import linspace
        
        super(KPi,self).beginLoop(mc)

        invMtxt = r'$m_{\mathrm{inv}}\,(\mathrm{GeV})$'
        makeInvM = lambda title : \
            Histogram(linspace(0,1.7,426), title, invMtxt)
        makeInvMX = lambda h,xlow,xhigh : \
            Histogram(h.binEdges,
                      h.title[:-1]+
                      r'\ x\in[{xlow:5.3f},{xhigh:5.3f})\mathrm{GeV}$',
                      h.xLabel)
        makeX = lambda title:  Histogram(self._xBins, title, r'$x$')
        
        self._histograms = {
            'nev': 0,
            'inv_mass': {
                'pi0':       makeInvM(r'$\gamma\gamma\rightarrow\pi^{0}$'),
                'pi0_fit':   makeInvM(r'$\gamma\gamma\rightarrow\pi^{0}\ \mathrm{fit}$'),
                'kStarK0Pi': makeInvM(r'$K^{*\pm}\rightarrow K^0\pi^{\pm}$'),
                'k0StarKPi': makeInvM(r'$K^{*0}\rightarrow K^{\pm}\pi^{\mp}$'),
                'kStarKPi0': makeInvM(r'$K^{*\pm}\rightarrow K^{\pm}\pi^{0}$'),
                'phiKK':     makeInvM(r'$\phi\rightarrow K^{\pm}K^{\mp}$'),
                'phiK0K0':   makeInvM(r'$\phi\rightarrow K^{0}K^{0}$'),
                'mK0K':      makeInvM(r'$X\rightarrow K^{0}K^{\pm}$'),
                'mKK': {
                    'same':  makeInvM(r'$X\rightarrow K^{\pm}K^{\pm}$'),
                    'oppo':  makeInvM(r'$X\rightarrow K^{\pm}K^{\mp}$'),
                },
            },
            'chi2': {
                'pi0': Histogram(linspace(0,10,101),r'$\chi^2',r'$\chi^2')
            }
        }
        dx  = { k: makeX(h.title)
                for k, h in self._histograms['inv_mass'].items()
                if isinstance(h,Histogram) and h.title != 'pi0' }
        byX = { k: [makeInvMX(h,xlow,xhigh)
                    for xlow,xhigh in zip(self._xBins[:-1],
                                          self._xBins[1:])]
                for k, h in self._histograms['inv_mass'].items()
                if isinstance(h,Histogram) and not h.title.startswith('pi0') }

        self._histograms['x'] = dx
        self._histograms['inv_mass']['x'] = byX

    def doPhotons(self,header,photons):
        '''Determine pi0 and eta V0s from photons'''
        from numpy import sqrt, full_like, logical_and, isin, logical_not, \
            array, all, any
        
        # Return values
        pi0     = []
        if len(photons) < 2:
            return pi0
        
        invM, three, p, ids = self.photonV0(photons)
            
        mOK   = logical_and(self._mPi0Range[0]<=invM,
                            invM<=self._mPi0Range[1])
        x     = sqrt((three**2).sum(axis=1)) * 2 / self.headerSqrtS(header)
        self._histograms['x']       ['pi0'].fill(x)
        self._histograms['inv_mass']['pi0'].fill(invM)

        notPi0  = full_like(mOK,True)
        notEta  = full_like(mOK,True)
        freeV0  = full_like(mOK,True)
        if not any(mOK):
            return pi0
        
        invMFit, threeFit, chi2 = self.fitPhotonV0(invM[mOK],
                                                   p[mOK],
                                                   photons[ids[mOK,0],:],
                                                   photons[ids[mOK,1],:],
                                                   Filler.pimMass,
                                                   self._nChi2Bins)
        # Now select the best fits
        best  = self.bestPhotonV0(invMFit,threeFit,chi2,ids[mOK,:])
                
        # `best` now contains index into invMFit, threeFit,
        # and chi2 which corresponds to the best fit to a pi0
        # mass.  invMFit is a one-to-one map of the sub-set
        # invM[mOK], so that best is really also an index into
        # `invM` where `mOK` is true.  Thus we can select from
        # `photons` by enumerating `mOK` and then select where
        # `mOK` is true, and from those select the `best`
        # indexes.
        #
        # We select those that are within chi2 cut
        ok    = chi2[best] <= self._maxChi2

        # So now `ok` is a mask of `best`, which in turn is an
        # index into where `mOK` is true, which is a mask on `photons`.
        pi0idx = best[ok]
            
        # We will use that to create a new pi0 track
        # table. pi0idx is an index on threeFit and invMFit
        pi0 = self.make3Tracks(threeFit[pi0idx],
                               self.pi0Mass, # invMFit[pi0idx],
                               111,
                               0,
                               22);
            
        x     = self.trackMomentumFraction(pi0, header)
        self._histograms['x']       ['pi0_fit'].fill(x)
        self._histograms['inv_mass']['pi0_fit'].fill(invMFit[pi0idx])
        self._histograms['chi2']    ['pi0']    .fill(chi2)

        return pi0

    def doFill(self,which,invM,x):
        '''Fill into channel specific histograms'''
        from numpy import digitize, sign
        
        self._histograms['inv_mass'][which].fill(invM)
        self._histograms['x']       [which].fill(x)
        if invM is None or x is None:
            return

        xBins = digitize(x,self._xBins)
        [self._histograms['inv_mass']['x'][which][bin-1].fill(m)
         for bin, m in zip(xBins,invM) if bin != len(self._xBins)]
        
    def doSide(self,header,photons,pions,kaons,kaon0s):
        '''Process tracks on one side of the thrust axis'''
        from numpy import sign
        pi0s = self.doPhotons(header,photons)
        cmEnergy = self.headerSqrtS(header)
        
        # Invariant mass of K0 pi+/-
        invMK0Pi, _, _, _, _, xK0Pi, _ = self.trackV0(kaon0s,pions,
                                                      cmEnergy=cmEnergy)
        self.doFill('kStarK0Pi',invMK0Pi,xK0Pi)

        # Invariant mass of K+/- pi+/-
        invMKPi, _,  pdgKPi,  _, _, xKPi, _  = self.trackV0(kaons,pions,
                                                            cmEnergy=cmEnergy)
        # We only care about the case of opposite sign (i.e., K+ pi-
        # and K- pi+)
        if pdgKPi is not None:
            oppo = sign(pdgKPi[:,0]) != sign(pdgKPi[:,1])
            # Fill histograms
            self.doFill('k0StarKPi',invMKPi[oppo],xKPi[oppo])

        # Invariant mass of K+/- pi0
        invMKPi0, _, _, _, _, xKPi0, _ = self.trackV0(kaons,pi0s,
                                                      cmEnergy=cmEnergy)
        self.doFill('kStarKPi0',invMKPi0,xKPi0)

        # Invariant mass of K+/- K+/-
        invMKK, _, pdgKK, _, _, xKK, _ = self.trackV0(kaons, cmEnergy=cmEnergy)
        if pdgKK is not None:
            same = sign(pdgKK[:,0]) == sign(pdgKK[:,1])
            oppo = sign(pdgKK[:,0]) != sign(pdgKK[:,1])
            # We only care about the case of opposite sign (i.e., K+
            # pi- and K- pi+)
            self._histograms['inv_mass']['mKK']['oppo'].fill(invMKK[oppo])
            self._histograms['inv_mass']['mKK']['same'].fill(invMKK[same])
            self.doFill('phiKK',invMKK[oppo],xKK[oppo])

        # Invariant mass of K+/- K0
        invMK0K, _, _, _, _, xK0K, _ = self.trackV0(kaon0s,kaons,
                                                    cmEnergy=cmEnergy)
        self.doFill('mK0K',invMK0K,xK0K)


    def event(self,event):
        if not self.selectEvent(event,nGoodTracks=4):
            return 

        # Increase event counter
        self._histograms['nev'] += 1
        
        # Get collision energy from header 
        header   = event.header
        cmEnergy = self.headerSqrtS(header)

        # Get tracks 
        tracks = event.tracks
        
        # Get the various particle tracks (and photons)
        # Charged pions 
        pions     = tracks[self.selectTracks(tracks, [211,-211], 0, 1000)]
        kaons     = tracks[self.selectTracks(tracks, [-321,321], 0, 1000)]
        kaon0s    = tracks[self.selectTracks(tracks, [-311,311], 0, 1000)]
        photons   = event.photons

        # Calculate sides of pions, photons, and conversions 
        photonSides = self.photonSide(photons, header)
        pionSides   = self.trackSide(pions,    header)
        kaonSides   = self.trackSide(kaons,    header)
        kaon0Sides  = self.trackSide(kaon0s,   header)

        # print('Photons',photons,photonSides)
        # print('Kaons',  kaons,  kaonSides)
        # print('Kaon0s', kaon0s, kaon0Sides)
        # print('Pions',  pions,  pionSides)
        
        for side in [-1,1]:
            self.doSide(header,
                        photons[photonSides == side],
                        pions  [pionSides   == side],
                        kaons  [kaonSides   == side],
                        kaon0s [kaon0Sides  == side])

        pass

    def results(self):
        return self._histograms

    @classmethod
    def plotOpts(cls,name,dir=None):
        opts = {
            'pi0':                  dict(color='C0', marker='o'), 
            'pi0_fit':              dict(color='C0', marker='s'),
            'kStarK0Pi':            dict(color='C2', marker='o'),
            'k0StarKPi':            dict(color='C2', marker='s'),
            'kStarKPi0':            dict(color='C2', marker='v'),
            'phiKK':                dict(color='C3', marker='o'),
            'phiK0K0':              dict(color='C3', marker='s'),
            'mK0K':                 dict(color='C4', marker='s'), 
            'mKK/oppo':             dict(color='C4', marker='*'),
            'mKK/same':             dict(color='C4', marker='*', mfc='none'),
        }
        nm = '/'.join([dir,name]) if dir is not None else name
        ret = opts.get(nm,{})
        if len(ret) < 1:
            print(f'Key "{nm}" not found')
        ret['ls'] = ''
        ret['ms'] = 8 if ret.get('marker','') == '*' else 6
        return ret
    
    @classmethod
    def plot(cls,dct,**kwargs):

        from matplotlib.pyplot import subplots, figure 
        if 'kpi' in dct:
            dct = dct['kpi']

        num = kwargs.pop('num','kpi')
        sav = kwargs.pop('save',False)
        nev = dct.get('nev',None)

        fig, ax = subplots(ncols=3,num=num+'_basic',**kwargs)

        inv_mass = dct.get('inv_mass',{})
        hi       = []
        for k, h in inv_mass.items():
            if isinstance(h,dict):
                for kk, hh in h.items():
                    if isinstance(hh,list):
                        continue
                    
                    a = cls.plotOpts(kk,k)
                    hh.plot(ax[0],density=True,nev=nev,**a)
                    hi.append([hh,a])
            elif isinstance(h,list):
                pass 
            else:
                a = cls.plotOpts(k)
                h.plot(ax[0],density=True,nev=nev,**a)
                hi.append([h,a])

        chi2 = dct.get('chi2',{})
        for k, h in chi2.items():
            h.plot(ax[1],normalized=True,ls='none')
        
        x = dct.get('x',{})
        for k, h in x.items():
            if isinstance(h,dict):
                for kk, hh in h.items():
                    a = cls.plotOpts(kk,k)
                    hh.plot(ax[2],density=True,nev=nev,**a)
            else:
                a = cls.plotOpts(k)
                h.plot(ax[2],density=True,nev=nev,**a)

        ax[0].set_yscale('log')
        ax[0].legend()
        ax[1].legend()
        ax[2].legend()

        fig.tight_layout()
        if sav:
            print(f'Storing plot in {fig.get_label()}.png')
            fig.savefig(fig.get_label()+'.png')

        fig, ax = subplots(ncols=1,num=num+'_invM',**kwargs)
        for h,o in hi:
            h.plot(ax,density=True,nev=nev,**o)
        ax.legend()
        ax.set_yscale('log')
            
        
        fig.tight_layout()
        if sav:
            print(f'Storing plot in {fig.get_label()}.png')
            fig.savefig(fig.get_label()+'.png')

#
# EOF
#



        
