# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3
#
from . filler import Filler
from . histogram import Histogram

class PiPi0(Filler):
    
    def __init__(self,maxChi2=3,
                 pi0MRange=[0.08,0.2],
                 etaMRange=[0.52,0.58],
                 nChi2Bins=50):
        '''Analyse events for charged pi-pi vertexes'''
        super(PiPi0,self).__init__()
        self._maxChi2   = maxChi2
        self._mPi0Range = pi0MRange
        self._mEtaRange = etaMRange
        self._nChi2Bins = nChi2Bins

    def beginLoop(self,mc):
        '''Start of looping.

        Parameters
        ----------
        mc : bool
            True if looping over simulated (so-called Monte-Carlo) data.
        '''
        
        from numpy import linspace
        
        super(PiPi0,self).beginLoop(mc)

        invMtxt = r'$m_{\mathrm{inv}}\,(\mathrm{GeV})$'
        # Lambda (function) to make and invariant mass histogram
        makeInvM = lambda title : \
            Histogram(linspace(0, 1.8, 180), title, invMtxt)
            
        makePi0M = lambda title : \
            makeInvM(r'$\pi^0\rightarrow\gamma\gamma'+title+'$')
        makeEtaM = lambda title : \
            makeInvM(r'$\eta\rightarrow\gamma\gamma'+title+'$')
        makeXPiM = lambda parent, daughter : \
            makeInvM(fr'${parent}\rightarrow{daughter}\pi^{{\pm}}$')
        makeXPiPiM = lambda parent, daughter, sign : \
            makeInvM(fr'${parent}\rightarrow({daughter}\pi^{{\pm}})\pi^{{{sign}}}$')
            
        makeX = lambda title : \
            Histogram([0.005,0.025,0.05,0.1,0.15,0.2,0.3,0.5],
                      title,
                      r'$x$',
                      r'$P(x)$')
        makeChi2 = lambda title : \
            Histogram(linspace(0,10,100),
                      fr'$\chi^2\, {self._mPi0Range[0]}\,\mathrm{{GeV}}'
                      fr'<m_{{\mathrm{{inv}}}}'
                      fr'<{self._mPi0Range[1]}\,\mathrm{{GeV}}'+title+'$',
                      r'$\chi^2$',
                      r'$P(\chi^2)$')
        # Our histograms 
        self._histograms = {
            'nev': 0,
            #  Momentum fractions
            'x': {
                'pi':      makeX(r'$\pi^{\pm}$'),
                'pi0':     makeX(r'$\pi^0$'),
                'pi0_fit': makeX(r'$\pi^0$ (fit)'),
                'eta':     makeX(r'$\eta$'),
                'pi0_pi':  makeX(r'$X\rightarrow\pi^{0}\pi^{\pm}$'),
                'eta_pi':  makeX(r'$X\rightarrow\eta\pi^{\pm}$'),
                'pi0_pi_pi': {
                    'same': makeX(r'$X\rightarrow(\pi^{0}\pi^{\pm})\pi^{\pm}$'),
                    'oppo': makeX(r'$X\rightarrow(\pi^{0}\pi^{\pm})\pi^{\mp}$'),
                },
                'eta_pi_pi': {
                    'same': makeX(r'$X\rightarrow(\eta\pi^{\pm})\pi^{\pm}$'),
                    'oppo': makeX(r'$X\rightarrow(\eta\pi^{\pm})\pi^{\mp}$'),
                }
            },
            'inv_mass': {
                'pi0':          makePi0M(''),
                'pi0_fit':      makePi0M(r'\ \mathrm{(fit)}'),
                'pi0_e':        makePi0M(r'(e^{\pm})'),
                'pi0_ee':       makePi0M(r'(e^{\pm}e^{\pm})'),
                'eta':          makeEtaM(''),
                'pi0_pi':       makeXPiM('X',r'\pi^{0}'),
                'eta_pi':       makeXPiM('X',r'\eta'),
                'pi0_pi_pi': {
                    'same': makeXPiPiM('X',r'\pi^{0}',r'\pm'),
                    'oppo': makeXPiPiM('X',r'\pi^{0}',r'\mp'),
                },
                'eta_pi_pi': {
                    'same': makeXPiPiM('\eta^{\prime}',r'\eta',r'\pm'),
                    'oppo': makeXPiPiM('\eta^{\prime}',r'\eta',r'\mp'),
                },
                'omega_pi0_pi_pi': {
                    'same': makeXPiPiM(r'\omega',r'\pi^{0}',r'\pm'),
                    'oppo': makeXPiPiM(r'\omega',r'\pi^{0}',r'\mp'),
                },
                'eta_pi0_pi_pi': {
                    'same': makeXPiPiM(r'\eta',r'\pi^{0}',r'\pm'),
                    'oppo': makeXPiPiM(r'\eta',r'\pi^{0}',r'\mp'),
                },
            },
            'chi2': {
                'pi0':      makeChi2(''),
            }
        }
        if self._isMC:
            d = self._histograms['inv_mass']
            d['eta_pi0_pi']   = makeXPiM(r'\eta_{\mathrm{MC}}',  r'\pi^{0}')
            d['omega_pi0_pi'] = makeXPiM(r'\omega_{\mathrm{MC}}',r'\pi^{0}')
            d['rho_pi0_pi']   = makeXPiM(r'\rho_{\mathrm{MC}}',  r'\pi^{0}')

        

    def doPhotons(self,header,photons):
        '''Determine pi0 and eta V0s from photons'''
        from numpy import sqrt, full_like, logical_and, isin, logical_not, \
            array, all, any
        
        # For cuts on eta masses 
        midEta  = (self._mEtaRange[0]+self._mEtaRange[1])/2;
        difEta  = (self._mEtaRange[1]-self._mEtaRange[0])/2;
        # Return values
        pi0     = []
        eta     = []
        if len(photons) < 1:
            return pi0, eta
        
        invM, three, p, ids = self.photonV0(photons)
            
        mOK   = logical_and(self._mPi0Range[0]<=invM,
                            invM<=self._mPi0Range[1])
        x     = sqrt((three**2).sum(axis=1)) * 2 / self.headerSqrtS(header)
        self._histograms['x']       ['pi0'].fill(x)
        self._histograms['inv_mass']['pi0'].fill(invM)

        notPi0  = full_like(mOK,True)
        notEta  = full_like(mOK,True)
        freeV0  = full_like(mOK,True)
        if any(mOK):
            invMFit, threeFit, chi2 = self.fitPhotonV0(invM[mOK],
                                                       p[mOK],
                                                       photons[ids[mOK,0],:],
                                                       photons[ids[mOK,1],:],
                                                       Filler.pimMass,
                                                       self._nChi2Bins)
            # Now select the best fits
            best  = self.bestPhotonV0(invMFit,threeFit,chi2,ids[mOK,:])
                
            # `best` now contains index into invMFit, threeFit,
            # and chi2 which corresponds to the best fit to a pi0
            # mass.  invMFit is a one-to-one map of the sub-set
            # invM[mOK], so that best is really also an index into
            # `invM` where `mOK` is true.  Thus we can select from
            # `photons` by enumerating `mOK` and then select where
            # `mOK` is true, and from those select the `best`
            # indexes.
            #
            # We select those that are within chi2 cut
            ok    = chi2[best] <= self._maxChi2

            # So now `ok` is a mask of `best`, which in turn is an
            # index into where `mOK` is true, which is a mask on `photons`.
            pi0idx = best[ok]
            
            # We will use that to create a new pi0 track
            # table. pi0idx is an index on threeFit and invMFit
            pi0 = self.make3Tracks(threeFit[pi0idx],
                                   self.pi0Mass, # invMFit[pi0idx],
                                   111,
                                   0,
                                   22);
            
            # We need to make a mask of V0's from gamma-gamma,
            # _not_ flagged as pi0 by the above.  For this, we
            # pick out those that where not the best fit to the
            # pi0 mass.
            #
            # List comprehension could probably be done with argwhere
            mOKidx = array([idx for idx,m in enumerate(mOK) if m])
            notPi0[mOKidx[pi0idx]] = False
            usedPhotons = ids[logical_not(notPi0)]
            freeV0      = logical_and(notPi0,all(isin(ids,usedPhotons,
                                                      invert=True),
                                                 axis=1))

            x     = self.trackMomentumFraction(pi0, header)
            self._histograms['x']       ['pi0_fit'].fill(x)
            self._histograms['inv_mass']['pi0_fit'].fill(invMFit[pi0idx])
            self._histograms['chi2']    ['pi0']    .fill(chi2)

        # At this point, we have pi0 tracks in `pi0`, and index of
        # V0s that are _not_ from these in `notPi0idx`.  From this
        # candidate set, we need to select the best eta
        # candidates, if any.
        if any(freeV0):
            # Now select the best fits
            diff     = abs(invM[freeV0]-midEta)
            etaCand  = self.bestPhotonV0(invM[freeV0],
                                         three[freeV0],
                                         diff,
                                         ids[freeV0,:])
            # `etaCand` are indexes into invM[notPi0],
            # three[notPi0], which has the smallest difference to
            # 0.55 (eta rest mass).
            #
            # Select those that are close enough
            ok     = diff[etaCand] < difEta
            mIdx   = array([idx for idx,m in enumerate(freeV0) if m])
            notEta[mIdx[etaCand[ok]]] = False
            
            # `ok` is a mask on `invM[notPi0]`, `three[notPi0]`
            # which are within the cut. We use that to make a
            # table of eta tracks.
            eta = self.make3Tracks(three[logical_not(notEta),:],
                                   invM [logical_not(notEta)],
                                   221,
                                   0,
                                   22);
            
            x     = self.trackMomentumFraction(eta, header)
            self._histograms['x']       ['eta'].fill(x)
            self._histograms['inv_mass']['eta'].fill(invM[logical_not(notEta)])

        # print(f'===')
        # for i,(m,(id1,id2),ok,noPi0,free,noEta) in \
        #         enumerate(zip(invM,ids,mOK,notPi0,freeV0,notEta)):
        #     print(f'{i:2d} {m:6.3f} {id1:2d}+{id2:2d} '
        #           f'pi0={" no" if noPi0 else "yes"}, '
        #           f'free={"yes" if free else " no"}, '
        #           f'eta={" no" if noEta else "yes"}')
        #     assert noPi0 or noEta, \
        #         'V0 is both pi0 and eta'
        return pi0, eta
        
    def doSide(self,header,photons,conv,pions):
        '''Do analysis on one side'''
        from numpy import arange, logical_not, logical_and
        
        cmEnergy = self.headerSqrtS(header)
        
        # Electrons from photo conversions.  We do these first, so
        # we can skip the below steps early, in case we find nothing. 
        cthree = conv[:,
                      [self.trackIndex.px,
                       self.trackIndex.py,
                       self.trackIndex.pz]]

        if len(conv) > 0 and len(photons) > 0:
            invM, three, *_ = self.photonV0(photons,cthree)
            self._histograms['inv_mass']['pi0_e'].fill(invM)
            # Should we build pi0's tracks from these?

        if len(conv) > 1:
            invM, three, *_ = self.photonV0(cthree)
            self._histograms['inv_mass']['pi0_ee'].fill(invM)
            # Should we build pi0's tracks from these?
            
        # If we have less than 2 photons on this side, go on 
        if len(photons) < 2:
            return
            
        pi0, eta = self.doPhotons(header,photons)
                
                
        # Check if we have pi0 or eta tracks.  If not, go on. 
        if (len(pi0) < 1 and len(eta) < 1) or len(pions) < 1:
            return

        # now we need to combine our pi0s and etas with charged
        # pions. 

        # Calculate V0s from pi0s and charged pions.  Note that we
        # calculate the index of the pi0 and pi that created these
        # V0s so that we can later use that.
        pi0PiM, pi0PiFour, pi0PiPdgs, _, pi0PiMothers, x, _ = \
            self.trackV0(pi0,pions,cmEnergy=cmEnergy)
        self._histograms['inv_mass']['pi0_pi'].fill(pi0PiM)
        self._histograms['x']       ['pi0_pi'].fill(x)
        # if pi0PiMothers is not None: print(pi0PiMothers[:,0])
        if self._isMC and False:
            piMother = abs(pi0PiMothers[:,0])
            self._histograms['inv_mass']['eta_pi0_pi']  \
                .fill(pi0PiM[piMother==221])
            self._histograms['inv_mass']['omega_pi0_pi']\
                .fill(pi0PiM[piMother==223]) 
            self._histograms['inv_mass']['rho_pi0_pi']  \
                .fill(pi0PiM[piMother==113]) 
        
            
        # Calculate V0s from eta and charged pions 
        etaPiM, etaPiFour, etaPiPdgs, _, etaPiMothers, x, _ = \
            self.trackV0(eta,pions,cmEnergy=cmEnergy) 
        self._histograms['inv_mass']['eta_pi'].fill(etaPiM)
        self._histograms['x']       ['eta_pi'].fill(x)

        # We create tracks of the found pi0(eta)+pi+/- V0s.  Note,
        # we do not know the identity of these particles.  We
        # store the mother pion PDG (211, or -211) as PDG of the
        # V0, so that we can later on distinguish between opposite
        # sign and same sign combinations.  We store the candidate
        # type as the mother type.
        pi0PiTracks = (self.make4Tracks(pi0PiFour,
                                        pi0PiPdgs[:,0],
                                        0,self.estimateType(pi0PiM))
                       if pi0PiFour is not None else None)            
        etaPiTracks = (self.make4Tracks(etaPiFour,
                                        etaPiPdgs[:,0],
                                        0,self.estimateType(etaPiM))
                       if etaPiFour is not None else None)

        # Now we can combine these two track sets with charged pions to make
        # 
        #    (pi0 + pi+/-)+pi+/-    (pi0 + pi+/-)+pi-/+ 
        #    (eta + pi+/-)+pi+/-    (eta + pi+/-)+pi-/+
        #
        # V0s
        for name,comb in {'pi0': pi0PiTracks,
                          'eta': etaPiTracks }.items():
            if comb is None:
                continue
            
            m, four, pdgs, _, mothers, x, _ = \
                self.trackV0(comb,pions,cmEnergy=cmEnergy)
            same = pdgs[:,0] == pdgs[:,1]
            oppo = logical_not(same)
            hnames = f'{name}_pi_pi'
            self._histograms['inv_mass'][hnames]['same'].fill(m[same])
            self._histograms['inv_mass'][hnames]['oppo'].fill(m[oppo])
            self._histograms['x']       [hnames]['same'].fill(x[same])
            self._histograms['x']       [hnames]['oppo'].fill(x[oppo])

            if name != 'pi0':
                continue

            # The IDs calculated below index the two-combinatorial
            # tracks (pi0+pi+/-).  We would like to find the invariant
            # masses from these two-combintorial tracks.
            #
            # Next, we estimate the type of the 3-particle
            # combinatorial tracks, in particular those that are
            # omegas or etas.  We then select those that a same-sign
            # and opposite-sign charged pions, and those that
            # correspond to the two types of particles (omegas and
            # etas).  We select the indexes of the pi0-pi+/- (and make
            # then unique via a set), which we can then use to index
            # the two-combinatorial track invariant masses.
            ids         = self.invariantAttr(arange(len(comb)),arange(len(pions)))
            estType     = self.estimateType(m)
            omegas      = estType == 223
            etas        = estType == 221
            idOmegaSame = list(set(ids[logical_and(same,omegas),1]))
            idOmegaOppo = list(set(ids[logical_and(oppo,omegas),1]))
            idEtaSame   = list(set(ids[logical_and(same,etas),  1]))
            idEtaOppo   = list(set(ids[logical_and(oppo,etas),  1]))
            mUp         = pi0PiM

            self._histograms['inv_mass']['omega_'+hnames]['same'].fill(mUp[idOmegaSame])
            self._histograms['inv_mass']['omega_'+hnames]['oppo'].fill(mUp[idOmegaOppo])
            self._histograms['inv_mass']['eta_'+hnames]  ['same'].fill(mUp[idEtaSame])
            self._histograms['inv_mass']['eta_'+hnames]  ['oppo'].fill(mUp[idEtaOppo])

    def event(self,event):
        '''Process an event'''
        # Check event. At least 2 good tracks and jet energy of at
        # least 32 GeV
        if not self.selectEvent(event,nGoodTracks=2):
            return 

        # Increase event counter
        self._histograms['nev'] += 1
        
        # Get collision energy from header 
        header   = event.header
        cmEnergy = self.headerSqrtS(header)

        # Get tracks 
        tracks = event.tracks

        # Select pios that have
        # - pT>0.2,
        # - cos(theta) < 0.9215
        # - status < 3 (enough hits in Si-tracker)
        pions     = tracks[self.selectPions(tracks).flatten()]
        x         = self.trackMomentumFraction(pions, header)
        self._histograms['x']['pi'].fill(x)
            
        # Get the photons of the event 
        photons = event.photons
        if photons is None:
            return 

        # Get conversion particles from the event
        conv = tracks[self.selectTracksStatus(tracks,
                                                0,
                                                Filler.TrackStatus.conversion)]

        # Calculate sides of pions, photons, and conversions 
        photonSides = self.photonSide(photons, header)
        pionSides = self.trackSide(pions, header)
        convSides = self.trackSide(conv, header)

        #p     = sqrt((photons**2).sum(axis=1))
        
        for s in [-1,1]:
            sphotons = photons[photonSides==s]
            spions   = pions[pionSides==s]
            sconv    = conv[convSides==s]

            self.doSide(header,sphotons,sconv,spions)

                    
    def results(self):
        '''Return dictionary of results'''
        return self._histograms


    @classmethod
    def plotOpts(cls,name,dir=None):
        opts = {
            'pi0':                  dict(color='C6', marker='o'), 
            'pi0_e':                dict(color='C7', marker='o'), 
            'pi0_ee':               dict(color='C7', marker='o'), 
            'pi0_fit':              dict(color='C0', marker='o'), 
            'pi0_pi':               dict(color='C0', marker='s'), 
            'eta_pi0_pi_pi/oppo':   dict(color='C3', marker='*'),
            'eta_pi0_pi_pi/same':   dict(color='C3', marker='*', mfc='none'),
            'eta_pi0_pi':           dict(color='C8', marker='*'),
            'omega_pi0_pi_pi/oppo': dict(color='C4', marker='*'),
            'omega_pi0_pi_pi/same': dict(color='C4', marker='*', mfc='none'),
            'omega_pi0_pi':         dict(color='C9', marker='*'),
            'pi0_pi_pi/oppo':       dict(color='C2', marker='d'),
            'pi0_pi_pi/same':       dict(color='C2', marker='d', mfc='none'),
            'rho_pi0_pi':           dict(color='C8', marker='*'),
            'eta':                  dict(color='C1', marker='o'), 
            'eta_pi':               dict(color='C1', marker='s'), 
            'eta_pi_pi/oppo':       dict(color='C5', marker='h'),
            'eta_pi_pi/same':       dict(color='C5', marker='h', mfc='none'),
        }
        nm = '/'.join([dir,name]) if dir is not None else name
        ret = opts.get(nm,{})
        if len(ret) < 1:
            print(f'Key "{nm}" not found')
        ret['ls'] = ''
        ret['ms'] = 10 if ret.get('marker','') == '*' else 8
        return ret

                        
    @classmethod
    def plot(cls,dct,**kwargs):
        from matplotlib.pyplot import subplots, figure 
        if 'pipi0' in dct:
            dct = dct['pipi0']

        num = kwargs.pop('num','pipi0')
        sav = kwargs.pop('save',False)
        nev = dct.get('nev',None)

        fig, ax = subplots(ncols=3,num=num+'_basic',**kwargs)

        inv_mass = dct.get('inv_mass',{})
        hi       = []
        for k, h in inv_mass.items():
            if isinstance(h,dict):
                for kk, hh in h.items():
                    a = cls.plotOpts(kk,k)
                    hh.plot(ax[0],density=True,nev=nev,**a)
                    hi.append([hh,a])
            else:
                if k == 'pi0_ee':
                    continue 
                a = cls.plotOpts(k)
                h.plot(ax[0],density=True,nev=nev,**a)
                hi.append([h,a])

        chi2 = dct.get('chi2',{})
        for k, h in chi2.items():
            h.plot(ax[1],normalized=True,ls='none')
        
        x = dct.get('x',{})
        for k, h in x.items():
            if isinstance(h,dict):
                for kk, hh in h.items():
                    a = cls.plotOpts(kk,k)
                    hh.plot(ax[2],density=True,nev=nev,**a)
            else:
                a = cls.plotOpts(k)
                h.plot(ax[2],density=True,nev=nev,**a)

        ax[0].set_yscale('log')
        ax[0].legend()
        ax[1].legend()
        ax[2].legend()

        fig.tight_layout()
        if sav:
            print(f'Storing plot in {fig.get_label()}.png')
            fig.savefig(fig.get_label()+'.png')

        fig, ax = subplots(ncols=1,num=num+'_invM',**kwargs)
        for h,o in hi:
            h.plot(ax,density=True,nev=nev,**o)
        ax.legend()
        ax.set_yscale('log')
            
        
        fig.tight_layout()
        if sav:
            print(f'Storing plot in {fig.get_label()}.png')
            fig.savefig(fig.get_label()+'.png')
        

#
# EOF
#
