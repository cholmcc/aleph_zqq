# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3
#
# Base class for defining loops
from . import Reader
from . progress import Progress

class Looper:
    def __init__(self,filename):
        self._filename = filename
        self._analyses = {}

    def addAnalysis(self,name,analysis):
        self._analyses[name] = analysis 

    def selectHeader(self,headers, headerIndex):
        '''Selects which events to receive based on the header
        information alone.  The default implementation selects all
        events.  Alternatively, one can fitler events by overriding
        this member function.  For example


            from numpy import logical_and, abs
            return logical_and(
               logical_and(header[:,headerIndex.cmEnergy] <= 91.6,
                           header[:,headerIndex.cmEnergy] >= 90.8),
               abs(header[:,headerIndex.thrustAxisZ]) <= 0.8)

        selects events with a collision energy in [90.8,91.6] and a
        thrust axis Z component less than or equal to 0.8
        '''
        from numpy import logical_and, abs

        ok_thrust = abs(headers[:,headerIndex.thrustAxisZ])<=0.8
        ok_cmE    = logical_and(headers[:,headerIndex.cmEnergy] <= 91.6,
                                headers[:,headerIndex.cmEnergy] >= 90.8)
        ok_all    = logical_and(ok_thrust, ok_cmE)
        return ok_all
        
        # from numpy import array
        # return array([True]*len(headers))

    def beginLoop(self):
        '''Called at the start of the loop after the headers have been
        read in.  Here, one may store indexes, etc. For example

            self.trackIndex  = reader.trackIndex;
            self.jetIndex    = reader.jetIndex;
            self.headerIndex = reader.headerIndex;
            
        '''
        self.trackIndex  = self._reader.trackIndex
        self.jetIndex    = self._reader.jetIndex
        self.headerIndex = self._reader.headerIndex
        self.v0Index     = self._reader.v0Index
        self.photonIndex = self._reader.photonIndex

        for name, analysis in self._analyses.items():
            analysis.setIndexes(self.trackIndex, 
                                self.jetIndex,   
                                self.headerIndex,
                                self.v0Index,    
                                self.photonIndex)
            analysis.beginLoop(self._reader.isMc)
 
    def event(self,event):
        '''Process a single event.  This is where the bulk of the work
        should be done.'''
        for name, analysis in self._analyses.items():
            analysis.event(event)

    def loop(self,nev=-1):
        '''Loop over all input events'''
        with Reader(self._filename, self.selectHeader) as reader:
            self._reader = reader 
            self.beginLoop()

            nev = (self._reader.numberOfSelectedEvents if nev < 0 else
                   min(self._reader.numberOfSelectedEvents,nev))
            with Progress(nev) as progress:
                iev = 0
                for event in reader:
                    progress.step()
                    self.event(event)
                    iev += 1
                    if iev >= nev:
                        break

    def finish(self,file_or_name):
        '''Called at the end of processing'''
        from . utils import saveResults
        
        results = {
            name : analysis.results()
            for name, analysis in self._analyses.items() }

        saveResults(file_or_name, results)



        
#
# EOF
#
