# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3
#
from . histogram import Histogram

def getFile(file_or_name,mode):
    from pathlib import Path
    
    isFile = (not isinstance(file_or_name, str) and
              not isinstance(file_or_name, Path) and
              ('w' in mode and hasattr(file_or_name,'write')) or
              ('r' in mode and hasattr(file_or_name,'read')))

    return file_or_name if isFile else open(file_or_name,mode)

class AsFile:
    def __init__(self,file_or_name,mode='r'):
        from pathlib import Path
        
        self._isFile = (not isinstance(file_or_name, str) and
                        not isinstance(file_or_name, Path) and
                        ('w' in mode and hasattr(file_or_name,'write')) or
                        ('r' in mode and hasattr(file_or_name,'read')))

        self._file = file_or_name if self._isFile else open(file_or_name,mode)

    def __enter__(self):
        return self._file

    def __exit__(self,*args):
        if self._isFile: return

        self._file.close()
        
        

def loadResults(file_or_name):
    from json import loads

    with AsFile(file_or_name, 'r') as input:
        ret = Histogram.collapseResults(loads(input.read()))

    return ret;

def saveResults(file_or_name,results,show=True):
    from json import dumps
    from . histogram import Histogram
    
    with AsFile(file_or_name, 'w') as output:
        if show:
            from pprint import pprint
            
            print('Results stored')
            pprint(results,depth=4)
        
        output.write(dumps(Histogram.expandResults(results),indent=1))
    

def doMerge(lhs,rhs):
    if isinstance(lhs,int):
        #print(f'Summing integers {lhs} and {rhs}')
        return lhs+rhs

    if isinstance(lhs,float):
        #print(f'Summing floats {lhs} and {rhs}')
        return lhs+rhs
    
    if isinstance(lhs,Histogram):
        #print(f'Summing histograms {lhs} and {rhs}')
        lhs.add(rhs)
        return lhs

    if isinstance(lhs,list):
        #print(f'Summing lists {lhs} and {rhs}')
        return [doMerge(l, r) for l, r in zip(lhs,rhs)]

    if isinstance(lhs,dict):
        #print(f'Summing dicts {lhs} and {rhs}')
        return {k: doMerge(l,rhs[k]) for k, l in lhs.items()}
            
    raise RuntimeError(f'Do not know how to deal with {type(lhs)} and {type(rhs)}')
            
def mergeResults(*results):
    ret = None
    for result in results:
        if ret is None:
            ret = result
            continue

        ret = doMerge(ret, result)

    return ret

        
#
# EOF
#


    
