# © 2023 Christian Holm Christensen
#
# Licensed under the GPL-3 
from . filler import Filler
from . histogram import Histogram

class GammaGamma(Filler):
    def __init__(self,maxChi2=3,mPi0Range=[0.08,0.2],nChi2Bins=50):
        '''Analyse events for charged pi-pi vertexes'''
        super(GammaGamma,self).__init__()
        self._maxChi2    = maxChi2
        self._mPi0Range  = mPi0Range
        self._nChi2Bins  = nChi2Bins

    def beginLoop(self,mc):
        '''Start of looping.

        Parameters
        ----------
        mc : bool
            True if looping over simulated (so-called Monte-Carlo) data.
        '''
        
        from numpy import linspace
        
        super(GammaGamma,self).beginLoop(mc)

        # Use that mcFlavour is non-zero for MC data 
        self._isMc = mc

        # Lambda (function) to make and invariant mass histogram 
        makePi0M = lambda title : \
            Histogram(linspace(0, 1, 201),
                      r'$X\rightarrow\gamma\gamma'+title+'$',
                      r'$m_{\mathrm{inv}}\,(\mathrm{GeV})$')
        makeX = lambda title : \
            Histogram([0.005,0.025,0.05,0.1,0.15,0.2,0.3,0.5,1],
                      title,
                      r'$x$')
        makeChi2 = lambda title : \
            Histogram(linspace(0,10,101),
                      fr'$\chi^2\, {self._mPi0Range[0]}\,\mathrm{{GeV}}'
                      fr'<m_{{\mathrm{{inv}}}}'
                      fr'<{self._mPi0Range[1]}\,\mathrm{{GeV}}'+title+'$',
                      r'$\chi^2$')
                      
        # Our histograms 
        self._histograms = {
            'nev': 0,
            #  Momentum fractions
            'x': {
                'pi':      makeX(r'$\pi^{\pm}$'),
                'g_g':     makeX(r'$\pi^0$'),
                'g_g_fit': makeX(r'$\pi^0\ \mathrm{Fit}$')
            },
            'inv_mass': {
                'g_g':     makePi0M(''),
                'g_g_fit': makePi0M(r'\ \mathrm{Fit}'),
                'g_e':     makePi0M(r'(e^{\pm})'),
                'e_e':     makePi0M(r'\rightarrow e^{\pm}e^{\pm}')
            },
            'chi2': {
                'g_g':    makeChi2(r''),
            }
        }
        

    def doSide(self,header,photons,conv):
        from numpy import sqrt, logical_and, any
        
        cmEnergy = self.headerSqrtS(header)
        
        # Electrons from photo conversions.  We do these first, so
        # we can skip the below steps early, in case we find nothing. 
        cthree = conv[:,
                      [self.trackIndex.px,
                       self.trackIndex.py,
                       self.trackIndex.pz]]
        
        # Now combine photons with electrons/positrons from
        # photo-conversion.
        if len(conv) >= 1 and len(photons) >= 1:
            invM, *_ = self.photonV0(photons,cthree)
            self._histograms['inv_mass']['g_e'].fill(invM)

        # Now combine electrons/positrons from photo-conversions.
        if len(conv) > 1:
            invM, *_ = self.photonV0(cthree)
            self._histograms['inv_mass']['e_e'].fill(invM)
        
        if len(photons) < 2:
            return
        
        invM, three, p, ids = self.photonV0(photons)
    
        mOK   = logical_and(self._mPi0Range[0]<=invM,
                            invM<=self._mPi0Range[1])
        x     = sqrt((three**2).sum(axis=1)) * 2 / cmEnergy
        self._histograms['x']       ['g_g'].fill(x)
        self._histograms['inv_mass']['g_g'].fill(invM)
        
        if not any(mOK):
            return
        
        invMFit, threeFit, chi2 = self.fitPhotonV0(invM[mOK],
                                                   p[mOK],
                                                   photons[ids[mOK,0],:],
                                                   photons[ids[mOK,1],:],
                                                   Filler.pimMass,
                                                   self._nChi2Bins)
        ok    = chi2 <= self._maxChi2
        x     = sqrt((threeFit**2).sum(axis=1)) * 2 / cmEnergy
        self._histograms['x']       ['g_g_fit'].fill(x[ok])
        self._histograms['inv_mass']['g_g_fit'].fill(invMFit[ok])
        self._histograms['chi2']    ['g_g']    .fill(chi2)
        
    def event(self,event):
        '''Process an event'''
        # Check event. At least 2 good tracks and jet energy of at
        # least 32 GeV
        if not self.selectEvent(event,nGoodTracks=2):
            return 

        # Increase event counter
        self._histograms['nev'] += 1
        
        # Get collision energy from header 
        header   = event.header
        cmEnergy = self.headerSqrtS(header)

        # Get tracks 
        tracks = event.tracks

        # Select pios that have
        # - pT>0.2,
        # - cos(theta) < 0.9215
        # - status < 3 (enough hits in Si-tracker)
        pions = tracks[self.selectPions(tracks)]
        x     = self.trackMomentumFraction(pions,header)
        self._histograms['x']['pi'].fill(x)
            

        # Get the photons of the event 
        photons = event.photons
        if photons is None:
            # print('No photons in this event')
            return 

        # Get conversion particles from the event
        conv   = tracks[self.selectTracksStatus(tracks,
                                                0,
                                                Filler.TrackStatus.conversion)]
        
        # Calculate side of each pion by projection onto the thrust
        # axis
        convSides = self.trackSide(conv, header)
        photonSides = self.photonSide(photons, header)
        
        for s in [-1,1]:
            self.doSide(header,
                        photons[photonSides==s],
                        conv[convSides==s])
                        
            
    def results(self):
        '''Return dictionary of results'''
        return self._histograms


    @classmethod
    def plot(cls,dct,**kwargs):
        from matplotlib.pyplot import subplots, figure 
        if 'gammagamma' in dct:
            dct = dct['gammagamma']

        nev = dct.get('nev',None)
        sav = kwargs.pop('save',False)

        fig, ax = subplots(ncols=3,**kwargs)

        inv_mass = dct.get('inv_mass',{})
        for k, h in inv_mass.items():
            h.plot(ax[0],density=True,nev=nev,ls='none')

        chi2 = dct.get('chi2',{})
        for k, h in chi2.items():
            h.plot(ax[1],normalized=True)
                
        x = dct.get('x',{})
        for k, h in x.items():
            h.plot(ax[2],density=True,nev=nev,ls='none')

        ax[0].set_yscale('log')
        ax[0].legend()
        ax[1].legend()
        ax[2].legend()

        fig.tight_layout()
        if sav:
            print(f'Storing plot in {fig.get_label()}.png')
            fig.savefig(fig.get_label()+'.png')
            
    
        
        

#
# EOF
#
